# Textboxen erstellen und konfigurieren

In diesem Skript zeigen wir dir, wie du mit einem Skript eine Textbox erstellst und diese konfigurierst.

```javascript
function main() {
  var document = app.documents.add({
    documentPreferences: {
      pageHeight: '210 mm',
      pageWidth: '210 mm'
    }
  });

  return setRect(document);
}

main();

function setRect(d) {
  var textframe = d.textFrames.add({
    geometricBounds : [0,0,'210mm', '210mm'],
    fillColor: app.activeDocument.swatches.item('Black'),
    fillTint: 20,
    contents: TextFrameContents.PLACEHOLDER_TEXT
  });

  textframe.textFramePreferences.insetSpacing = ['10mm','10mm','10mm','10mm'];
  return textframe;
}

```

Zuerst definieren wir die Funktion `main`. Mit ihr erstellen wir ein neues Dokument und speichern eine Referenz auf dieses Dokument in der Variable `document`. Danach übergeben wir die Variable `document` als Argument an die Funktion `setRect`. Das `return` stellt sicher, dass unser Skript einen Schlusswert ausgibt.

Die Funktion `setRect` nimmt einen Parameter `d` entgegen. Diesen haben wir beim Aufruf in `main` mit dem Argument `document` besetzt. Das heisst, `document` ist in `setRect` als `d` verfügbar.

Mit `d.textFrames.add({ ... })` erstellen wir eine neue Textbox, die wir mit `var textframe` referenzieren. Innerhalb der geschweiften Klammern `{}` legen wir die Eigenschaften der neuen Textbox fest. Zum Beispiel definieren wir, dass sie so gross sein soll wie die Seite (`geometricBounds : [0,0,'210mm', '210mm']`).

Nachdem wir die Textbox erstellt haben, können wir mit `textframe.textFramePreferences.insetSpacing = ['10mm','10mm','10mm','10mm']` den inneren Abstand zum Rand konfigurieren. Zum Schluss geben wir die Textbox mit `return` zurück an die Funktion `main`, welche sie ihrerseits als Schlusswert des Skripts zurückgibt.
