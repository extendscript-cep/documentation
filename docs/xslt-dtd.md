# XML-Struktur mit einer DTD validieren

In der Aufgabe 1 unserer XML-Transformation haben wir den Bezeichner des Wurzelelements von `items` in `Root` umbenannt. Danach haben wir in der zweiten Aufgabe die File-URI (Pfad) in ein eigenes Tag mit dem Bezeichner `image` extrahiert. Im vorerst letzten Schritt wollen wir für die ursprüngliche und die transformierte Datei eine DTD erstellen. Damit kannst du auf eine einfache Art und Weise sicherstellen, dass deine Ursprungs- und deine Zieldatei die richtige Struktur haben (werden). Falls du nicht weisst, was eine DTD ist, findest du im Abschnitt [*XML > DTD*](./xml-dtd.md) eine Einführung.

Falls du direkt in diesem Abschnitt einsteigst, musst du dir als Erstes die Projektdateien <a href="/examples/xslt.zip" target="_blank" download>hier</a> herunterladen und danach die Dokumente der Transformation <a href="/examples/xslt-final.zip" target="_blank" download>hier</a> herunterladen (zwei ZIP-Dateien).

Wenn du die Abschnitte Transformation 1 und Transformation 2 mitgemacht hast, kannst du mit deinen Dokumenten weiterarbeiten, sofern alles gestimmt hat – oder zur Sicherheit die Dokumente der Transformation <a href="/examples/xslt-final.zip" target="_blank" download>hier</a> nochmals herunterladen.

## Projektabschluss vorbereiten

Führe die beiden ZIP-Archive in einem Ordner zusammen. Deine Ablage sollte nun so aussehen:

![XSLT, Ordnerstruktur Projektabschluss](./img/xslt-exercise-03.png)

Die Datei `.project` wurde von Eclipse generiert, wenn du das Projekt bereits in Eclipse geöffnet hast. Sie wird dir aber nur angezeigt, wenn du die *Anzeige von versteckten Dateien* unter Apple MacOS oder Microsoft Windows *aktiviert* hast.

Öffne das Projektverzeichnis nun in Eclipse und erstelle zwei neue Dokumente. Wenn du nicht weisst wie das geht, findest du eine Einführung im Abschnitt [*XSL > Erste Schritte*](./xslt-firststeps.md).

## DTD erstellen

Wir werden in diesem Abschnitt zügig durch die DTD-Dateien gehen. Das Ziel soll nicht sein, die [Document Type Definition DTD (nochmals) zu erklären](./xslt-dtd.md), sondern, dir zu zeigen, wie du sie in deine XML-Dateien und deine Transformation einbinden kannst.

Erstelle in Eclipse zwei neue Dokumente:

1. `index.dtd`
2. `index.out.dtd`

Dein Projekt sollte in Eclipse nun so aussehen:

![XSLT, DTD Projektstruktur](./img/xslt-dtd.001.png)

### `index.dtd`

Als nächstes musst du deine XML-Datei `index.xml` um deine DTD-Spezifikation erweitern. Füge dazu direkt nach der Deklaration den Verweis auf deine Datei `index.dtd` ein:

```diff
  <?xml version="1.0" encoding="UTF-8"?>
+ <!DOCTYPE items SYSTEM "index.dtd">
  <items>
    ...
  </items>
```

Sobald du diese Zeile eingefügt hast, wird dir Eclipse sofort anzeigen, dass deine XML-Datei `Index.xml` komplett falsch sei. Das kommt daher, dass Eclipse versucht, deine XML-Datei mit deiner DTD zu validieren. Weil deine DTD aber noch leer ist und keine Regeln enthält, schlägt die Validierung komplett fehl.

![XSLT, DTD Validierung schlägt fehl](./img/xslt-dtd.002.png)

Wenn du das Erstellen einer DTD nochmals üben willst, wäre jetzt der richtige Augenblick dazu: Erstelle eine DTD, bis dir Eclipse keine Fehler mehr in deiner XML-Datei mit dem Namen `Index.xml` anzeigt.

Unsere Lösung für `index.dtd` ist:

```xml
<!ELEMENT items (item)+>
<!ELEMENT item (name, author, photo)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT author (#PCDATA)>
<!ELEMENT photo (#PCDATA)>
```

Dein XML-Dokument `index.xml` sollte nun keine Fehler mehr anzeigen:

![XSLT, Lösung index.dtd](./img/xslt-dtd.004.png)

## `index.out.dtd`

Damit du die DTD für das Ausgabe-XML mit dem Namen `index.out.xml` überprüfen und entwickeln kannst, musst du es zuerst zu deiner Transformation hinzufügen. Dies machst du mit dem Hinzufügen des Attributs `doctype-system` an das Element `xsl:output`:

```diff
  <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
- <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
+ <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" doctype-system="index.out.dtd"/>
  <xsl:strip-space elements="*"/>

    <xsl:template match="node()|@*">
      ...
    </xsl:template>
```

![XSLT, doctype-system setzen](./img/xslt-dtd.005.png)

Nachdem du die Konfiguration von `xsl:output` um `doctype-system` erweitert hast, kannst du deine Transformation ausführen (falls du noch keine Transformation konfiguriert hast, schau bitte im Abschnitt [*XSL > Transformation 1*](./xslt-transformation-1.md#transformation-konfigurieren) nach):

![XSLT, Transformation ausführen](./img/xslt-dtd.006.png)

`doctype-system` bewirkt, dass beim Transformierten `Index.out.xml` die Zeile …

```xml
<!DOCTYPE Root SYSTEM "index.out.dtd">
```

… angelegt wird. Sie referenziert deine DTD in `index.out.dtd`.

Nun zeigt dir Eclipse, wie bei `index.xml`, ganz viele Validierungsfehler an, weil deine DTD `index.out.xml` noch keine Regeln enthält. Diese solltest du jetzt hinzufügen:

```xml
<!ELEMENT Root (item)+>
<!ELEMENT item (name, author, photo, image)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT author (#PCDATA)>
<!ELEMENT photo (#PCDATA)>
<!ELEMENT image EMPTY>
<!ATTLIST image href CDATA #REQUIRE
```

Nach dem Speichern sollten die Validierungsfehler in Eclipse verschwinden:

![XSLT, Transformation mit DTD](./img/xslt-dtd.009.png)

Geschafft – das wars! In den letzten drei Abschnitten hast du eine einfache Transformation erstellt und den Datenimport und Datenexport mit einer DTD validiert. Die finalen Dokumente aus dieser Übung kannst du <a href="/examples/xslt-dtd.zip" target="_blank" download>hier</a> bei Bedarf herunterladen. Bitte denke daran, dass du sie, damit sie funktionieren, mit den Übungsdateien von hier in einem Ordner zusammenführen musst!

![XSLT, finale Dateien](./img/xslt-dtd.010.png)
