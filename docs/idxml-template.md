# Adobe-InDesign-Template erstellen

Damit du später die XML-Daten mit deinem Layout zusammenführen kannst, musst du als erstes ein Adobe-InDesign-Template definieren. Bei diesem platzierst du die XML-Elemente als eine Art Vorlage auf einer Seite. Beim Zusammenführen der Daten wird Adobe InDesign diese Seite verwenden, um dir im finalen Layout pro Datensatz eine Seite anzulegen.

Theoretisch könntest du an dieser Stelle direkt transformierte Datei `Index.out.xml` in Adobe InDesign laden. Wir wollen dir dagegen zeigen, wie du beim Laden einer XML-Datei auch eine Transformation durchführen kannst. Öffne dazu zunächst den Dialog für das Laden von XML-Dateien in der Ansicht *Struktur*:

![ID XML, Import XML](./img/idxml-importxml.png)

Stelle dabei sicher, dass du dir die Importoptionen anzeigen lässt. Wähle dazu *Optionen* und markiere die Checkbox bei *Importoptionen anzeigen*:

![ID XML, Show XML Import Options](./img/idxml-showimportoptions.png)

Klicke nun auf *Öffnen*. Im nächsten Dialog zeigt dir Adobe InDesign die Importoptionen für XML an:

1. Wähle alle voreinstellten Haken ab
2. Setz den Haken bei *Apply XSLT* und wähle im Dropdown das Dokument `transformation.xslt` aus unserem Projektverzeichnis aus

![ID XML, Imoprtoptionen für Template](./img/idxml-importoptionstpl.png)

Wenn du die beiden Einstellungen gemacht hast, kannst du das XML mit einem Klick auf den Button *OK* importieren.

## Importiertes XML validieren

Nach dem Import zeigt dir Adobe InDesign in der Struktur-Ansicht das importierte XML an:


![ID XML, importierte Struktur](./img/idxml-xmlstructure.png)

Wenn du die Tags mit jenen aus der XML-Datei `index.xml` vergleichst, stellst du fest, dass Adobe InDesign die Transformation wie gewünscht durchgeführt hat. Das Wurzelelement heisst nun `Root` und nicht mehr `items` und jedes `item`-Tag enthält ein `image` mit einer Referenz auf ein Bild im Attribut `href`.

***Wichtig: Adobe InDesign kann Bilder nur dann erkennen, wenn deren Referenz in einem Attribut mit dem Namen `href` gespeichert sind. Wenn der Pfad auf das Bild einfach im Tag `image` liegen würde, so wie zum Beispiel der Name des Autors im Tag `Name`, würde Adobe InDesign das Tag nicht als `Image` markieren. Diese «Markierung» erkennst du daran, dass das Tag `image` ein anderes Icon erhalten hat.***

Als nächstes wollen wir validieren, dass unsere Transformation wirklich korrekt ist. Dafür machen wir einen Doppelklick auf das oberste Element in der Struktur-Ansicht. Es sollte den Namen `DOCTYPE Root` tragen. Adobe InDesign zeigt dir in einem Dialog, der sich danach öffnet, dass es die DTD korrekt laden konnte:

![ID XML, DTD geladen](./img/idxml-dtdloaded.png)

 Das war möglich, weil wir in der Transformation mit `doctype-system` die DTD der Zielstruktur hinzugefügt haben. In manchen XML-Dateien fehlt diese Referenz. Dann musst du sie via *DTD laden* manuell hinzufügen:

![ID XML, DTD mauell laden](./img/idxml-loaddtd.png)

Damit wir die Struktur validieren können klickst du nun auf *Validate from Root Element* im Flyout-Menü der Struktur-Ansicht:

![ID XML, validate DTD](./img/idxml-validatedtd.png)

Wenn dir Adobe InDesign unterhalb des Strukturbaums keine Fehler ausgibt, ist alles OK (was es sein sollte):

![ID XML, keine Fehler gefunden](./img/idxml-noerrors.png)

## Elemente platzieren

Zoom als erstes so weit aus dem Dokument heraus, dass du den gesamten Infobereich siehst und erstelle eine Textbox ungefähr so gross wie die ***Infobox***:

![ID XML, neue Textbox](./img/idxml-newtextbox.png)

Lösche in der Struktur-Ansicht alle `item`-Elemente bis auf das aller erste:

![ID XML, `item`-Elemente löschen](./img/idxml-deleteitemelements.png)

Ziehe das **`Root`**-Element über den Textrahmen:

![ID XML, `Root` platzieren](./img/idxml-dragroot.png)

Nachdem du das gemacht hast, zeigt dir Adobe InDesign den Inhalt des ersten Datensatzes an.

***Warum machen wir das so? Dazu musst du wissen: Adobe InDesign hat ein Problem beim Platzieren von Bildern aus einer XML-Struktur. Wenn das Bild, das als verankertes Objekt im Textrahmen platziert wird, grösser ist als die Montagefläche (zum Beispiel 1 Meter breit), zeigt dir Adobe InDesign auf ewig Übersatztext an. Du hast keine Möglichkeit, die Parameter der Platzierung zu bearbeiten – oder uns ist keine bekannt. Deshalb ist es wichtig, dass du die Bilder, die du platzieren willst, vorab ungefähr auf die richtige Grösse (in Millimeter, nicht in Pixel) zuschneidest. Unsere Bilder sind auf das Format A4 zugeschnitten.***

Klicke nun in das Textfeld und öffne den *Text Editor* in Adobe InDesign über *Bearbeiten* > *im Textmodus bearbeiten*. Falls du diesen nicht kennst: Im *Plain Text Editor* von Adobe InDesign lassen sich Tags und Inhalte ohne Design, also in reiner Textform, bearbeiten:

![ID XML, Text Editor öffnen](./img/idxml-opentexteditor.png)

Als nächstes formatieren wir die Platzierung der XML-Elemente und entfernen überflüssige Abstände:

* Lösche die Abstände vor dem `Root`-Tag
* Lösche die Abstände vor den Tags innerhalb von `item`
* Füge nach dem schliessenden `photo` mit `Return` eine Leerzeile ein, falls es dort noch keine hat
* Lösche allfällige Leerzeilen am Ende des Dokuments

*Ansicht Text Editor vor korrekturen – beachte die gelb markierten Leerzeilen:*

![ID XML, Text Editor vor Korrekturen](./img/idxml-texteditorbefore.png)

*Ansicht Text Editor nach Korrekturen:*

![ID XML, Text Editor nach Korrekturen](./img/idxml-texteditorafter.png)

Schliesse den *Text Editor* wieder und skaliere die Textbox (und das verankerte Bild) nun auf deine Seitenränder. Wähle das verankerte Bild an und öffne über einen Rechtsklick und dann *Anpassen* > *Textrahmenoptionen*  den Dialog mit den Anpassungsoptionen:

![ID XML, Textrahmenoptionen](./img/idxml-fittingoptions.png)

Aktiviere in diesem den Haken bei *Automatisch Anpassen* und wähle die Option *Inhalt proportional anpassen*. ***Bevor*** du den Dialog schliesst, stellst du den *Beschnittbetrag* noch überall auf `0`. Danach bestätigst du mit *OK*:

![ID, XML, Einstellungen Textrahmenoptionen](./img/idxml-fittingconf.png)

***Achte gut darauf, dass du auf der Seite keinen Übersatztext hast.*** Solltest du trotzdem welchen haben, hast du irgendwo noch einen Zeilenumbruch zu viel.

Füge eine zweite Seite ein, erstelle einen Textrahmen auf die inneren Seitenränder (gleiche Grösse wie oben) und verkette die Textrahmen von Seite 1 und Seite 2 miteinander:

![ID XML, zweite Seite in Template einfügen](./img/idxml-tplsecondpage.png)

Speichere nun das Dokument als ***Adobe-InDesign-Template*** ab. In unserem Fall ***musst*** du das Template neben der Datei `Index.xml` in unserem Projektverzeichnis abspeichern. Andernfalls kann Adobe InDesign später die Dateipfade zu den Bildern nicht mehr auflösen!

![ID XML, Template speichern](./img/idxml-savetpl.png)

***Schliesse nun das erstellte Template!***
