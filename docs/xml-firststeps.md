# Erste Schritte mit XML

In den nächsten Abschnitten werden wir ein XML-Dokument aufbauen und Schritt für Schritt durch Themen wie «die Validierung» und «die Transformation» navigieren. Dazu brauchst du die VSCodium-Extension *XML* (von Red Hat). Falls du diese noch nicht installiert hast, siehst du in der [Einführung](./einfuehrung.md), wie das geht. Als Endprodukt werden wir eine Struktur aus Bildern und Texten haben, die wir anschliessend am Beispiel von Adobe InDesign verarbeiten.

## Ein XML-Dokument anlegen

Ein XML-Dokument trägt in der Regel die Erweiterung `*.xml`. Um ein solches Dokument ablegen zu könenn, legst du als erstes einen neuen Ordner an (auf dem Schreibtisch oder an einem Ort deiner Wahl). Bennene diesen zum Beispiel mit `xml`.

In VSCodium kannst du im Menü *File* > *New* ein neues Dokument erstellen und dieses mit *Save as* speichern. Bennene dieses zum Beispiel mit `data.xml`. Du kannst es jedoch benennen wie du willst – solange das Dokument die Dateinamenserweiterung `*.xml` hat.

Nach dem Speichern des Ordners und des Dokumentes, kannst du (wieder in VSCodium) in der ***Activity Bar*** auf das oberste Icon mit den Dokumenten klicken und dann den Ordner via *Open Folder*  öffnen.

![Ordner in VSCodium öffnen](./img/xml-open-dir.png)

 1. Activity Bar, Icon Dokumenten
 2. Icon «Open Folder»

Anschliessend sollte dein Workspace so aussehen:

![Ordner in VSCodium geöffnet](./img/xml-opened-dir.png)

Nun können wir die Struktur unseres XML-Dokuments aufbauen. Als erstes platzieren wir die Deklaration:

```xml
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
```

Danach erstellen wir den Wurzelknoten, das erste Element im XML-Baum:

```xml
<root></root>
```

Dein XML-Dokument sollte nun so aussehen:

![Ein XML Dokument erstellen](./img/xml-root.png)

## Hinweise und Fehler erkennen

Die VSCodium-Extension *XML* (von Red Hat) hat einige hilfreiche Funktionen für dich bereit. Einige dieser Funktionen wollen wir jetzt etwas genauer anschauen. Eine vollständige Liste aller Funktionen findest du direkt in der Beschreibung der Extension selbst.

### Hinweise

Wenn du im Editor drei kleine graue Punkte siehst und auf der rechten Seite ein grauer Balken sichtbar wird, heisst das, dass die XML-Extension einen Hinweis für dich bereit hat. Du kannst diesen abrufen, indem du mit der Maus über die drei kleinen grauen Punkte fährst.

Im aktuellen Dokument siehst du die drei kleinen grauen Punkte unter dem `r` des öffnenden `<root>`-Tags. Wenn du mit der Maus darüber fährst, informiert dich die XML-Extension darüber, dass du keine DTD und kein XML-Schema als Referenz angegeben hast:

![XML Hint](./img/xml-hint.png)

Diesem Hinweis werden wir etwas später nachgehen.

### Fehler

Wenn du einen Tippfehler machst, wird dir das mit einer gewellten roten Linie angezeigt. Du kannst das testen, indem du die schliessende Spitzklamer vom schliessenden `</root>`-Tag entfernst: `</root`.

Wenn du das machst, erscheint – nebst der gewellten roten Linie – im Editor auf der rechten Seite ein roter Balken. Die gewellte Linie steht im aktuellen Beispiel nicht unter dem falschen, schliessenden `</root` sondern, unter dem öffnenden `<root>`-Tag. Das hat damit zu tun, dass der Fehler technisch gesehen beim öffnenden Tag und nicht beim schliessenden Tag auftritt. Im Augenblick der Verarbeitung versucht der (Test-)Parser der XML-Extension, ein schliessendes Tag für dein öffnendes `<root>` zu finden. Das ist nicht möglich, weil dein schliessendes `</root` zur Zeit nicht geschlossen ist:

![XML Error](./img/xml-error.png)

Nachdem wir nun einen Fehler simuliert haben, ist es wichtig, dass du dein XML-Dokument wieder komplettierst und dein schliessendes `</root`-Tag auf `</root>` korrigierst.
