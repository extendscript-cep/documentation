# Vorbereitendes für die nächsten Übungen

In den nächsten Abschnitten bereiten wir eine XML-Datei für die weitere Verwendung in Adobe InDesign auf. Wenn du nicht an Adobe InDesign interessiert bist, ist dies kein Problem. Die Aufbereitung der XML-Datei ist generell strukturiert. Erst der eigentliche Adobe-InDesign-Import ist dann programmbezogen. Bisher haben wir mit Ausschnitten aus dieser XML-Datei gearbeitet. Jetzt ist es Zeit, einen Blick auf das ganze Projekt zu werfen. Du kannst es <a href="/examples/xslt.zip" target="_blank" download>***hier***</a> herunterladen.

## Vorgehen

* **Schritt 1:** Wir bereiten das XML-Dokument des Projekts für Adobe InDesign auf. Dazu führen wir mehrere XML-Transformationen durch. Mit ihnen lernen wir XSLT kennen.

* **Schritt 2:** Wir importieren die aufbereitete XML-Datei in Adobe InDesign und erstellen einen Bilderkatalog. Jedes Bild in diesem Katalog bekommt einen Bildnachweis.

*Rechtliches: Alle Bilder stammen von <a href="https://unsplash.com/" target="_blank">unsplash.com</a>. Wir können sie verwenden, weil wir die Urheber korrekt erwähnen. Weitere Informationen findest du unter <a href="https://unsplash.com/license" target="_blank">unsplash.com/license</a>.*

## Projekt in Eclipse öffnen

Nachdem du das Projektverzeichnis (ZIP) heruntergeladen hast – <a href="/examples/xslt.zip" target="_blank" download>***hier***</a> nochmals der Link –, kannst du es entpacken. Wir haben es auf den Schreibtisch extrahiert. Der Verzeichnisbaum sollte so aussehen, dass du auf der obersten Ebene eine Datei mit dem Namen index.xml hast und daneben einen Ordner mit dem Namen `images`. Im Ordner `images` findest du eine Reihe von Bildern.

![XSLT, Project structure](./img/xslt-project-structure.png)

Nun ist es Zeit, Eclipse zu starten. Falls du das Programm Eclipse noch nicht installiert hast, kannst du das im Abschnitt [XPath](./xml-xpath.md) nachholen.

Sofern dich Eclipse beim Start nach dem Workspace fragt,  bestätige einfach mit dem *OK*-Button, ohne dass du den Pfad im Fenster veränderst. *Tipp: Wenn du die Checkbox bei `Use this as the default and don not ask again` aktivierst, wird dir die Meldung künftig nicht mehr angezeigt.*

![Eclipse Workspace](./img/xslt-eclipse-start.png)

Über *File* > *Open Projects from File System* öffnen wir jetzt unseren Ordner `xslt` in Eclipse.

![Eclipse, open XSLT](./img/xslt-eclipse-open.png)

Im nächsten Dialog stellst du via den Button *Directory* den Pfad auf den Ordner `xslt` ein und bestätigst deine Eingabe mit *Finish*.

![Eclipse, set Project path](./img/xslt-eclipse-path.png)

Eclipse legt dir nun ein neues Projekt an und zeigt dir die Ordnerstruktur im *Package Explorer*. Klicke jetzt doppelt auf unser Arbeitsdokument `index.xml` um es im Editor von Eclipse zu öffnen.

![XSLT, Eclipse open document](./img/xslt-eclipse-docopen.png)

Das Wurzelelement des XML-Dokuments ist `items`. Innerhalb von `items` siehst und eine Reihe von `item`-Elementen. Jedes `item`-Element besteht aus genau einem Element mit dem Bezeichner `name`, `author` und `photo`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<items>
  <item>
    <name>Francois Hoang</name>
    <author>https://unsplash.com/@aoirostudio</author>
    <photo>https://unsplash.com/photos/2x6VHSRVqSA</photo>
  </item>
  <item>
    <name>Lee Soo hyun</name>
    <author>https://unsplash.com/@arisu_view</author>
    <photo>https://unsplash.com/photos/YqwDtuwxKfc</photo>
  </item>
  <!-- weitere <items> -->
</items>
```

## Aufgaben

Was uns für den späteren Import bei Adobe InDesign fehlt ist:

1. Unserer Erfahrung nach ist es eine gute Idee, das Wurzelelement für Adobe InDesign von `<items>` in `<Root>` umzubenennen. Das ist zwar nicht zwingend erforderlich, hilft aber in gewissen Situationen Fehler in der Verschachtelung importierter XML-Strukturen zu vermeiden.
2. Innerhalb der `item`-Tags gibt es keinen Link-Verweis auf das Bild. Einen solchen müssen wir hinzufügen, damit Adobe InDesign später den Pfad zur Datei findet.
3. Wir brauchen eine DTD, damit wir das XML nach dem Import in Adobe InDesign validieren können.

Diese drei Aufgaben nehmen wir uns als Beispiel, um das Transformieren von XML-Dateien kennenzulernen.
