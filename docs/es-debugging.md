# Adobe ExtendScript Debuggen

In diesem Abschnitt schauen wir uns an, wie du Skripte analysieren und auf Fehler untersuchen kannst. Wir verwenden dafür das Skript aus dem [letzten Abschnitt](./es-firststeps.md). Falls du direkt in diesem Abschnitt einsteigst, kannst du es <a href="/examples/es-dialog.jsx" target="_blank" download>hier</a> herunterladen und anschliessend in VSCodium öffnen.

Falls du VSCodium oder die notwendigen Extensions noch nicht installiert hast, kannst du das [hier](./einfuehrung.md) nachholen.

## VSCodium bereit machen

Als erstes solltest du kontrollieren, dass unten rechts JavaScript steht und die Target Application gesetzt ist:

![VSCodium einrichten](./img/es-vscodium-start.gif "VSCodium einrichten")

## Breakpoints

Ein Breakpoint ist ein Haltepunkt in deinem Skript. Wenn der Interpreter an einen Breakpoint kommt, wird dein Skript angehalten und du kannst seinen Zustand genau untersuchen. Wichtig ist, dass du die Breakpoints am richtigen Ort setzt. Angenommen du hast folgendes Skript:

```javascript
function main() {
  var d = app.documents.add();
  return d;
}

main();
```

Wenn du nun die Variable `d` untersuchen willst, musst du deinen Breakpoint nach dieser Variable setzen, zum Beispiel auf der Zeile von `return d;`. Der Interpreter hält am Zeilenanfang an. Das heisst, wenn du deinen Breakpoint genau bei der Variable `d` setzt, hat der Interpreter diese noch nicht abgearbeitet weil er noch am Zeilenanfang steht. Entsprechend kannst du sie noch nicht analysieren.

Wir setzen nun bei unserem Skript auf den Zeilen `13`, `25` und `49` einen Breakpoint, in dem wir links von der Zeilennummer in die weisse Fläche klicken:

![VSCodium Breakpoints setzen](./img/es-dialog-breakpoints.gif "VSCodium Breakpoints setzen")

Während einer Debugging-Session werden dir oben in der Mitte sechs Steuerungstasten angezeigt. Sie helfen dir, durch deinen Code zu navigieren:

![VSCodium Steuerungstasten](./img/vscodium-debug-controls.png "VSCodium Steuerungstasten")

Von links nach rechts:

* ***Continue*** (blau): Skript bis zum nächsten Breakpoint ausführen, wenn kein Breakpoint mehr kommt, bis zum Schluss ausführen;
* ***Step over*** (blau): Etwas überspringen, zum Beispiel eine Schleife, so dass du nicht alle Schritte klicken musst;
* ***Step into*** (blau): Mit diesem Steuerelement kannst du in Funktionen hineinhüpfen, auch wenn du in diesen keinen Breakpoint gesetzt hast.
* ***Step out*** (blau): Aus Funktionen hinaushüpfen;
* ***Restart*** (grün): Eine Debugging-Session erneut starten;
* ***Stop*** (rot): Debugging-Session abbrechen.

Jetzt, da du deine Breakpoints definiert hast, kannst du mit dem Debuggen beginnen. Während dem Debugging wird der Debugger zwischen VSCodium und Adobe Programm (hier InDesign) hin und her springen. Manchmal gelingen diese Sprünge nicht und du musst die Applikation von Hand wechseln. In der nachfolgenden Kurzanimation siehst du gut, wie du das machen kannst.

Als nächstes klickst du auf das Icon mit dem Play-Button und dem Käfer auf der linken Seite in der ***Activity Bar*** und gehst auf *Run and Debug*. VSCodium fragt dich mit welchem Debugger du arbeiten willst. Hier klickst du auf *ExtendScript Debug*. Danach startet deine Debug Session.

![VSCodium Start Debugging-Session](./img/es-dialog-debug-controls.gif "VSCodium Start Debugging-Session")

## Elemente untersuchen

Wir starten eine neue Debugging-Session, so wie wir das oben gemacht haben. Das Skript hält beim ersten Breakpoint an. Nun kannst du untersuchen, was deine Variablen zu diesem Zeitpunkt für einen Wert haben. Dafür klickst du auf der linken Seite in die ***Side bar***. Dort gibt es zwei Einträge ***Global*** und ***Local***. ***Global*** zeigt dir alle verfügbaren globalen Elemente. Die globalen Elemente werden definiert durch die Laufzeitumgebung und den globalen Namensraum. ***Local*** zeigt dir dagegen die Werte deiner Variablen an. Hier sehen wir zum Beispiel beim dritten Breakpoint, dass die Variable `os` den Wert `Macintosh` hat. Das Skript lief für diese Demo also auf einem Apple macOS.

![VSCodium Degugging, Elemente untersuchen](./img/es-dialog-inspect.gif "VSCodium Degugging, Elemente untersuchen")

## Elemente überprüfen und ausprobieren

Während dem Debugging kannst du auch Elemente evaluieren oder Werte über einen längeren Zeitpunkt verfolgen. Entferne jetzt die Breakpoints auf den Zeilen `13`, `25` und `49`, indem du auf die roten Punkte klickst. Danach fügst du auf den Zeilen `20` und `25` zwei neue Breakpoints hinzu, indem du links neben den Zeilennummern auf die weisse Fläche klickst:

![VSCodium Degugging, neue Breakpoints setzen](./img/es-dialog-eval-breakpoints.gif "VSCodium Degugging, neue Breakpoints setzen")

Starte nun eine neue Debugging-Session. Wenn du den ersten Breakpoint erreichst, kannst du in der ***Side Bar*** unter *watch* die Elemente hinzufügen, die du untersuchen willst.

Wir wollen drei Elemente untersuchen

* `i`: Wo steht unsere Zählervariable in der `for`-Schleife?
* `config[i].name`: Gibt es eine Konfiguration mit einem Namen bei dem betreffenden Index gemäss dem Wert von `i`?
* `options`: Welche Werte hat unser `options`-Array, das später die Auswahlliste des Dropdowns bestimmt?

Nachdem du deine Werte bei `watch` eingefügt hast, kannst du mit Continue (Play Button der Debug-Steuerelemente), deine Debugging-Session weiterführen. Wir navigieren nun durch jeden Schleifendruchlauf und haben haben einen letzten Breakpoint, kurz bevor der Dialog erstellt wird. Das gibt uns die Möglichkeit, nochmals alle untersuchten Elemente in Ruhe zu studieren, bevor wir weitergehen.

![VSCodium Degugging, Elemente untersuchen](./img/es-dialog-eval-watch.gif "VSCodium Degugging, Elemente untersuchen")
