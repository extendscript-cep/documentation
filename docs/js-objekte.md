# Objekte

Bei den Datentypen `String`, `Number` oder `Array` gibt JavaScript die Struktur zu einem grossen Teil vor. Dagegen bist du bei der Definition von (eigenen) Objekten relativ frei. Das ist möglich, weil die Basis-Klasse für Objekte (`Object`), auch die Basis-Klasse für alle anderen Datentypen ist. Das heisst, die Basis-Klassen von `String` oder `Array` erben immer von der Basis-Klasse für Objekte.

Eigene Objekte definierst du mit geschweiften Klammern `{}`. Im Innern der Klammern folgt eine Liste von Schlüssel-Werte-Paaren, die durch einen Doppelpunkt (`:`) getrennt werden. Die Schlüssel dienen später für den Zugriff, so wie der (implizite) Index bei den Arrays. Die Werte können alle in JavaScript verfügbare Objekttypen enthalten. Beachte, dass jedes Schlüssel-Werte-Paar mit einem Komma (`,`) abgeschlossen wird – ausser das Letzte: Dort ist kein Komma mehr nötig.

```text
           Zuweisungsoperator   
                │      │         
                │      │  Wert   
                ▼      │    │    
     var person = {    ▼    ▼    
      ▲      ▲     name: 'Hans', 
      │      │     alter: 28,    
      │      │     ort: 'Zürich',
      │      │     plz: 8008     
      │      │    };▲            
      │      │      │            
      │      │      │            
      │      │  Schlüssel        
      │      │                   
      │      │                   
      │ Bezeichner               
      │                          
      │                          
  Schlüsselwort
```

Die Schlüssel-Werte-Paare von Objekten werden entweder als *Eigenschaften* der *Methoden* bezeichnet. Die Eigenschaften haben einen konkreten Wert. Im obigen Beispiel haben wir zum Beispiel ein Objekt mit den vier Eigenschaften `name`, `alter`, `ort` und `plz` erstellt. Dagegen sind *Methoden* die Funktionen eines Objekts. Das heisst, sie machen «etwas» und speichern nicht «bloss» einen Wert ab. Das nächste Beispiel zeigt dir, wie sich *Eigenschaften* und *Methoden* unterscheiden. `month` und `day` sind *Eigenschaften*. Dagegen ist `calc` eine Funktion, die eine übergebene Zahl (`param`) multipliziert.

```text
      Zuweisungsoperator
              │
              │
              ▼
     var data = {
      ▲   ▲  month: 'July',           >>> Eigenschaft
      │   │  day: 15,                 >>> Eigenschaft
      │   │
      │   │
      │   │  calc: function(param){   >>> Methode
      │   │    return param * param;
      │   │        }
      │   │     };
      │   │
      │   │
      │ Bezeichner
      │
      │
Schlüsselwort
```

## Syntax

Bisher haben wir jede Zeile mit einem Semikolon (`;`) abgeschlossen. Das Semikolon zeigt dem Interpreter an, dass das Ende einer Anweisung erreicht ist und er diese nun ausführen kann. Streng genommen könnten wir das Semikolon auch weglassen und nur einen Zeilenumbruch machen. Bei komplexeren Skripten kann das allerdings zu Problemen führen. Wenn die Zeilenumbrüche nicht genau stimmen, wird uns der Interpreter einen Fehler anzeigen.

Wer ganz sicher sein will, fügt nach jeder Anweisung ein Semikolon ein. Wir werden im weiteren Verlauf eine Mischform anwenden, wie sie in den meisten Skripten eingesetzt wird. Variablen werden wir immer mit einem Semikolon abschliessen. Bei Funktionen oder Schleifen werden wir das Semikolon weglassen.

## Übung 1, ein Objekt erzeugen

Objekte sind, wie Arrays, komplexe Datentypen. In dieser Übung wollen wir herausfinden, ob wir unser Objekt korrekt erzeugt haben. Für ein korrektes Ergebnis müssen wir deshalb den Typ nicht nur mit `typeof` abfragen, sondern auch über den `constructor.name`.

### Aufgabe

Wir erstellen ein JavaScript-Objekt mit zwei Eigenschaften und ermitteln, ob dieses den korrekten Datentyp zugewiesen bekommen hat.

```javascript
var data = {
  month: 'July',
  day: 15
};

console.log(typeof data);
console.log(data.constructor.name);
```

### Lösung

![RunJS, erstes Objekt erstellen](./img/runjs-object-first.png "Mit RunJS ein Objekt erstellen.")

Das Ergebnis von `typeof` zeigt uns wiederum nur `object` an. Dagegen ist das Ergebnis von der Konstruktor-Prüfung `Object`. Ganz wichtig ist, in diesem Zusammenhang, der feine Unterschied der Gross-Klein-Schreibung. Das kleine `object` zeigt uns den Datentyp der aktuellen Objekt-Instanz an. Das heisst, es reflektiert den Datentyp, ***nach dem*** der Interpreter das Objekt von der Basis-Klasse abgeleitet hat. Im Gegenzug zeigt uns das grossgeschriebene `Object` an, von welcher Basis-Klasse abgeleitet und nach welchem «Bauplan» das Objekt zusammengebaut wurde. Dazu muss du wissen, dass in JavaScript für die Definition von Klassen (= Bauplänen) und Instanzen eine Namenskonvention von Gross- und Klein-Schreibung angewendet wird, damit du Klassen und Instanzen besser unterscheiden kannst. Die Klassen-Bezeichner (= Namen) werden immer grossgeschrieben, während die Bezeichner der abgeleiteten Objekte vom Programmierenden kleingeschrieben werden sollten.

## Übung 2, auf Eigenschaften und Methoden zugreifen

Bei Objekten kannst du über die Punktnotation (`.`) auf Eigenschaften und Methoden zugreifen.

### Aufgabe

Wir erstellen ein Objekt und greifen auf seine Eigenschaften und Methoden zu.

```javascript
var data = {
  month: 'July',
  day: 15,
  calc: function(param) {
    return param * param;
  }
};

data.month;
data.day;
data.calc(5);
```

### Lösung

![RunJS, auf Eigenschaften & Methoden zugreifen](./img/runjs-object-access.png "Wie du auf Eigenschaften und Methoden von Objekten zugreifst.")

In diesem Beispiel müssen wir nicht (mehr) mit `console.log()` arbeiten, weil uns *RunJS* das Ergebnis automatisch zurückgibt. Es wäre aber auch richtig, die letzten drei Zeilen so zu schreiben:

```javascript
console.log(data.month);
console.log(data.day);
console.log(data.calc(5));
```

Bei `data.calc(5)` führst du deine erste Methode respektive Funktion aus. Du übergibst das Argument `5`, welches in der Funktion multipliziert wird. Du kannst gerne auch noch andere Ziffern ausprobieren.

## `this`

Das Schlüsselwort `this` hat in JavaScript eine spezielle Rolle. Es zeigt auf das Objekt respektive den Kontext, in dem es angewendet wird. Es kann zum Beispiel nützlich sein, um Eigenschaften eines Objekts zu verändern oder in einer Methode zu verwenden:

```javascript
var module = {
    attribute : 'Der Wert ist ',
    method : function (param) {
        return this.attribute + param + '.';
    }
};

module.method(5);
```

In diesem Beispiel haben wir ein Objekt mit dem Namen `module`. Dieses Objekt hat eine Eigenschaft `attribute` und eine Methode mit dem Namen `method`. Diese Methode ist eine Funktion mit dem Rückgabewert `this.attribute + param + '.'`. Wenn wir die Methode mit dem Argument `5` über `module.method(5)` aufrufen, greift die Methode via `this.attribute` auf den Wert der Eigenschaft `attribute` zu und führt diesen mit dem Wert des übergebenen Arguments zu einem Satz zusammen.

![RunJS, mit this arbeiten](./img/runjs-object-this.png "Mit This auf Elemente zugreifen.")

Die wesentlichen Konzepte hinter `this` sind *Closures* und *Scoping*. Sie schaffen eine Art Namensräume oder «Zugriffsräume» in deinem Skript, die gerne als *Kontext* bezeichnet werden. `this` ist «kontextsensitiv». Das heisst, es kommt darauf an, wo du es ausführtst. Wenn du `this` in einem Objekt anwendest, so wie wir gerade eben, zeigt es auf das Objekt. Wenn du`this` allerdings ausserhalb von Objekten oder Funktionen anwendest, zeigt es auf den globalen Kontext der JavaScript-Umgebung.

Für uns ist `this` in den nächsten Schritten noch nicht so wichtig. Wenn du dich bereits jetzt etwas genauer einlesen willst, empfehlen wir dir die Beiträge zu [*Scoping*](http://www.mediaevent.de/JavaScript/globale-lokale-variablen.html) und [*Closures*](http://www.mediaevent.de/JavaScript/globale-lokale-variablen.html) von [mediaevent.de](http://www.mediaevent.de/). Für ganz exakte, allerdings meist englische Erklärungen, sind die Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/) ratsam.
