# XSL und XSLT

Die *Extensible Stylesheet Language*, kurz *XSL*, ist eine erweiterbare Formatsprache. Sie bildet die Grundlage für die Anwendung von XSLT und XSL-FO.

## XSL Transformations (XSLT)

Die *XSL Transformations*, kurz *XSLT*, sind der wahrscheinlich wichtigste Teil von XSL. In *XSLT* kannst du Regeln definieren, mit denen du XML-Dokumente transfromieren kannst. «Transformieren» meint in diesem Kontext «in eine andere Form bringen». Du kannst mit *XSLT* zum Beispiel ein XML-Dokument in ein HTML-Dokument umwandeln.

Die *XSLT*-Regeln werden in so genannten *Templates* abgelegt. Diese Templates sind selbst auch XML-Dokumente. Sie werden vom XSL-Prozessor eingelesen und später auf das zu transformierende XML-Dokument angewendet. Der Transformationsprozess arbeitet aber nicht direkt auf dem XML-Dokument sondern auf einer Repräsentation des Document Object Models, kurz DOM. Das DOM ist die Baumstruktur, wo alle Knoten deines XML-Dokuments aufgelistet werden. Diese Überführung in den DOM-Baum ermöglicht es, dass zu jedem Zeitpunkt der Laufzeit auf alle Elemente und Daten zugegriffen werden kann und das Dokument nicht sequenziell abgearbeitet werden muss.

Was im obigen Abschnitt etwas technisch tönt, ist im Prinzip ganz einfach: Deine Transformationen werden nicht Schritt-für-Schritt sondern parallel abgearbeitet. Das kann  zu unerwünschten Resultaten führen, wenn du nicht daran denkst. Wenn du zum Beispiel mehrere Transformationen auf eine Gruppe von XML-Elementen anwenden willst, musst du sie so schreiben, dass es keine Rolle spielt, in welcher Reihenfolge sie abgearbeitet werden.

## XSL Formatting Objects (XSL-FO)

Im weiteren Verlauf wollen wir uns auf XSLT fokussieren und eine XML-Datei für den Import in Adobe InDesign aufbereiten.
