# Document Type Definition (DTD)

In der Dokumentenbeschreibung, in Englisch die *Document Type Description* (kurz *DTD*), kannst du die Struktur eines XML-Dokuments definieren. Mit ihr kannst du definieren, welche Struktur dein XML-Dokument haben muss. Das ist in vielen Situationen hilfreich oder gar erforderlich. Nehmen wir an, ein Kunde schickt dir ein XML-Dokument mit Finanzdaten. Alle diese Finanzdaten werden im Tag `<total>` summiert. Weil der Kunde beim Export aber einen Fehler gemacht hat, fehlen alle Tags `<total>` im gelieferten Dokument. Wenn du dieses Dokument nun auf die DTD des Kunden prüfst, wird dir der Parser sagen, dass da Dinge fehlen und du die Daten nicht weiter verarbeiten kannst. Mit dieser Fehlermeldung kannst du bei deinem Kunden vorstellig werden und melden, dass der Export fehlerhaft sei und du die Daten nicht weiter verarbeiten könnest.

## DTD erstellen

Du kannst eine DTD direkt ins XML-Dokument schreiben oder als externes Dokument laden. Wir werden nun als erstes die DTD ins Dokument schreiben und sie später in ein eigenes Dokument auslagern.

Die DTD wird über den Schlüssel `!DOCTYPE` eingeführt, das von spitzen Klammern `<>` umgeben wird. Wir fügen nun eine DTD direkt nach der Deklaration ein und definieren, dass unser Wurzelelement, das oberste Element, `data` heissen muss:

```xml
<!DOCTYPE data [
 <!ELEMENT data (items)*>
]>
```

Dein XML-Dokument sieht nun so aus:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!DOCTYPE data [
 <!ELEMENT data (items)*>
]>
<data>
  <items>
    <item>
      <name>Peter Muster</name>
      <author>https://example.com/petermuster</author>
      <photo>https://example.com/photos/pm1234</photo>
    </item>
  </items>
</data>
```

Wenn wir die DTD im XML-Dokument definieren, müssen wir als erstes das Wurzelelement definieren. Das machen wir, indem wir direkt nach `!DOCTYPE` den Bezeichner von `data` angeben, gefolgt von eckigen Klammern `[]`. Innerhalb dieser eckigen Klammern definieren wir als erstes, dass `data` nur Elemente mit dem Bezeichner `items` enthalten darf:

```xml
<!DOCTYPE data []>
```

Nachdem wir diese erste Definition gesetzt haben, legen wir in den eckigen Klammern `[]` als erstes die Definition für das `data` Element selbst fest. In dieser geben wir dem Parser an, dass unser `data`-Element ausschliesslich Kindelemente mit dem Bezeichner `items` haben darf. Die Definition der Kindelemente wird in runde Klammern `()` eingefasst. Eine besondere Betrachtung verdient der Stern `*` neben der schliessenden runden Klammer. Er besagt, dass beliebig viele `items` in `data` vorkommen dürfen:

```xml
<!DOCTYPE data [
  <!ELEMENT data (items)*>
]>
```

Aus den vergangenen Abschnitten können wir folgende Muster im Kopf behalten: Für die Definition …

```text
<!DOCTYPE [Wurzelelement] [
  [Elementdefinitionen]
]>
```

… und für die Spezifikation:

```text
<!ELEMENT [Bezeichner] ([Unterelemente|Inhalt])>
```

Mittlerweile wird dir VSCodium eine ganze Reihe von Fehlern anzeigen. Vermutlich haben alle Elemente ausser `data` eine gewellte rote Unterlinie:

![XML, DTD Fehler](./img/xml-dtd-errors.png)

 Das kommt daher, dass wir mit der DTD eine Referenz auf die erforderliche Struktur eingefügt haben. Eine solche Referenz muss immer vollständig sein. Es funktioniert für XML respektive den Parser nicht, dass du nur einen Teil deiner Struktur definierst. Wenn du eine Struktur haben willst, musst du dich genau festlegen – für alle Elemente. Das wollen wir nun tun, indem wir für die weiteren Elemente Definitionen anlegen:

```diff
<!DOCTYPE data [
  <!ELEMENT data (items)*>
+   <!ELEMENT items (item)?>
+   <!ELEMENT item (name, author, photo)>
+   <!ELEMENT name (#PCDATA)>
+   <!ELEMENT author (#PCDATA)>
+   <!ELEMENT photo (#PCDATA)>
]>
```

Dein ganzes XML-Dokument sieht nun so aus:

 ```xml
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <!DOCTYPE data [
   <!ELEMENT data (items)*>
   <!ELEMENT items (item)?>
   <!ELEMENT item (name, author, photo)>
   <!ELEMENT name (#PCDATA)>
   <!ELEMENT author (#PCDATA)>
   <!ELEMENT photo (#PCDATA)>
 ]>
 <data>
   <items>
     <item>
       <name>Peter Muster</name>
       <author>https://example.com/petermuster</author>
       <photo>https://example.com/photos/pm1234</photo>
     </item>
   </items>
 </data>
 ```

 Mit `<!ELEMENT items (item)?>` definieren wir, dass `items` mindestens ein `item`-Tag enthalten muss. Entscheidend für diese Regel ist das Plus-Symbol `+`. Es legt fest, dass ein Element `1 bis n` Kindelemente haben muss.

Mit `<!ELEMENT item (name, author, photo)>` definieren wir, dass das Element `item` exakt drei direkte Kindelemente haben darf – und das genau in der Reihenfolge `name`, `author` und `photo`. Es wäre also nicht erlaubt, dass in `item` zum Beispiel das Element `author` zuerst kommt:

![XML, wrong sequence](./img/xml-wrong-sequence.png)

Für die Elemente `name`, `author` und `photo` legen wir fest, dass sie `#PCDATA` enthalten dürfen. `#PCDATA` steht für *Parsed Character Data* und kennzeichnet *Textinhalt*. Das heisst, diese Element dürfen keine weiteren Kindelemente mehr enthalten – es sein denn, sie sind in einen `CDATA`-Abschnitt eingeschlossen. `CDATA` steht für *Character Data* und kennzeichnet Zeichendaten, die vom Parser nicht analysiert werden. Er versteht sie stattdessen als *Textinhalt*. So ist es zum Beispiel möglich, in einem XML-Tag HTML-Daten zu versorgen.

## DTD auslagern

Im nächsten Schritt lagern wir unsere DTD in ein eigenständiges Dokument aus. Damit wird sie unabhängig und kann besser als Kontrollinstanz verwendet werden. Wenn dir ein Kunde ein neues XML-Dokument zuschickt, musst du es nicht als erstes öffnen und prüfen, ob die DTD noch gleich ist. Du kannst es stattdessen direkt validieren und deinem Kunden sofort mitteilen, ob sich auf der XML-Struktur etwas verändert hat, worüber du nicht informiert bist oder was falsch ist.

Erstelle nun neben deinem XML-Dokument im gleichen Ordner ein neues Dokument mit der Endung `.dtd`. Wir verwenden hier einfachheitshalber den selben Namen wie beim XML-Dokument und nennen es `data.dtd`. In VSCodium kannst du über *File* > *New File* ein neues Dokument erstellen.

![XML, neue DTD](./img/xml-dtd-new.png)

Als nächstes ***kopierst*** du die folgenden Zeilen in dein neues Dokument und ***speicherst*** es:

```text
<!ELEMENT data (items)*>
<!ELEMENT items (item)?>
<!ELEMENT item (name, author, photo)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT author (#PCDATA)>
<!ELEMENT photo (#PCDATA)>
```

Wenn du das gemacht hast wechselst du zurück in dein XML-Dokument. Bei uns heisst dieses `data.xml`. Noch weiss unser `data.xml` nichts vom neuen `data.dtd`-Dokument. Das ändern wir jetzt, indem wir als erstes in der Deklaration `standalone` auf `no` stellen:

```text
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
```

Nun kannst du deine Inline-DTD komplett löschen, in dem du seine Zeilen entfernst …

```diff
- <!DOCTYPE data [
-   <!ELEMENT data (items)*>
-   <!ELEMENT items (item)?>
-   <!ELEMENT item (name, author, photo)>
-   <!ELEMENT name (#PCDATA)>
-   <!ELEMENT author (#PCDATA)>
-   <!ELEMENT photo (#PCDATA)>
- ]>
```

… und anschliessend an der gleichen Stelle eine Referenz auf dein eigenständiges DTD-Dokument einfügst:

```text
<!DOCTYPE data SYSTEM "data.dtd">
```

Dein XML-Dokument sollte nun so aussehen …

![XML ohne DTD](./img/xml-dtd-file.png)

… und dein DTD-Dokument so:

![Neues DTD Dokument](./img/xml-dtd-dtd.png)

Die finalen Dokumente dieser Übung kannst du <a href="/examples/xml-dtd.zip" target="_blank" download>hier</a> herunterladen.

## DTD Cheat Sheet

Wir haben für dich einige wichtige Elemente der DTD-Syntax hier nochmals zusammengefasst. Ein umfassendes Beispiel findest du unter anderem bei <a href="http://www.cheat-sheets.org/sites/xml.su/dtd.html" target="_blank">cheat-Sheets.org</a>.

### Element Type Declaration

```xml
<!ELEMENT [Bezeichner] ([Unterelemente|Inhalt])>
```

### Allowable Contents

Zum Beispiel:

```xml
<!ELEMENT test (ANY)>
```

| Allowable Contents  | Definition                                                                         |
| ------------------- | ---------------------------------------------------------------------------------- |
| EMPTY               | Referenziert Tags, die leer sind (keinen Inhalt haben, vielleicht aber Attribute) |
| ANY                 | Alles ist erlaubt, solange die grundlegenden XML-Regeln eingehalten werden        |
| *Children elements* | Tags, die als Kindelemente vorkommen dürfen                         |
| *Mixed Content*     | Mehrere Tags oder `#PCDATA` sind im Tag erlaubt                              |

### Steuerzeichen zur Strukturierung

| Zeichen | Aufgabe                                                                                |
| ------- | -------------------------------------------------------------------------------------- |
| `,`     | Trennt mehrere Elemente, die in der angegebenen Reihenfolge vorkommen müssen         |
| &#124;  | *ODER* – kennzeichnet eine Auswahl von Elementen, von denen genau eines vorkommen muss |
| `?`     | Optional, `0 oder 1`                                                                   |
| `+`     | Mehrfach, `1 bis n`                                                                    |
| `*`     | Beliebig, `0 bis n`                                                                    |
