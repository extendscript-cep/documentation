# Variablen

## Syntax

                Zuweisungsoperator                     
	                     │                             
	                     │                             
	                     ▼                             
	    var abcdefghijkl = 'Ein Wert';                 
	     ▲       ▲              ▲                      
	     │       │              └───────┐              
	     │       │                      │              
	     │  Bezeichner                  │              
	     │                 Wert, Objekt oder Referenz (= Literal)  
	Schlüsselwort    

## Übersicht

Variablen werden mit dem Schlüsselwort `var` eingeleitet. Was nun folgt ist die sogenannte «Deklaration». In diesem Vorgang wird die Variable benannt und mit einem Wert belegt.

Mit dem *Bezeichner* definierst du einen Namen, unter dem die Variable im Skript später erreichbar ist. Der *Zuweisungsoperator* weist deiner Variable einen Wert zu. Dieser Wert kann ein Text, eine Zahl oder auch eine Referenz auf ein anderes Objekt des Skripts sein.

## Datentypen

Datentypen bestimmen den «Inhaltstyp» einer Variable. Sie werden durch JavaScript verwaltet. Das heisst, JavaScript weist einer Variable immer implizit einen Datentyp zu. Sowohl die Variable selbst als auch der Wert der Variable, der *Literal*, können ein Datentyp sein. In JavaScript ist das besonders wichtig, weil der Datentyp der Variable indirekt durch den Datentyp des Werts gesteuert wird.

Die wesentlichen Datentypen in JavaScript sind:

| Datentyp | Beschreibung 
|----------|--------------
|`Boolean`| `true` or `false`, wobei `0` = `false` und alle Werte ungleich `0` = `true`
|`Number` | `Number` speichert sowohl Ganzzahlen als auch Fliesskommazahlen. Als Trennzeichen bei Fliesskommazahlen setzt JavaScript den `.` ein. Die wissenschaftliche Notation ist über die Erweiterungen `e` und `E` möglich. In diesem Fall wird der Potenzwert hinter die Exponentialkennung notiert.
|`Object` | `Object` wird für das Speichern von Objekten verwendet. Es kann einen beliebigen Typ enthalten.
|`String` | Dieser Datentyp enthält eine Zeichenkette mit maxiamal 256 Positionen. Bei Bedarf können mehrere Zeichenketten mit dem `+` Operator verbunden werden. Das Verbinden mit weiteren Datentypen ist ebenfalls erlaubt. Diese werden allerdings bei einer Verknüpfung mit `String` bei der Ausgabe in einen solchen konvertiert.
|`Undefined` | `Undefined` beschreibt eine nichtdefinierte Situation. Eine Variable besitzt so lange diesen Wert, bis ihr nach dem Anlegen explizit ein Wert zugewiesen wird.
|`Null` | Dieser Sonderdatentyp entspricht der Situation, wenn ein Objekt noch keinen Wert hat, und entspricht keiner «keiner Bedeutung» oder `null`. Er unterscheidet sich somit von einer leeren Zeichenkette oder dem Literal `0`. Er kann zum Beispiel zurückgegeben werden wenn in einem Dialogfenster die `ABBRECHEN`-Schaltfläche betätigt wird.
|`typeof` | Mit diesem Datentyp kann in JavaScript der Typ einer Variable/eines Literals/eines Rückgabewerts überprüft werden.

## Übung 1, Typen von Variablen

### Aufgabe

Wir erstellen drei Variablen: eine vom Typ `String` (= Text), eine vom Typ `Number` und eine vom Typ `Boolean` (= wahr oder falsch). Danach prüfen wir mit `console.log()` und `typeof`, ob wir die richtigen Typen erwartet haben. `console.log()` weist die JavaScript-Umgebung, das ist im Augenblick *RunJS*, an, das Ergebnis auszugeben. `typeof` ist eine JavaScript-internes Schlüsselwort, das ein Objekt (unsere Variablen sind alles Objekte) auf seinen Typ prüft.


```javascript
var text = 'Ein Text für mich.';
var num = 1234;
var bool = true;

console.log(typeof text);
console.log(typeof num);
console.log(typeof bool);
```

### Lösung

![RunJS Variablen Typen](./img/runjs-js-vartypeof.png)

## Übung 2, Variablen kombinieren

Variablen vom gleichen Typ lassen sich kombinieren. In dieser Übung setzen wir einen Text zusammen und addieren zwei Ziffern.

### Aufgabe

Zuerst wollen wir einen Text aus mehreren Variablen zusammensetzen und mit `console.log()` ausgeben. In JavaScript werden Strings (= Text) mit dem Pluszeichen (`+`) zusammengesetzt respektive «addiert».

```javascript
var prename = 'Peter';
var lastname = 'Muster';

var message = 'Hallo ' + prename + " " + lastname;

console.log(message);
```

### Lösung

![RunJS Concat](./img/runjs-concat.png "Strings in JavaScript zusammensetzen")

### Aufgabe

Nachdem wir nun einen Text zusammengesetzt haben, führen wir eine einfache Addition aus. Auch hier werden die Werte der Variablen mit dem Pluszeichen (`+`) zusammengesetzt.

```javascript
var x = 2;
var y = 3;

var z = x + y;

console.log(z);
```

### Lösung

![RunJS Addition](./img/runjs-addition.png "RunJS Variablen addieren")

## Einfache und komplexe Datentypen

In JavaScript gibt es einfache und komplexe Datentypen. Mit `String`, `Number` und `Boolean` haben wir drei einfache Datentypen kennengelernt. Daneben gibt es auch komplexe Datentypen, die mehr als «nur» einen Wert aufnehmen können.

## Arrays

Arrays sind Datenfelder mit einem festen Index. Dieser Index beginnt immer mit der Ziffer `0`. Das heisst, der erste Wert liegt auf dem Index `0`, der zweite Wert liegt auf dem Index `1` usw. Arrays können zur Laufzeit eines Skripts dynamisch vergrössert und verkleinert werden.

* über den Konstruktor: `var x = new Array(3)`;
* über die deklarative Erzeugung mit Literalen: `var y = ['Apfel', 'Birne', 'Banane'];`

### Array mit Konstruktor erzeugen

Konstruktor-Funktionen sehen aus wie normale Funktionen, die wir etwas später noch genauer anschauen. Sie werden mit dem Schlüsselwort `new` eingeführt. Für den Augenblick ist wichtig zu verstehen, dass du in JavaScript Objekte über Konstruktoren und deklarativ definieren kannst. Wenn du eine Variable über eine Konstruktor-Funktion erstellst, ist das eine explizite Definition. Das heisst, im Code ist genau ersichtlich, von welcher Basis-Klasse du dein Objekt ableitest. So siehst du über das Schlüsselwort `Array` auf einen Blick, dass du ein Array erzeugt hast.

	         Zuweisungsoperator             
	                  │                     
	                  │                     
	                  ▼                     
	      var gemuese = new Array();       
	       ▲      ▲          ▲              
	       │      │          │              
	       │      │          └───┐          
	       │ Bezeichner          │          
	       │                     │          
	Schlüsselwort      Konstruktormethode  
	                   mit "new" erzeugt
	                   neues Array mit 
	                   3 leeren Elementen.  
	                   

### Array mit Literalen erzeugen

Im Unterschied zur Konstruktor-Funktion kannst du über die Literale die Objekte deklarativ erzeugen. Das heisst, der JavaScript-Interpreter erkennt durch die Verwendung der eckigen Klammern (`[]`) implizit, dass du ein `Array` erstellen willst. ***In der Praxis ist die Deklaration viel weiter verbreitet als die Verwendung von Konstruktoren***.

	         Zuweisungsoperator             
	                  │                     
	                  │                     
	                  ▼                     
	      var gemuese = [];                 
	       ▲      ▲     ▲                   
	       │      │     │                   
	       │      │     └───┐               
	       │ Bezeichner     │               
	       │                │               
	Schlüsselwort      Literale []          
	                   

## Übung 3, ein Array erzeugen

In dieser Übung wagen wir einen ersten Schritt mit Arrays und verwenden dafür die deklarative Erzeugung. Danach lassen wir uns mit `console.log()` und `typeof` den Datentyp des Arrays ausgeben.

### Aufgabe

Deklariere ein Array und frage den Datentyp ab.

```javascript
var data = [];

console.log(typeof data);
```

### Lösung

![RunJS, Array mit Typeof abfragen](./img/runjs-array-typeof.png "Array Datentyp über typeof abfragen.")

Oh! Die JavaScript-Umgebung (= *RunJS*) sagt uns, dass wir ein Objekt erstellt haben. Wir wollten doch ein Array erstellen! Die JavaScript-Umgebung hat aber immer Recht. Ein Array ist ein (komplexes) Objekt. Das heisst, anders wie bei den einfachen Datentypen, erfordert JavaScript hier, dass wir etwas genauer nachfragen. Es ist ein bisschen so wie beim Bestellen von Kaffee. Wenn der Barista fragt: «Kaffee?», und du antwortest: «Ja!», dann ist das eindeutig. Wenn der Barista aber fragt: «Was hätten sie gerne?», dann ist deine Antwort vielleicht «ich hätte bitte gerne ein Kaffee mit Rahm, ohne Zucker und ein Glas Wasser». Diese Antwort hat viel mehr «Ebenen», so wie ein Array. Damit wir nun sehen, ob wir wirklich ein Array erstellt haben, müssen wir etwas genauer nachfragen.

### Aufgabe

Deklariere ein Array und frage den Datentyp (genau) ab.

```javascript
var data = [];

console.log(data.constructor.name);
```

### Lösung

![RunJS, Array über Konstruktor abfragen](./img/runjs-array-constructor.png "Array Datentyp über den Konstruktor abfragen.")

## Mit Arrays arbeiten

Bisher haben wir immer leere Arrays erzeugt `var data = [];`. Im nächsten Abschnitt zeigen wir dir, wie du Arrays bereits bei der Erzeugung mit Werten befüllst und diese nachträglich änderst.

### Array initial mit Werten befüllen

Du kannst beim Erzeugen von Arrays auch gleich Werte definieren. Diese Werte können allen verfügbaren Datentypen entsprechen.


Das nun erzeugte Array `things` hat drei Einträge: auf dem Index `0` liegt ein `string` mit dem Wert «Apple». Danach folgen auf dem Index `1` der Wert «Kiwi» vom Typ `String` und auf dem Index `2` der Wert «3» vom Typ `Number`. 

	       Zuweisungsoperator         Wert          
	                │                   │           
	                │                   │           
	                ▼                   ▼           
	     var things = ['Apple', 'Kiwi', 3];         
	      ▲      ▲    ▲                  ▲          
	      │      │    │                  │          
	      │      │    └────────────┬─────┘          
	      │ Bezeichner             │                
	      │                 Array-Literale          
	Schlüsselwort                                   

Der Index ist wichtig, wenn du später Dinge aus deinem Array abrufen oder verändern willst. Du kannst ihn über den Variablen-Namen (`things`) und eckige Klammern abrufen:


	 things[0] >>> hat den Wert 'Apple'
	 things[1] >>> hat den Wert 'Kiwi'
	 things[2] >>> hat den Wert 3 
	        ▲              
	        │              
	        │              
	Index (= Position)      



## Übung 4, Array erzeugen und verändern

### Aufgabe

Erstelle das Array `things` und ändere «3» auf «4».

```javascript
var things = ['Apple', 'Kiwi', 3];

things[2] = 4;

console.log(things);
```

### Lösung

![RunJS Array updaten](./img/runjs-array-update.png "Array nach der Erzeugung aktualisieren.")
