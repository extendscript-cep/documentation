# Ein XML-Dokument aufbauen

Im letzten Abschnitt haben wir mit dem Aufbau eines XML-Dokuments begonnen. Falls du erst in diesem Abschnitt einsteigst, ist es wichtig, dass du als erstes einen Ordner und ein XML-Dokument erstellst. Wie du das machst, siehst du [hier](./xml-firststeps.md#ein-xml-dokument-anlegen).

## XML-Elemente benennen (Bezeichner)

Unser bisheriges XML-Dokument sieht so aus:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<root></root>
```

In XML kannst du – anders als bei HTML – selbst bestimmen, wie du deine Elemente bezeichnen willst. Sofern du keine DTD und kein Schema referenzierst, ist nur wichtig, dass du dich an die grundlegenden Regeln für ein *well formed* XML hältst (siehe auch [hier](./xml-grundlagen.md#wie-xml-verarbeitet-wird)). Das heisst unter anderem, dass  öffnende und schliessende Tags den gleichen Bezeichner tragen. Um dies zu demonstrieren, ändern wir nun die Bezeichnung von `root` auf `items`:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<items></items>
```

Anschliessend legen wir innerhalb von Data einige weitere XML-Elemente an. Im Fachjargon werden diese gerne auch als *Child Element(s)* bezeichnet:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<items>
  <item>
    <name>Peter Muster</name>
    <author>https://example.com/petermuster</author>
    <photo>https://example.com/photos/pm1234</photo>
  </item>
</items>
```

Die VSCodium-Extension *XML* (von Red Hat) hat nun eine weitere Hilfe bereit, welche dir die Navigation im Code erleichtert: die ***Outline***. Die Outline zeigt dir an, wie dein Dokument strukturiert ist. Du du kannst mit einem Klick auf ein Element in der Outline einfach zwischen den Elementen navigieren. Das ist vor allem bei grossen XML-Dokumenten mit vielen Verschachtelungen sehr hilfreich.

Die Outline öffnest du, indem du in der ***Side Bar*** von VSCodium auf ***Outline*** klickst:

![XML Outline](./img/xml-outline.png)
1. Outline
