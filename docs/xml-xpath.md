# XPath

Für diesen und die nächsten Abschnitte verlassen wir die gewohnte Umgebung in VSCodium. Es wäre sicher auch möglich, XML über das Nachladen von weiteren Extension in VSCodium zu entwickeln, bei uns hat aber keine dieser Extensions so richtig funktioniert. Wir haben unter anderem die Extension <a href="https://open-vsx.org/extension/DotJoshJohnson/xml" target="_blank">XML Tools</a> und die Extension <a href="https://open-vsx.org/extension/redhat/vscode-xml" target="_blank">VSCode XML</a> getestet.

## XML-IDE

Für die Entwicklung von XML gibt es eine Reihe von optimierten Programmen. Sie stellen dir eine Umgebung zur Verfügung, in der du nicht nur an deinen XML-Dokumenten arbeiten, sondern auch Transformationen durchführen kannst. Im Fachjargon werden solche Programme als *Integrated Development Environment*, kurz IDE, bezeichnet. Eine XML-IDE ist also ein Programm, das speziell für die Entwicklung von XML hergerichtet ist. In der deutschen Sprachen werden diese *IDE* gerne auch als Entwicklungsumgebungen bezeichnet.

Eine kleine Liste von XML-Entwicklungsumgebungen findest du im Abschnitt [Tools/Programme](./tools-applications.md). Wir verwenden für unsere Beispiele die <a href="https://www.eclipse.org/" target="_blank">Eclipse IDE</a> mit dem Plugin <a href="https://marketplace.eclipse.org/content/eclipse-xml-editors-and-tools-0" target="_blank">Eclipse XML Editors and Tools</a>. Beide sind Open Source Software. Das heisst, sie sind für dich kostenlos und werden durch eine Gruppe unabhängiger, freiwilliger Entwickler getragen. Dem gegenüber gibt es eine Reihe von kostenpflichtigen Produkten, die mehr können als wir hier für unseren Einstieg brauchen. Sie werden dir in der Liste unter [Tools/Programme](./tools-applications.md) als kostenpflichtig ausgewiesen.

Das Programm Eclipse gibt es in verschiedenen Versionen. Wir verwenden die Version für die Entwicklung von Java-Applikationen.

## Eclipse installieren

Damit du Eclipse richtig installieren kannst, brauchst du Java. Die Installation von Java ist – seit Oracle Java gekauft hat – keine triviale Sache mehr, weil Oracle die Lizenzierung der Software verändert hat. Wenn du bereits Java installiert hast, kannst du diesen Schritt überspringen.

Wenn du Java neu installierst, empfehlen wir dir als erstes <https://www.oracle.com/java/technologies/javase-downloads.html> in deinem Browser zu öffnen. Sobald die Seite geladen ist, klickst du auf *JDK Download*.

![Java Install](./img/java-install.png)

Nach dem Klick wird eine neue Seite geöffnet. Auf dieser scrollst du nach unten bis die Liste mit den Installations-Dateien erscheint. Hier wählst du die Installationsdatei für dein Betriebssystem aus, lädst sie herunter und öffnest sie anschliessend mit einem Doppelklick.

![Java Installers](./img/java-installers.png)

Die Installation ist einfach. Du kannst dich ohne weitere Aktion einfach durch alle Schritte durchklicken. Am Ende solltest du diese Bestätigung sehen:

![Java SE installiert](./img/java-se.png)

### Los geht's

Unter <a href="https://www.eclipse.org/downloads" target="_blank">www.eclipse.org/downloads</a> kannst du die Installations-Datei von Eclipse herunterladen. Speichere sie bei dir in den Downloads oder an einem Ort deiner Wahl ab und öffne sie anschliessend mit einem Doppelklick.

![Eclipse Downloads](./img/eclipse-download.png)

Als nächstes klickst du doppelt auf den *Eclipse Installer* …

![Eclipse Installer ausführen](./img/eclipse-installer-run.png)

… und wählst die Installation von *Eclipse Java*:

![Eclipse Java](./img/eclipse-choose.png)

Wir schlagen dir vor, dass du die Pfade für die Installation auf den Standardwerten lässt. Eclipse wird ***nicht*** bei deinen Applikationen installiert. Stattdessen findest du die Installation in deinem Benutzerordner. Das ist dort, wo auch die Ordner *Dokumente* und *Downloads* liegen.

Nachdem die Installation abgeschlossen ist, kannst du Eclipse direkt öffnen, indem du auf *Launch* klickst:

![Eclipse launch](./img/eclipse-launch.png)

Beim ersten Start wird dich Eclipse fragen, wo es den ersten *Workspace* anlegen soll. Auch hier empfehlen wir dir, die Standardwerte zu übernehmen. Einen Workspace kannst du dir am einfachsten als Projektarbeitsumgebung vorstellen. Weil wir noch kein Projekt haben, erstellt uns Eclipse nun einen leeren Workspace.

![Eclipse Workspace](./img/eclispe-workspace.png)

### XML-Tools installieren

Damit du in Eclipse-Plugins installieren kannst, musst du den *Marketplace* öffnen. Das geht über *Help* > *Eclipse Marketplace*.

![Eclipse Marketplace](./img/eclipse-marketplace.png)

Im Suchfeld gibst du nun `eclipse xml` ein. Die Suche des Marketplace ist nicht die schnellste. Es kann gut sein, dass es einige Sekunden dauert, bis du deine Ergebnisse bekommst. In diesen Ergebnissen sollte es ein Packet mit dem Namen *Eclipse XML Editors and Tools* haben. Bei diesem klickst du auf den Button *Install*.

![Eclipse Marketplace Search](./img/eclipse-marketplace-search.png)

In den nächsten Schritten musst du bestätigen, dass du dieses Plugin installieren willst und die Lizenz abnicken. Danach wird die Installation im Hintergrund gestartet. Der aktuelle Stand wird dir unten rechts in der Fussleiste angezeigt. Sobald das Plugin installiert ist, musst du Eclipse neu starten. Es wird dir ein entsprechender Dialog angezeigt.

![Eclipse Installer Status](./img/eclipse-install-status.png)

## Einführung in XPath

Die XML-Path-Language, kurz XPath, ist eine standardisierte Sprache, damit du in XML-Dokumenten navigieren kannst. Sie ähnelt ein bisschen dem Schema einer URL, wie du sie im Internet verwendest, wenn du zum Beispiel auf eine Kontakt-Seite navigierst, welche die URL `/kontakt` hat.

XPath wird, wie die anderen Sprachen der XML-Sprachfamilie, vom [W3C entwickelt und spezifiziert](https://www.w3.org/TR/xpath/). Im Fachjargon wird XPath gerne als Abfragesprache für XML-Dokumente benannt, mit welcher du Teile eines Solchen adressieren und auswerten kannst.

### XPath-Achsen

Durch das angeben von Achsen kannst du (ausgehend vom aktuellen Knoten) durch das XML-Dokument navigieren. Die einzelnen Schritte nach unten werden durch den Schrägstrich `/` gekennzeichnet. Zusätzlich gibt es die Möglichkeit, auch über Schlüsselwörter zu navigieren. Diese Navigation wollen wir uns im nächsten Schritt etwas genauer anschauen.

### XML-Projekt öffnen

Damit wir in Eclipse mit dir durch das nächste Beispiel gehen können, öffnen wir unser Projekt aus den letzten Abschnitten. Falls du hier einsteigst: Du kannst dir den letzten Stand des Projekts <a href="/examples/xml-schema.zip" target="_blank" download>hier</a> herunterladen.

In Eclipse gehst du nun auf *File* > *Open Projects from File System*:

![Eclipse, new project](./img/eclipse-project-new.png)

Im nächsten Dialogfenster wählst du dein Projekt, in dem du den Button *Browse* klickst und zu deinem Ordner navigierst. Wir haben es auf dem Schreibtisch abgelegt.

![Eclipse browse](./img/eclipse-source-select.png)

Zum Schluss bestätigst du mit *Finish*.

### XML-Dokument öffnen

Wie in VSCodium hast du auf der linken Seite dein Dokumentenverzeichnis. Öffne nun mit einem Doppelklick das Dokument `data.xm` im Editor, der – wie bei VSCodium – in der Mitte angezeigt wird.

![Eclipse, open XML](./img/eclipse-xml-open.png)

Der XML-Editor des Plugins *Eclipse XML Editors and Tools* kennt zwei Modi: `Design` und `Source`. `Design` ist ein visueller Editor, in dem du einfach und bequem deine XML-Dokumente bearbeiten kannst. Dagegen zeigt dir `Source` den effektiven Code aus dem XML-Dokument an. Wenn du nun auf `Source` klickst, wirst du dein XML-Dokument wieder so sehen, wie du es in VSCodium gesehen hast.

![Eclipse, XML editors](./img/eclipse-xml-editors.png)

### XPath-Navigator öffnen

Die *Eclipse XML Editors and Tools* stellen dir visuelle Oberfläche für das navigieren mit XPath zur Verfügung. Hier kannst du zum Beispiel Adressierungen validieren, bevor du sie in eine Transformation einfügst. Damit du mit dieser Oberfläche arbeiten kannst, musst du den *XPath View* öffnen. Das geht am schnellsten mit einem klick auf die Lupe oben rechts in der Toolbar von Eclipse.

![Eclipse Quick Search](./img/eclipse-quicksearch.png)

Die Lupe öffnet dir eine Command Palette [ähnlich wie in VSCodium](./einfuehrung.md#command-palette). Hier kannst du `XPath` eintippen und anschliessend mit `Enter` oder einem Klick den *XPath (XML)*-View öffnen:

![Eclipse XPath View wählen](./img/eclipse-xpath-editor.png)

### XPath-Beispiele

Generell kannst du in XPath über den Schrägstrich `/` zum nächsten Knoten im XML-Baum springen. Wenn du in unserem XML zum Beispiel den Knoten `items` zugreifen willst, dann kannst du das über:

```xpath
/data/items
```

Das Ergebnis in Eclipse müsste dann so aussehen:

![XPath, Example absolute](./img/xpath-example-absolute.png)

Mit dieser Notation navigierst du über die *Dokument-Knoten*, ausgehend vom *Wurzelknoten* `data`. Sie beginnt ***immer*** mit einem `/`.

Das Gleiche erreichst du auch, wenn du `/data` mit zwei Schrägstrichen ersetzt:

```xpah
//items
```

In der Notation mit zwei Schrägstrichen `//` kannst du alle Elemente des Dokuments (ausser das *Wurzelelement*) direkt selektieren:

![XPath double slashes](./img/xpath-slashes.png)

Mit dem Selektor `child` kannst du alle direkten Kindknoten adressieren. Wenn wir zum Beispiel mehrere `item` in `items` hätten, könntest du diese mit …

```xpath
/data/items/child::item
```

… ansprechen. Im aktuellen Beispiel haben wir nur einen Kindknoten `item`. Die Auswahl funktioniert aber trotzdem.

![XPath child](./img/xpath-child.png)

Mit `parent` kannst du das Elternelement eines Elements auswählen. Wenn wir zum Beispiel das Elternelement von allen `name`-Elementen haben wollen – also `item` –, dann sieht das so aus:

```xpath
//name/parent::item
```

Als Ergebnis liefert dir der XPath-View in Eclipse das `item`-Element:

![XPath parent](./img/xpath-parent.png)

Ein wichtiger Vertreter ist auch noch `self`. Mit `self` kannst du einen Knoten sich selbst adressieren lassen. Das mag in den aktuellen Beispielen etwas überflüssig wirken. Innerhalb von XML-Transformationen kann das aber sehr nützlich sein:

```xpath
//item/self::item
```

Hier adressieren wir `item` und weisen `item` an, «sich selbst zu nehmen».

![XPath self](./img/xpath-self.png)

Du kannst XML-Elemente jedoch nicht nur über den Element-Namen adressieren, sondern auch über ihre Attribute. Wir machen dir hier vollständigkeitshalber ein Beispiel dafür, obwohl wir noch gar nicht mit Attributen gearbeitet haben. Falls du dich nicht mehr so ganz daran erinnern kannst, was Attribute sind, werf doch nochmals kurze einen Blick in die [Einführung zu XML](./xml-grundlagen.md).

Damit wir das Beispiel machen können, ändern wir unser XML kurz ein wenig ab. Wir haben dem Element `item` das Attribut `id` mit dem Wert `123` hinzugefügt:

```diff
- <item>
+ <item id="123">
    <name>Peter Muster</name>
    <author>https://example.com/petermuster</author>
    <photo>https://example.com/photos/pm1234</photo>
  </item>
```

Attribute können über den «at»-Selektor `@` adressiert werden. Hierbei kannst du entweder alle Elemente selektieren, die ein entsprechendes Attribut haben …

```xpath
//item[@id]
```

… oder ein Element selektieren, das einen spezifischen Wert bei einem Attribut hat:

```xpath
//item[@id="123"]
```

In Eclipse sieht das so aus:

![XPath attribute](./img/xpath-attribute.png)

***Es gibt noch eine Reihe weiterer Selektoren, Funktionen und Operatoren.*** Eine gute Übersicht den gebräuchlichen findest du <a href="https://wiki.selfhtml.org/wiki/XML/XSL/XPath" target="_blank">hier</a>.

## Fehler in Eclipse erkennen

Vielleicht ist es dir aufgefallen: Seit wir unsere Zeile von `<item>` auf `<item id="123">` geändert haben, zeigt uns Eclipse bei dieser Zeile ein rotes Kreuz an. Das kommt daher, dass unser XML-Schema Elementen keine Attribute erlaubt (siehe auch ABC).

Die genauen Fehlermeldungen findest du bei Eclipse im Tab *Problems*.

![Eclipse Problems](./img/eclipse-problems.png)

Wenn du bei *Problems* den Kurzbefehl **⌘i** (Windows **ctrl i**) drückst, bekommst du weitere Details angezeigt:

![Eclipse Problem Detail](./img/eclipse-problem-detail.png)

Alternativ kannst du dir die Fehler auch über einen Rechtsklick auf die Fehlermeldung und dann *Properties* anzeigen lassen. In unserem Beispiel steht da …

*cvc-complex-type.3.2.2: Attribute 'id' is not allowed to appear in element 'item'.*

… weil Attribute durch das Schema nicht definiert sind.
