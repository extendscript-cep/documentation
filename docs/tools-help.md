# Adobe ExtendScript, wo kriege ich Hilfe

In diesem Abschnitt fassen wir einige Orte zusammen, wo du Hilfe zu Adobe ExtendScript erhältst. Es ist eine unvollständige Liste und wir sind dir dankbar, wenn du uns weitere Orte mitteilen kannst.

## Adobe ExtendScript

### Web

* Adobe Developer Center: <https://www.adobe.com/devnet/creativesuite.html>
  * ***WICHTIG: Du musst ein Account erstellen, um auf die aktuellsten Dokumentationen zuzugreifen – ansonsten erhältst du nur veraltete Ausgaben von Adobe CS6***

## Adobe AfterEffects

### Web

* After Effects Scripting Guide: https://docs.aenhancers.com/>

## Adobe InDesign

### Web

*  InDesign ExtendScript API von Gregor Fellenz: <https://www.indesignjs.de/extendscriptAPI/indesign-latest/#about.html>
* HilfDirSelbst.ch: <https://www.hilfdirselbst.org/1.php?t=Skripte&read_category=94>
* InDesign XML and JavaScript (Blog): <https://www.indesignblog.com/>
* InDesignScript: <https://indesignscript.de/>
* indesignjs-resources: <http://grefel.github.io/indesignjs-resources/>

### Bücher

* InDesign automatisieren: <https://www.indesignjs.de/auflage2/>
* XML and InDesign: <https://www.oreilly.com/library/view/xml-and-indesign/9781449344153/>

## Adobe Photoshop

### Bücher

* Photoshop Scripting: <https://gumroad.com/l/rAAKQ>
