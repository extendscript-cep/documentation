# Einführung

## Adobe Programme via Schnittstellen ansprechen

Adobe Programme können über eine Reihe von Schnittstellen angesprochen werden. Wir fokussieren uns auf die JavaScript-Schnittstellen. Daneben gibt es unter Apple MacOS noch eine AppleScript- und unter Microsoft Windows VBScript-Schnittstelle. JavaScript hat den Vorteil, dass es nicht an ein Betriebssystem gebunden ist. Es kann auf allen Plattformen ausgeführt werden.

## JavaScript

JavaScript ist die syntaktische Grundlage für Adobe ExtendScript. Das tönt kompliziert – ist es aber nicht. JavaScript ist eine einfache Skriptsprache, die hauptsächlich in der Webentwicklung gebraucht wird. Sie kann aber auch in oder mit Programmen ausgeführt werden. Adobe ExtendScript-Skripte werden nach den Direktiven von JavaScript verfasst.

## Adobe ExtendScript

Adobe ExtendScript setzt auf JavaScript auf und besteht aus einer Reihe von Befehlen, die erlauben, Adobe Programme anzusprechen. Zum einen gibt es die *Core JavaScript Elemente*, welche dem Standard von JavaScript entsprechen. Zum anderen gibt es die *Adobe ExtendScript Elemente*, welche durch Adobe definiert werden. Wichtig ist hier zu wissen, dass sich Adobe mit ExtendScript auf einen 25 Jahre alten Standard von JavaScript stützt. Das heisst, neuere Sprachelemente von JavaScript, wie zum Beispiel `const`, können nicht direkt verwendet werden.

Adobe ExtendScript-Dateien haben meist die Endungen *.jsx* oder *.jsxbin*. Bei Dateien mit der Endung *.jsx* kann der Code in der Regel eingesehen und bei Bedarf abgeändert werden. Dagegen sind Dateien mit der Endung *.jsxbin* eine Art «computercode». Das heisst, das Skript kann nicht oder nur unter erschwerten Bedingungen eingesehen werden.

## VSCode und VSCodium

Lange Zeit war das Programm [Adobe ExtendScript Toolkit][estk] die Entwicklungsumgebung für Skripte. 2018/19 wurde es durch ein [VSCode Plugin][vscode-init] abgelöst.

VSCode ist ein populärer Editor für Text und Programmcode von Microsoft. Er ist kostenlos und kann [hier][vscode-download] heruntergeladen werden.

Der [Programmcode von VSCode steht unter der OpenSource Lizenz MIT][vscode-oss]. Dagegen wird das [fertige Programm als Freeware][vscode-license] veröffentlicht. Es ist angereichert mit Diensten, die Daten über die Nutzung sammeln.

Wir verwenden in dieser Dokumentation das Programm [VSCodium][vscodium-web]. VSCodium ist eine Abspaltung von VSCode und wird ohne Überwachung ausgeliefert – hat aber den gleichen Funktionsumfang wie VSCode.

### Installation

VSCodium ist in verschiedenen Versionen für Microsoft Windows, Apple MacOS und Linux verfügbar. Alle verfügbaren Installationen findest du [hier][vscodium-install-all]. Für Apple MacOS kanst du in der Regel [VSCodium.1.46.1.dmg][vscodium-install-mac] verwenden, für Microsoft Windows [VSCodiumSetup-x64-1.46.1.exe][vscodium-install-win].

### Benutzeroberfläche

![VSCodium Benutzeroberfläche](./img/vscodium-ui.png "VSCodium Benutzeroberfläche")

Die Benutzeroberfläche ist in fünf wesentliche Teile unterteilt:

1. Mit der ***Activity Bar*** kann zwischen Ansichten gewechselt werden. Zum Beispiel lässt sich mit der Lupe die Sucheingabe aufrufen.
2. Die ***Side Bar*** zeigt verschiedene Ansichten auf dein Projekt. Sie ist unter anderem abhängig vom gewählten Icon in der Activity Bar.
3. Der ***Editor*** ist der Hauptbereich für das Bearbeiten deiner Skripte und Dokumente.
4. Die ***Panels*** werden unterhalb des Editors angezeigt. Sie öffnen sich in der Regel automatisch, wenn sie gebraucht werden. Panels können als eine Art «Fenster» für allerlei Information verstanden werden – zum Beispiel, wenn Adobe InDesign einen Fehler in einem Skript zurückmeldet.
5. In der ***Status Bar*** werden Informationen zum aktuellen Projekt und den gerade aktiven Daten dargestellt.

### Command Palette

Die ***Command Palette*** kann als die zentrale «Steuereinheit» verstanden werden. Über sie lassen sich unter anderem alle Befehle des Editors sowie alle Dokumente des Projekts aufrufen.

#### Alle Befehle

![VSCodium Command Palette Befehle](./img/vscodium-cmdpal-fn.png "VSCodium mit geöffneter Command Palette für Befehle")

Um alle Befehle aufzurufen muss gleichzeitig **⌘⇧P** (Windows **ctrl⇧P**) gedrückt werden. Im Suchfenster kann nach den gewünschten Befehlen gesucht werden. Sie lassen sich danach durch die Pfeiltasten selektieren und mittels der **Enter**-Taste auswählen.

#### Alle Dokumente

![VSCodium Command Palette Dokumente](./img/vscodium-cmdpal-files.png "VSCodium mit geöffneter Command Palette für Dokumente")

Um nach Dokumenten im aktuellen Projekt zu suchen muss **⌘P** (Windows **ctrl+P**) gedrückt werden. Danach kann im Suchfenster nach den gewünschten Dokumenten gesucht werden.

### VSCodium Extensions für Adobe ExtendScript

VSCodium ist unsere Arbeitsumgebung. Noch können wir keine Skripte für Adobe Programme schreiben. Damit das möglich wird, müssen wir einige Extensions (= Erweiterungen) installieren.

![VSCodium Plugins installieren Coammand](./img/vscodium-extensions.png)

1. Extensions
2. Extensions suchen
3. Installieren

In VSCodium können Extensions über den «Würfel-Button» in der ***Activity Bar*** installiert werden. Nach dem Klicken öffnet sich in der ***Side Bar*** ein Suchfenster. In diesem kann nach Extensions gesucht werden, die sich anschliessend über den Button `Install` installieren lassen.

Erforderliche Erweiterungen, die installiert werden müssen:

* [Adobe ExtendScript Debugger][vscodium-ext-esdebug]: Nach «ExtendScript Debugger» suchen und anschliessend via Button `Install` installieren.
* [Adobe Script Runner][vscodium-ext-scriptrunner]: Nach «Adobe Script Runner» suchen und anschliessend via Button `Install` installieren.

Empfohlene, weitere Extensions für diesen Kurs:

* [Markdown All in One][vscodium-ext-mdall]
* [XML (Red Hat)][vscodium-ext-xmltools]

![VSCodium Extension Details](./img/vscodium-extensions-detail.png "Details zu den VSCodium Extensions für die Entwicklung von Adobe ExtendScript")
1. ***Adobe Script Runner*** Die Erweiterung öffnet die Host-Anwendung (bsp. Adobe InDesign) und führt dann das Skript aus.
2. ***ExtendScript Debugger*** Mit dieser Erweiterung können Fehler in Skripten gesucht werden (degugging).
3. ***Markdown All in One*** Alles, um ein Markdown zu schreiben (Tastaturkürzel, Inhaltsverzeichnis, automatische Vorschau usw.).
4. ***XML (von Red Hat)*** Diverse XML-Werkzeuge inklusive Validierung.

[estk]:https://www.adobe.com/ch_de/products/extendscript-toolkit.htmlESTK
[vscode-init]:https://medium.com/adobetech/the-future-of-extendscript-development-a-vscode-plugin-2d8d0172a357
[vscode-download]:https://code.visualstudio.com/
[vscode-oss]:https://github.com/microsoft/vscode
[vscode-license]:https://code.visualstudio.com/License/
[vscodium-web]:https://vscodium.com/
[vscodium-install-all]:https://github.com/VSCodium/vscodium/releases
[vscodium-install-mac]:https://github.com/VSCodium/vscodium/releases/download/1.46.1/VSCodium.1.46.1.dmg
[vscodium-install-win]:https://github.com/VSCodium/vscodium/releases/download/1.46.1/VSCodiumSetup-x64-1.46.1.exe
[vscodium-ext-esdebug]:https://marketplace.visualstudio.com/items?itemName=Adobe.extendscript-debu
[vscodium-ext-scriptrunner]:https://marketplace.visualstudio.com/items?itemName=renderTom.adobe-script-runner
[vscodium-ext-mdall]:https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one
[vscodium-ext-xmltools]:https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml
