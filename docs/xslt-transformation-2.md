# Transformation Aufgabe 2: Das Element `<item>` mit Bildpfad ergänzen

In der [Aufgabe 2](./xslt-firststeps.md#aufgaben) unserer XSL-Trasformation wollen wir jedem `<item>`-Tag ein weiteres Tag mit dem Pfad zum Bild hinzufügen.

Falls du erst hier einsteigst, kannst du dir das Projektverzeichnis <a href="/examples/xslt.zip" target="_blank" download>hier</a> herunterladen. Zusätzlich brauchst du das Ergebnis aus der letzten Aufgabe, die Datei `transform.xslt`, welche du <a href="/examples/transformation.xslt" target="_blank" download>hier</a> herunterladen kannst. Um weiterzufahren, musst du die Datei `transform.xslt` ins Projektverzeichnis speichern, so dass du auf die folgende Dateistruktur kommst:

```text
.
├── index.xml
├── transformation.xslt
└── images/
```

Ebenfalls solltest du dein Eclipse für die Transformation konfiguriert haben. Wenn du nicht weisst wie das geht, empfehlen wir dir den Abschnitt [*Transformation konfigurieren*](./xslt-transformation-1.md#transformation-konfigurieren) aus der Aufgabe 1.

## Übersicht

Bevor wir in die Aufgabe eintauchen, wollen wir die Rahmenbedingungen nochmals genau anschauen.

Wir haben eine XML-Datei die mehrere `item`-Elemente enthält. Ein `item`-Element sieht so aus:

```xml
<item>
    <name>Francois Hoang</name>
    <author>https://unsplash.com/@aoirostudio</author>
    <photo>https://unsplash.com/photos/2x6VHSRVqSA</photo>
</item>
```

Es enthält verschiedene Angaben zum Autor und dem Bildnachweis. Uns fehlt noch der Pfad, unter welchem Adobe InDesign das Bild auf dem Computer finden kann. Damit wir diesen bestimmen können, werfen wir einen Blick auf die Bildnamen im Ordner `images` im Projektverzeichnis:

```bash
.
├── images
│   ├── 2x6VHSRVqSA.jpg
│   ├── 6z5SJE3WoC8.jpg
│   ├── 9e8xN9BTcRg.jpg
│   └── ... # weitere Bilder
└── index.xml
```

Der Bildname entspricht exakt dem letzten Teil der URL im Element mit dem Bezeichner `<photo`. Ein Beispiel:

```text
<photo>https://unsplash.com/photos/2x6VHSRVqSA</photo>
2x6VHSRVqSA.jpg
```

Was in der URL fehlt, ist die Dateinamenerweiterung `.jpg`.

## Eigenheiten von Adobe InDesign

Adobe InDesign erkennt Bildpfade nur, wenn sie in einem Element als Attribut `href` ausgewiesen werden.

## Vorgehen

Für die Abbildung des Bildpfades wollen wir in jedem `item`-Element ein neues Kindelement erzeugen. Jedes dieser Kindelemente soll über das Attribut `href` verfügen und auf das korrekte Bild verweisen.

## Neues Element `image` erzeugen

Unsere bisherige Transformation sieht so aus:

```xml
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
    <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
```

Wir wollen nun jedem `item`-Element ein neues Kindelement hinzufügen, bei dem wir später das Attribut `href` anhängen. Wir geben diesen Element den Bezeichner `image`. Wenn du willst, kannst du es natürlich auch anders benennen. Die anderen Elemente aus `item` – `name`, `author` und `photo` – wollen wir weiterhin unberührt lassen. Wir erstellen deshalb, wie bei der [Transformation für die Aufgabe 1](./xslt-transformation-1.md), ein neues Template. In dieses kopieren wir mit `xsl:copy` alle Element  – ausser der Parser findet ein Template, welches er auf ein Element anwenden kann:

```diff
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
    <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

+ <xsl:template match="//item">
+     <xsl:copy>
+       <xsl:apply-templates select="node()|@*"/>
+     </xsl:copy>
+ </xsl:template>
</xsl:stylesheet>
```

Im Unterschied zur Aufgabe 1 wollen wir aber nicht nur kopieren und ein bestehendes Element verändern, sondern ein Element hinzufügen. Das machen wir, indem wir gleich nach dem Anwenden der Templates respektive dem Kopieren mit der Direktive <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/element" target="_blank">`xsl:element`</a> ein neues Element erzeugen:

```diff
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
    <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

 <xsl:template match="//item">
     <xsl:copy>
       <xsl:apply-templates select="node()|@*"/>
+      <xsl:element name="image">
+      </xsl:element>
     </xsl:copy>
 </xsl:template>
</xsl:stylesheet>
```

Wenn du die Transformation in Eclipse nun ausführen lässt …

![XSLT, Start Transformation](./img/xslt-eclipse-next-run.png)

… sollte in deinem `index.out.xml` nun jedes `item`-Element ein leeres `image`-Element haben:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Root>
    <item>
        <name>Francois Hoang</name>
        <author>https://unsplash.com/@aoirostudio</author>
        <photo>https://unsplash.com/photos/2x6VHSRVqSA</photo>
        <image/>
    </item>
    <!-- weitere <item> -->
</Root>
```

## Letzter Teil der URL «ausschneiden»

Der nächste Abschnitt wird ziemlich anspruchsvoll. Das liegt an den verschiedenen Versionen von XSLT. Eigentlich wäre die Spezifikation bereits in der Version 3.0 verfügbar. Viele Parser und Software-Produkte unterstützen aber noch immer nur Version 1.0. Wir bleiben deshalb innerhalb des Standards in der Version 1.0. Das heisst für unsere Aufgabe: viel Tipparbeit. In neueren Standards könnte das Auswählen des letzten URL-Abschnitts mit wesentlich weniger Code erreicht werden. Wir haben dir das notwendige Template vorbereitet und fügen es nun ein:

```diff
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
    <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//item">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
        <xsl:element name="image">
        </xsl:element>
    </xsl:copy>
  </xsl:template>
+
+ <xsl:template name="substring-after-last">
+   <xsl:param name="input" />
+   <xsl:param name="marker" />
+   <xsl:choose>
+     <xsl:when test="contains($input,$marker)">
+       <xsl:call-template name="substring-after-last">
+         <xsl:with-param name="input" select="substring-after($input,$marker)" />
+         <xsl:with-param name="marker" select="$marker" />
+       </xsl:call-template>
+     </xsl:when>
+     <xsl:otherwise>
+       <xsl:value-of select="concat('file:///images/',$input,'.jpg')" />
+     </xsl:otherwise>
+   </xsl:choose>
+ </xsl:template>
</xsl:stylesheet>
```

Hier passiert nun eine Menge. Als erstes erstellen wir ein *named* Template. In XSL-Transformationen kannst du Templates entweder direkt auf Elemente anwenden oder Ebenen benennen. Bis anhin haben wir Templates mit dem Attribut `match` immer direkt auf ein Element angewendet – zum Beispiel mit `<xsl:template match="//item">` auf das Element `item`. Dagegen erstellen wir mit `<xsl:call-template name="substring-after-last">` ein Template, das ***nicht*** direkt auf ein Element angewendet wird, sondern den Bezeichner `substring-after-last` hat. Dieses Template kann irgendwo in der Transformation über seinen Bezeichner, also seinen Namen, aufgerufen werden.

Innerhalb des Templates erstellen wir als erstes zwei Parameter mit `xsl:with-param`. Diese Parameter können beim Aufruf des Templates mit Werten belegt werden. Bei `input` erwarten wir einen String, also einen Text. Das wird später der Inhalt aus dem `photo`-Element sein. Beim `marker` erwarten wir das «Trennzeichen». Bei unserer URL ist das der Schrägstrich `/`.

Nach den Parametern folgt eine <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/choose" target="_blank">`choose`-Instruktion</a> mit `when` und `otherwise`. Du kannst dir diese Instruktion wie eine [if-Bedingung](./js-schleifen.md#if-bedingung) bei Javascript vorstellen. Solange ein Zustand eintritt, der auf `when` zutrifft, wird der Code innerhalb von `when` ausgeführt. Trifft ein Zustand ein, der nicht mehr auf die Bedingung von `when` passt, wird der Code innerhalb von `otherwise` ausgeführt.

In unserem Template hat es die Bedingung von `when` in sich. Mit `test="contains($input,$marker)"` prüfen wir, ob der `marker` – in unserem Fall ein Schrägstrich `/` – im Text der URL `https://unsplash.com/photos/2x6VHSRVqSA` vorkommt. Trifft das zu, ruft das Template sich (erneut) selbst auf. Bei diesem «Selbstaufruf» übergibt sich das Template mit <a href="https://developer.mozilla.org/en-US/docs/Web/XPath/Functions/substring-after" target="_blank">`substring-after`</a> die Übergebene URL des bisherigen Input, abgeschnitten nach dem ersten Schrägstrich `/`. Das wäre also `/unsplash.com/photos/2x6VHSRVqSA` ohne `https:/`. Dieser Prozess wird solange wiederholt, bis der Text kein Schrägstrich mehr enthält und nur noch der letzte Teil der URL übrig bleibt (`2x6VHSRVqSA`). Ist dies der Fall, wird der Code von `otherwise` ausgeführt und das Template beendet, weil kein Schrägstrich `/` mehr im Text vorhanden ist.

Innerhalb von `otherwise` setzen wir das Schlussresultat mit <a href="https://developer.mozilla.org/en-US/docs/Web/XPath/Functions/concat" target="_blank">`concat('file:///images/',$input,'.jpg')`</a> zusammen, so dass der Name der Datei in eine File-URI eingepflegt wird und die Endung `.jpg` erhält. `$input` steht dabei für `2x6VHSRVqSA`. In XSL ist es so, dass Parameter oder Variablen mit einem vorangestellten `$` referenziert werden können.

Nachdem wir unser *named* Template erstellt haben, müssen wir es auch noch aufrufen und den Rückgabewert in ein Attribut verpacken. Das machen wir nun innerhalb des `xsl:element name="image"`, denn wir wollen das `href`-Attribut ja dem `image`-Element anhängen:

```diff
<xsl:template match="//item">
  <xsl:copy>
    <xsl:apply-templates select="node()|@*"/>
    <xsl:element name="image">
+      <xsl:attribute name="href">
+     	  <xsl:call-template name="substring-after-last">
+           <xsl:with-param name="input" select="./photo" />
+           <xsl:with-param name="marker" select="'/'" />
+         </xsl:call-template>
+       </xsl:attribute>
    </xsl:element>
  </xsl:copy>
</xsl:template>
```

Deine gesamte Transformation müsste nun so aussehen:

```xml
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
    <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="//item">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
      <xsl:element name="image">
        <xsl:attribute name="href">
          <xsl:call-template name="substring-after-last">
            <xsl:with-param name="input" select="./photo" />
            <xsl:with-param name="marker" select="'/'" />
          </xsl:call-template>
        </xsl:attribute>
      </xsl:element>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="substring-after-last">
    <xsl:param name="input" />
    <xsl:param name="marker" />
    <xsl:choose>
      <xsl:when test="contains($input,$marker)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="input" select="substring-after($input,$marker)" />
          <xsl:with-param name="marker" select="$marker" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
         <xsl:value-of select="concat('file:///images/',$input,'.jpg')" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
```

Wenn du nun erneut deine Transformation ausführst, müsstest du in deinem `index.out.xml` in jedem `item` ein `image`-Element mit dem Attribut `href` sehen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Root>
  <item>
      <name>Francois Hoang</name>
      <author>https://unsplash.com/@aoirostudio</author>
      <photo>https://unsplash.com/photos/2x6VHSRVqSA</photo>
      <image href="file:///images/2x6VHSRVqSA.jpg"/>
  </item>
  <!-- weitere <item> -->
</Root>
```

Ist dies nicht der Fall, stimmt was nicht. Bei Bedarf kannst du die Transformation und das Resultat <a href="/examples/xslt-final.zip" target="_blank" download>hier</a> zur Kontrolle herunterladen.
