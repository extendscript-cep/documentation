# Adobe InDesign

In Adobe InDesign werden die Skripte über das Bedienfeld *Skripte* verwaltet. Es kann über den Menübefehl *Fenster \> Hilfsprogramme \> Skripte* aufgerufen werden.

![Adobe InDesign Scripts Panel öffnen](./img/id-open-scriptspanel.png "Adobe InDesign Scripts Panel öffnen")

Die Skripte im Ordner *Anwendung* sind für alle Benutzer des Systems erreichbar. Die Skripte im Ordner *Benutzer* können nur vom aktuell angemeldeten Benutzer gesehen werden.

Das *Skripte*-Bedienfeld ist eine Art «Mini-Browser». Es zeigt alle verfügbaren Skripte an. Diese verfügbaren Skripte müssen in einem bestimmten Ordner liegen, der Adobe definiert. Du kannst weitere Skripte hinzufügen, in dem du im Bedienfeldmenü *Im Finder anzeigen* respektive *Im Explorer anzeigen* auswählst. Der Befehl öffnet einen Ordner im Apple Finder respektive Windows Explorer, wo du deine Skripte hinkopieren kannst. Damit sind sie «installiert» und du kannst sie mit einem Doppelklick im *Skripte*-Bedienfeld ausführen.

![Adobe Skripte installieren](./img/id-open-finder.png "Adobe Skripte installieren")

Neben dem *Skripte*-Panel gibt es noch das Panel *Skriptetikett*. Dieses kannst du verwenden, um einem Objekt in deinem InDesign Dokument ein Label zuzuweisen. Dieses Label kannst du später in deinen Skripten ansprechen.

![Adobe InDesign Script Label Panel](./img/id-scriptlabelpanel.png "Adobe InDesign Script Label Panel")

## Object Model

Jedes Adobe Programm hat ein *Object Model*. Das *Object Model* ist ein abstrakter Plan der dir zeigt, wie du auf die einzelnen Elemente des Programms zugreifen kannst. Ganz zu oberst von diesem *Object Model* steht zum Beispiel `app`. `app` steht für die Applikation. Wenn du also zum Beispiel `app.document.add()` in einem deiner Skripte aufrufst, navigierst du durch drei Ebenen oder «Namensräume» dieses *Object Model*. Du sagst der Applikation, sie soll mit Hilfe von `document` durch das aufrufen der Methode `add()`, die ein Teil von `document` ist, ein neues Dokument erzeugen.

Das Adobe InDesign Object Model sieht im Überblick zum Beispiel so aus:

![Adobe InDesign Object Model](./img/idObjectModel.png) 

*(Quelle: Adobe InDesign CS6 Scripting Tutorial.pdf)*

Die einzelnen Elemente des Objektmodells können so erklärt werden *(Quelle: Adobe InDesign CS6 Scripting Tutorial.pdf)*:

| Klasse                  | Bedeutung
|-------------------------|-----------------------
| Application             | Das Programm Adobe InDesign.
| Application defaults    | Die Einstellungen von Adobe InDesign (z. B. Farben, Absatzformate, Objektformate und die Standardeinstellungen für alle neuen Dokumente).
| Applicatoin events      | Ereignisse die eintreten, wenn ein Benutzer oder ein Skript mit Adobe InDesign interagiert (Dokumente öffnen/schliessen/speichern, Menübefehle aufrufen usw.).
| Application Menus       | Menüs, Submenüs und Kontextmenüs der Benutzeroberfläche. Skripte können diese ausführen oder ausgeführt werden, wenn Menübefehle aufgerufen werden.
| Application methods     | Methoden, die von einem Skript in Adobe InDesign ausgeführt werden können (z. B. Suchen/Ersetzen von Text, aktuelle Auswahl kopieren, Dokumente anlegen usw.).
| Application preferences | Z. B. Text-Voreinstellungen, PDF Export-Einstellungen oder Dokument-Voreinstellungen. Viele dieser Voreinstellungsobjekte existieren auch auf dem Dokumenten-Level. Ebenso wie bei der Benutzeroberfläche werden Anwendungseinstellungen auf neue Dokumente angewendet und Dokumenteinstellungen ändern die Einstellungen eines bestimmten Dokuments.
| Application properties  | Die Voreinstellungen des Programms (z. B. der Installationspfad).
| Books                   | Die Sammlung der geöffneten Adobe InDesign Bücher.
| Document                | Ein Adobe InDesign Dokument.
| Document defaults       | Die Dokumenteinstellungen (z. B. Farben, Absatz- und Zeichenformate).
| Document elements       | Elemente einer Seite. Z. B. Stories und importierte Grafiken aber auch gezeichnete Elemente (Rechtecke, Kreise usw.), Textfelder, XML-Elemente und alle anderen Typen von Objekten, die auf einer Seite importiert oder erstellt werden können.
| Document event          | Ereignisse auf der Ebene «Dokument» (z. B. Textimport).
| Document methods        | Methoden auf der Ebene «Dokument» (z. B. Dokument schliessen, drucken oder exportieren).
| Document preferences    | Dokumenteigenschaften wie Name, Anzahl Seiten und die Lage des Nullpunktes.
| Documents               | Eine Sammlung von Dokumenten.
| Libraties               | Eine Sammlung von Bibliotheken.
| Page                    | Eine einzelne Adobe InDesign Seite.
| Page items              | Alle Objekte, die auf einem Druckbogen erstellt oder platziert werden können.
| Pages or Spreads        | Seiten oder Druckbogen in einem Adobe InDesign Dokument.
| Stories                 | Die Textketten in einem Adobe InDesign Dokument.
| Text objects            | Zeichen, Wörter, Zeilen, Absätze und Spalten o. ä. sind Textobjekte in einer Adobe InDesign Story.

Die Basis-Klassen oder Klassen beginnen in der Regel mit einem Grossbuchstaben. Mit diesen Klassen kannst du auf zwei Arten arbeiten: Entweder du rufst die Klasse direkt auf – oder du rufst ein instanziiertes Objekt auf (Begriffe: Siehe *Klassen, Instanzen, Objekte* in [Javascript Grundlagen](./js-grundlagen). Wenn du eine Klasse direkt aufrufst, gibt sie dir ein Objekt mit Informationen zurück. Dagegen kannst du auch zuerst eine Instanz erstellen und diese danach weiterverwenden. Der wesentliche Punkt ist, dass der Kontext stimmen muss, in dem du die Klasse ausführst. Im folgenden Beispiel erstellen wir eine Variable `filePath` die einen Pfad auf ein Dokument definiert. Danach rufen wir die Basis-Klasse `File` auf und übergeben ihr den Pfad auf das File. Ab jetzt weiss der Interpreter, dass er dieses Objekt als `File` behandeln muss und es stehen dir Eigenschaften und Methoden zur Verfügung, die von der Klasse geerbt werden – zum Beispiel kannst du mit `file.created` abfragen, wann ein Dokument erstellt wurde.

![Adobe ExtendScript File Class](./img/id-file-class.png "Adobe ExtendScript File Class")
