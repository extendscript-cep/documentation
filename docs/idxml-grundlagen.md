# Adobe InDesign und XML

In diesem und den nächsten Abschnitten wollen wir dir zeigen, wie du XML in Adobe InDesign importieren und verwenden kannst.

## Projektverzeichnis erstellen

Falls du die Abschnitte und Transformationen aus [XSL](./xslt-grundlagen.md) gemacht hast, kannst du mit dem Ordner aus [XSL > Validieren mit DTD](./xslt-dtd.md) weiterfahren. Falls du jetzt einsteigst, solltest du als erstes die Projektdateien <a href="/examples/xslt.zip" target="_blank" download>hier (`xslt.zip`)</a> herunterladen. Zusätzlich brauchst du die Resultate aus den XSL-Abschnitten, welche du <a href="/examples/xslt-dtd.zip" target="_blank" download>hier (`xslt-dtd.zip`)</a> herunterladen kannst. Führe anschliessend die beiden ZIP-Archive in einem Ordner zusammen. *Wichtig ist, dass du die Datei `index.xml` aus der ZIP-Datei `xslt.zip` mit jener aus der ZIP-Datei `xslt-dtd.zip` überschreibst.* Dein Projektverzeichnis sollte nach der Zusammenführung so aussehen:

![XSLT, Ordnerstruktur Projektabschluss](./img/idxml-projectstructure.png)

Die Datei `.project` wurde von Eclipse generiert, wenn du das Projekt bereits in Eclipse geöffnet hast. Sie wird dir aber nur angezeigt, wenn du die *Anzeige von versteckten Dateien* unter Apple MacOS oder Microsoft Windows *aktiviert* hast.

## Vorgehen

In den nächsten Abschnitten wollen wir das Ergebnis aus den [XSL-Abschnitten](./xslt-grundlagen.md) in Adobe InDesign importieren. Bitte beachte, dass wir in unserem Beispiel keinen Wert auf exakte Typografie und Raster legen. Vielmehr geht es uns darum, dir fokussiert das Vorgehen und die Stolpersteine beim Arbeiten mit XML in Adobe InDesign zu zeigen.

## Aufgabe

Wir erstellen gemeinsam einen Bilderkatalog, der auf jeder Seite das Bild, den Autor und die Referenz auf das Bild enthält.
