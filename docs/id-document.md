# Dokumente erstellen und speichern

Für die nächsten Beispiele gehen wir davon aus, dass du die Abschnitte über JavaScript und ExtendScript gelesen hast oder bereits über etwas Fachkenntnisse verfügst. Wir zeigen nicht mehr explizit, wie du das Skript in VSCodium anlegst und debuggst. Du kannst aber alle Skripte in VSCodim abschreiben (was wir dir empfehlen) oder kopieren, unter einem Namen deiner Wahl speichern, und in InDesign ausführen. Alternativ kannst du sie auch direkt über das *Scripts Panel* für InDesign «installieren». In diesem Fall wirst du jedoch nicht debuggen können.

Im folgenden Beispiel zeigen wir dir, wie du Dokumente erzeugen, speichern und prüfen kannst. Das Skript speichert bei jedem Aufruf das aktive Dokument in Adobe InDesign in einer neuen Version auf dem Desktop ab und verwendet dafür den Zeitstempel als Name. Das sieht dann zum Beispiel so aus: `1598569427299.indd`.


```javascript
var timestamp = Date.now();
var doc = new File('~/Desktop/' + timestamp + '.indd');

if (app.documents.length === 0) {
    var create = app.documents.add();
}

if (app.activeDocument.saved === false || app.activeDocument.modified === true) {
  app.activeDocument.save(doc);
  
  if (doc.exists) {
    alert('Neue Version mit Zeitstempfel erstellt: ', doc);
  } else {
    alert('Das Dokument konnte nicht gespeichert werden!')
  }
}
```

In der Variable `timestamp` speichern wir den aktuellen Zeitstempel in Millisekunden. Danach erstellen wir eine neue Instanz der `File`-Klasse, die auf den Pfad `~/Desktop/document.indd` zeigt und speichern sie in die Variable  `doc`. Anschliessend prüfen wir mit `app.documents.length === 0)` in einer `if`-Entscheidung, ob bereits ein Dokument geöffnet ist. Wenn noch kein Dokument geöffnet ist, erstellen wir mit `app.documents.add()` ein neues Dokument und speichern eine Referenz auf dieses Dokument in der Variable `create`.

Mit `app.activeDocument.saved` und `app.activeDocument.modified` testen wir anschliessend in einer `if`-Entscheidung, ob das Dokument schonmal gespeichert wurde oder bereits modifiziert ist. Wurde es entweder noch nie gespeichert, weil es mit diesem Skript erstellt wurde, oder wurde es modifiziert, wird es mit  `app.activeDocument.save(doc)` gespeichert. Wurde es dagegen bereits gespeichert aber ist nicht modifiziert, speichern wir es nicht.

Wenn der Speicherbefehl ausgeführt wurde prüfen wir zum Schluss mit `doc.exists`, ob das Dokument wirklich am vorgesehenen Ort existiert und teilen dem Anwender mit, ob das so ist oder nicht.

