# Erste Schritte mit JavaScript

Bevor wir mit Adobe ExtendScript starten, wollen wir dir JavaScript etwas näher bringen. Wenn du die grundlegenden Elemente von JavaScript verstanden hast, wird dir der Einstieg in Adobe ExtendScript einfacher fallen.

## RunJS installieren

Für die nächsten Übungen brauchst du eine kleine, kostenlose Applikation mit dem Namen [*RunJS*][runjs]. *RunJS* kann deinen JavaScript Code in Echtzeit auswerten und das Resultat anzeigen. Wichtig ist: Mit *RunJS* kannst du kein ExtendScript entwickeln! Wir brauchen es nur als «Spielwiese» für die kommenden ***JavaScript***-Übungen.

Du kannst *RunJS* für Windows [hier][runjs-win] und *RunJS* für Apple MacOS [hier][runjs-mac] herunterladen und anschliessend installieren.

Nach der Installation kannst du *RunJS* wie alle anderen Applikationen auf deinem Computer starten. Die Benutzeroberfläche besteht aus zwei Teilen. Auf der linken Seite kannst du deinen JavaScript-Code eintippen und auf der rechten Seite siehst du die Auswertung deines Codes.

![Run JS UI](./img/runjs-ui.png "Benutzeroberfläche von RunJS")

Wir verwenden für die Screenshots das Farbmuster «Neo». Möglicherweise wird bei dir *RunJS* mit einem dunklen Hintergrund dargestellt. Du kannst die Farbeinstellungen, das sogenannte Syntax Highlighting, in den Einstellungen bei Bedarf umstellen.

Während du tippst, kann es vorkommen, dass dir *RunJS* auf der rechten Seite in der Auswertung einen Fehler anzeigt, obwohl du noch nicht fertig bist. Lass dich davon nicht beirren. Wenn du allerdings der Meinung bist, dass dein Code fertig ist und dir *RunJS* auf der rechten Seite einen Fehler anzeigt, dann stimmt etwas nicht in deinem JavaScript.

![Run JS Error](./img/runjs-error.png "RunJS zeigt einen Fehler an")

## Übungen

In den kommenden Abschnitten haben wir eine Reihe von Übungen für dich vorbereitet. Wir empfehlen dir, den Code nicht einfach nach *RunJS* zu kopieren, sondern ihn abzutippen, um ein Gefühl dafür zu bekommen. Die Übungen bestehen meist aus einem Aufgabenfeld, wo wir dir etwas vorprogrammieren. Diesen Code kannst du nach *RunJS* übertragen und schauen, ob du auf das gleiche Ergebnis kommst, wie wir.

### Testlauf

#### Aufgabe

Schreibe die folgende Linie in die linke Fensterhälfte von *RunJS*.

```javascript
console.log("Hello World");
```

#### Lösung

*RunJS* sollte dir diese Lösung anzeigen:

![RunJS Testen](./img/runjs-test.png "RunJS ein erstes Mal testen.")

[runjs]:https://runjs.dev/
[runjs-mac]:https://github.com/lukehaas/RunJS/releases/download/v1.10.1/RunJS-1.10.1.dmg
[runjs-win]:https://github.com/lukehaas/RunJS/releases/download/v1.10.1/RunJS-Setup-1.10.1.exe
