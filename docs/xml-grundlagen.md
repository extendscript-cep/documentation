# XML Grundlagen

## Übersicht

Die e***X***tensible ***M***arkup ***L***anguage, kurz XML, ist eine Auszeichnungssprache. In der Praxis wird der Begriff XML oft polyvalent verwendet. Du findest ihn im Zusammenhang mit der Sprache selbst und als Sammelbegriff verschiedener Sprachen aus der XML-Sprachfamilie.

Wir beginnen bei der Auszeichnungssprache: XML ist eine Auszeichnungssprache – in englisch eine ***Markup Language***. Eine Markup Langage ist eine maschinenlesbare Sprache, mit der du Daten gliedern und formatieren kannst. Das heisst, dein XML-Dokument hat eine Struktur, die sowohl von dir als auch von deinem Computer verstanden wird. Ihr trefft euch so zu sagen auf einer sprachlichen Metaebene.

Für XML gibt es international festgelegte Standards, die genau beschreiben, wie ein XML-Dokument beschaffen sein muss, um als XML-Dokument zu gelten. Der Schlüsselbegriff in diesem Kontext ist ***valides*** XML. Nur valides XML ist soweit korrekt, dass es von jedem Menschen und Computer verstanden werden kann.

Der XML-Standard und die Standards der XML-Sprachfamilie werden vom World Wide Web Konsortium, kurz *W3C* entwickelt. Das ist das gleiche Gremium, welches auch den *HTML*-Standard entwickelt und definiert. Wenn wir von der XML-Sprachfamilie sprechen, dann meinen wir eine Reihe von Auszeichnungssprachen, die (direkt oder indirekt) auf XML aufbauen. Das sind unter anderem:

| Name                                                         | Kürzel    |
| ------------------------------------------------------------ | --------- |
| XML Schema                                                   | XSD       |
| XML Path Language                                            | XPath     |
| Dokumenttypdefinition                                        | DTD       |
| Extensible Stylesheet Language (inkludiert Transformationen) | XSL, XSLT |
| Extensible Hypertext Markup Language                         | XHTML     |
| Scalable Vector Graphics                                     | SVG       |
| Mathematical Markup Language                                 | MathML    |

## Elemente, Tags und Attribute

Ein XML-Dokument besteht aus mindestens einem XML-Element:

```text
<root id="1000">Lorem ipsum dolor</root>
│                                      │
└──────────────────┬───────────────────┘
                 Element
```

Ein XML-Element erkennst du an einem öffnenden und einem schliessenden Tag. Achte auf den Schrägstrich `/` (Slash) beim schliessenden Tag:

```text
      Tag                          Tag
┌─ ── ─┴ ── ── ┐                 ┌─ ┴─ ┐
│              │                 │     │
<root id="1000">Lorem ipsum dolor</root>
└──────────────────┬───────────────────┘
                 Element
```


Das öffnende (respektive schliessende) Tag wird gerne auch als Start- und Endbezeichner benannt. Im Beispiel trägt der Bezeichner den Namen `root`. Das ist aber keine Pflicht. Du kannst deine Bezeichner grundsätzlich nennen, wie du willst – solange dein Dokument valide ist. Das heisst zum Beispiel, dass der Start- und Endbezeichner den gleichen Namen haben müssen. XML erkennt am Schrägstrich `/` und dem Bezeichner, wo ein Element abschliesst.

In manchen Fällen werden dir die Namen deiner Tags respektive Bezeichner durch ein Schema oder eine DTD vorgegeben. Dazu kommen wir aber später.

Ein Start-Tag kann in seinem inneren, also zwischen der öffnenden Klammer `<` und der schliessenden Klammer `>`, Attribute haben. Attribute kannst du dir als Eigenschaften vorstellen, die ein Element und seinen Inhalt charakterisieren:

```text
      Tag                          Tag
┌─ ── ─┴ ── ── ┐                 ┌─ ┴─ ┐
│              │                 │     │
<root id="1000">Lorem ipsum dolor</root>
│     └─ ─┬ ──┘                        │
│      Attribut                        │
│                                      │
└──────────────────┬───────────────────┘
                 Element
```

In unserem Beispiel haben wir das Attribut `id`. Es trägt den Wert `1000`. Das könnte zum Beispiel bedeuten, dass wir vor uns den Datensatz mit der Identifikationsnummer 1000 sehen. Ganz genau würden wir das aber nur wissen können, wenn wir uns auf eine Referenz, zum Beispiel ein XML-Schema, berufen können.

Zwischen den Tags liegen die Daten des XML-Elements. In unserem Beispiel ist das ein Textknoten:

```text
      Tag                          Tag
┌─ ── ─┴ ── ── ┐                 ┌─ ┴─ ┐
│              │                 │     │
<root id="1000">Lorem ipsum dolor</root>
│     └─ ─┬ ──┘ └ ─ ─ ─ ┬ ─ ─ ─ ┘      │
│      Attribut    Textknoten          │
│                                      │
└──────────────────┬───────────────────┘
                 Element
```

Von einem Textknoten spricht man, wenn ein XML-Element aus nur einem Textwert besteht. Es kann aber auch weitere Elemente enthalten. Wie wir damit umgehen, siehst du im nächsten Abschnitt.

## Wie XML verarbeitet wird

Damit ein Computer oder Programm XML verarbeiten kann, braucht es einen XML-Parser. Der XML-Parser analysiert dein Dokument und macht etwas damit. Zum Beispiel ist der XML-Parser von Adobe InDesign dafür zuständig, dass du XML importieren, platzieren und exportieren kannst. Damit der Parser seine Arbeit machen kann, muss dein Dokument eine absolut korrekte Struktur haben. Ansonsten wird dir der Parser sagen, dass er dein Dokument nicht versteht.

Diese «absolut korrekte Struktur» folgt einigen wenigen Regeln:

* Alle XML-Elemente müssen ein schliessendes Tag haben
* Alle XML-Tag-Bezeichner sind case-sensitive (zwischen Gross- und Kleinschreibung wird unterschieden)
* Alle XML-Elemente müssen korrekt verschachtelt sein
* Das XML-Dokument darf nur ein einziges Wurzelelement enthalten
* Die Werte von Attributen müssen in Anführungszeichen eingeschlossen sein

Wenn möglich solltest du dem Parser zu Beginn immer angeben, nach welchem Standard, welchen Schemas und gegebenenfalls in welchen Namensräumen du arbeitest. Das machst du mit einer speziellen, ersten Zeile in deinem XML-Dokument: der Deklaration.

## Deklaration

Mit der Deklaration informierst du den Parser über die folgende Struktur des Dokuments. Du erkennst sie gut an den Klammer-Fragezeichen-Kombinationen (`<?xml`/`?>`). Innerhalb der Deklaration kannst du sogenannte *Pseudo-Attribute* definieren. Sie bestehen wie normale Attribute aus einer Kombination von Bezeichner und Wert. Für den XML-Parser sind sie jedoch keine wirklichen Attribute. Stattdessen entnimmt er ihnen Anweisungen für die weitere Verarbeitung.

Theoretisch könntest du die Deklaration auch weglassen. In diesem Fall entscheidet jedoch der Parser, wie er arbeiten wird. Das kann zum Problem werden – zum Beispiel, wenn sich dein Dokument von den  angenommenen Standards des Parsers unterscheiden. Wir empfehlen deshalb, in jedem Dokument eine Deklaration zu setzen. In der einfachsten Form wird in der Deklaration einzig die verwendete XML-Spezifikation in deinem Dokument deklariert:

```xml
 <?xml version="1.0"?>
 ```

XML gibt es in zwei Versionen respektive Spezifikationen: 1.0 und 1.1. *Der wesentliche Unterschied ist, dass die Version 1.1 die Weiterentwicklung von Unicode berücksichtigt.* Die Version 1.0 gilt allerdings als ausgereift und ist heute noch massgebend. Verwende daher die Version 1.1 nur, wenn du einen konkreten Anwendungsfall hast – zum Beispiel, wenn dein XML-Dokument Zeichen enthalten wird, die erst mit Unicode 2.0 eingeführt wurden.

Neben der verwendeten XML-Version solltest du auch das Encoding des Dokuments angeben. Der Parser erkennt daran, von welchem Zeichensatz er ausgehen soll. Das ist besonders wichtig für die richtige Verarbeitung von Sonderzeichen:

```xml
<?xml version="1.0" encoding="UTF-8">
```

Mit dem dritten *Pseudo-Attribut*, dem `standalone`, sagst du dem Parser, ob er dein Dokument gegenüber einem Schema oder einer DTD validieren soll. Was ein Schema und eine DTD ist, zeigen wir dir in den nächsten Abschnitten. Für den Augenblick kannst du dir das so vorstellen, als würdest du mit oder ohne Rechtschreibeprüfung arbeiten. Wenn `standalone` den Wert `yes` hat, weiss der Parser, dass er nicht mit der Rechtschreibeprüfung arbeiten soll. Das heisst, er wird nur prüfen, ob dein Dokument wohlgeformt ist und den oben genannten Regeln entspricht. Eine weitere Prüfung, zum Beispiel ob alle notwendigen Tags enthalten sind oder die Tags richtig heissen, wird er nicht machen.

```xml
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 ```

Wenn du mit einer solchen, «einfachen» Deklaration arbeitest, ist es wichtig, dass du die drei *Pseudo-Attribute* genau in dieser Reihenfolge aufführst. So verlangt es der XML-Standard.
