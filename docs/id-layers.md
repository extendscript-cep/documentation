# Ebenen erstellen und benennen

Dieses Skript zeigt dir an einem Beispiel, wie du Ebenen in Adobe InDesign ansprechen kannst.

```javascript
function main() {
  var labels = [ 'Deutsch', 'Französisch', 'Italienisch' ];
  
  if (app.documents.length === 0) {
    var document = app.documents.add();
  }

  return setLayers(labels);
}

main();


function setLayers(names) {
  var layers = app.activeDocument.layers;

  for (n in names) {
    if (app.activeDocument.layers.itemByName(names[n])) {
      alert('Existierende Ebene mit dem Namen "' + names[n] +
        '", Skript abgebrochen und nicht umbenannt!');
      return false;
    }

    if (n < layers.length) {
      app.activeDocument.layers[n].name = names[n];
    } else {
      app.activeDocument.layers.add({ name: names[n] });
    }
  }

  return layers;
}
```

Als erstes definieren wir die Funktion `main` und ordnen der Variable `labels` in einem Array die Namen der Ebenen zu. Das erlaubt uns, später unkompliziert über alle Namen zu iterieren.

Mit `app.documents.length` prüfen wir in einer `if`-Bedingung, ob mindestens ein Dokument geöffnet ist. Die Prüfung schlägt fehl, wenn kein Dokument geöffnet und der Rückgabewert von `app.documents.length` entsprechend `0` ist. In diesem Fall erstellen wir ein neues Dokument mit `app.documents.add()`. Würde nämlich kein Dokument geöffnet sein, könnten wir auch keine Ebenen ansprechen, was zu einer `exception` führen würde. Im dümmsten Fall könnte das bedeuten, dass sich InDesign an diesem Skript aufhängt – und das wollen wir nicht.

Mit `setLayers(Labels)` rufen wir die gleichnamige Funktion auf und übergeben ihr die `label` als Argument. Das Schlüsselwort `return` davor, stellt sicher, dass unser Skript mit einem Rückgabewert endet.

In der Funktion `setLayers(names)` nehmen wir einen Parameter entgegen, den wir mit `names` bezeichnen. Das heisst, die `labels` aus `main` werden innerhalb von `setLayers` mit `names` angesprochen.

In `setLayers` prüfen wir als erstes in einer `if`-Bedingung, ob bereits eine Ebene mit einem Namen, den wir setzen wollen, exisiert. Ist dies der Fall, wird ein `alert`-Dialog ausgegeben und das Skript umgehend mit `return false` abgebrochen. Das müssen wir so machen, weil ExtendScript nicht eine gleichnamige Ebene hinzufügen kann. Das führt zu einem Laufzeitfehler.

Wenn keine gleichnamige Ebene gefunden wurde, prüfen wir anschliessend in einer `for...in`-Schleife mit einer `if`-Bedingung, ob das Dokument bereits eine oder mehrere Ebenen hat. Ist das der Fall, werden die existierenden Ebenen umbenannt. Sind keine existierenden Ebenen mehr vorhanden und noch nicht alle Namen abgearbeitet, springt `else` an und erstellt die fehlenden Ebenen.

Möglicherweise willst du nicht, dass deine existierenden Ebenen umbenannt werden. In diesem Fall kannst du die `if...else`-Bedingung herauslöschen und nur die Zeile `app.activeDocument.layers.add({ Name: names[i] });` stehen lassen.

```diff
- if (i < layers.length) {
-   app.activeDocument.layers[i].name = names[i];
- } else {
-   app.activeDocument.layers.add({ name: names[i] });
- }
+ app.activeDocument.layers.add({ name: names[i] });
```

Mit `return layers` geben wir eine Referenz auf alle Ebenen des Dokuments zurück in die Funktion `main`, welche diese als Schlusswert and die Umgebung weiterleitet.
