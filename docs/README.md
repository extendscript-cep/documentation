# Einführung in Adobe ExtendScript & CEP

<div style="display:flex;border-top:2px solid rgba(0,0,0,0.1);border-bottom:2px solid rgba(0,0,0,0.1);margin-bottom:0.85em;padding-top: 0.85em;padding-bottom: 0.85em;">
  <div style="flex: 0 0 auto;width:100px;">
    <img src="https://gitlab.com/extendscript-cep/project/-/raw/master/logo/export/master.png" style="display:block;height:auto;width:100%">
  </div>
  <div style="flex: 1 1 auto;padding-left:1.3em;">
    <p style="font-size:0.9em;font-weight:bold;">Unabhängige Dokumentation zu den Themen Adobe ExtendScript, CEP und XML. Zusammengetragen durch eine Gruppe von Freiwilligen.</p>
  </div>
</div>

## Übersicht

Diese Dokumentation ist als Lernumgebung, Leitfaden und Referenz für den Alltag gedacht. Einsteigende können sich ins Thema einlesen. Für regelmässige Anwender haben wir viele nützliche Beispiele und Referenzen zusammengestellt. Und für die «schnellen Sucher» gibt es eine Reihe von Skripten, die du sofort nutzen kannst.

## Auszeichnungen

| Formatierung                     | Bedeutung
|----------------------------------|------------------------------
| ***Fett/Kursiv***                | Sehr wichtige Begriffe, Konzepte
| *Kursiv*                         | Wichtige Begriffe, Konzepte oder Techniken sowie Dateinamen und Pfade
| *`Datei > Neu`*                  | Navigation zu Befehlen über Adobe InDesign Menüs und Shortcuts* sowie Bezeichnungen von Dialogen, Bedienfeldern oder Menüs
| `GREP & Code`                    | GREP (im Quellcode) Anweisungen und Programmcode
| [Link/Email](https://www.gfz.ch) | Links zu Webseiten und E-Mail-Adressen

\* Die Tastaturkürzel orientieren sich am Apple MacOS. Sofern nichts anderes erwähnt ist, entspricht die `cmd`-Taste (⌘) der `ctrl`-Taste unter Windows.

## Core Team

<table>
  <thead>
    <tr>
      <th style="text-align:center;">Andreas</th>
      <th style="text-align:center;">Matteo Baschera</th>
      <th style="text-align:center;">Manuela Gloor</th>
      <th style="text-align:center;">Stefanie Maurer</th>
      <th style="text-align:center;">Stefan Huber</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center;"><img height="100px" src="https://secure.gravatar.com/avatar/ce83e9cacd4e32589c04bfe60deb4a51?s=180&d=identicon"></td>
      <td style="text-align:center;"><img height="100px" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5987791/avatar.png"></td>
      <td style="text-align:center;"><img height="100px" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5942770/avatar.png"></td>
      <td style="text-align:center;"><img height="100px" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/5970963/avatar.png"></td>
      <td style="text-align:center;"><img height="100px" src="https://secure.gravatar.com/avatar/545345c83f095bbc785578c37878d62e?s=800&d=identicon"></td>
    </tr>
    <tr>
      <td style="text-align:center;">Entwicklung</td>
      <td style="text-align:center;">Technische Beratung</td>
      <td style="text-align:center;">Kommunikation</td>
      <td style="text-align:center;">Entwicklung</td>
      <td style="text-align:center;">Entwicklung</td>
    </tr>
  </tbody>
</table>

## Changelog

Den Verlauf unserer Änderungen findest du im <a href="https://gitlab.com/extendscript-cep/documentation/-/blob/master/CHANGELOG.md" target="_blank">CHANGELOG.md</a> des Repositories.

## Rechtliches

Alle referenzierten Quellen wurden während des Erstellungsprozesses sorgfältig geprüft. Insbesondere bei Verweisen auf Inhalte aus dem Internet kann es sein, dass diese nach einer bestimmten Zeit nicht mehr verfügbar sind oder verändert wurden. Die Urheberschaft dieser Dokumentation übernimmt deshalb in keinem Fall Haftung für die Inhalte Dritter. Dies gilt auch für empfohlene Software oder ähnliche Produkte. Die Handhabung dieser geschieht auf eigene Verantwortung.

## Urheberrecht & Lizenzen

Programmcode steht unter der [`MIT-Lizenz`](https://gitlab.com/extendscript-cep/documentation/-/blob/master/LICENSE). Text und Bilder stehen unter der [Creative Commons BY NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode).

## Open Source

Wir verwenden für diese Dokumentation diverse Open-Source-Bibliotheken. Siehe [Hinweis im Quellcode](https://gitlab.com/extendscript-cep/documentation/-/blob/master/README.md).
