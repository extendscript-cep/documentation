# Adobe InDesign einrichten

Öffne Adobe InDesign und erstelle ein leeres neues Dokument:

* Format: A4, hoch
* Seiten: Einzelseiten (***keine*** Doppelseiten)
* Ränder: Gleichmässig 12 Millimeter an allen vier Seiten
* Anschnitt: 5 Millimeter
* Infobereich: 100 Millimeter

![ID XML, neues Dokument](./img/idxml-newdocument.png)

Als nächstes musst du die Struktur-Ansicht einblenden. Das machst du über das Menü *Ansicht* > *Struktur* > *Struktur einblenden*:

![ID XML, Struktur anzeigen](./img/idxml-showstructure.png)
