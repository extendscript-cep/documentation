# XML zusammenführen und Bilderkatalog erstellen

Öffne ein neues Dokument und wähle als Vorlage das erstellte Adobe-InDesign-Template. Wir haben es als `katalog.indt` benannt:

![ID XML, open Template](./img/idxml-opentpl.png)

Öffne die Adobe-InDesign-Voreinstellungen im Abschnitt *Eingabe* und stelle sicher, dass du die Option *Intelligenter Textfluss* aktiviert hast und der Haken bei *Auf primäre Textrahmen beschränken* ***deaktiviert*** ist:

![ID XML, Einstellungen *Eingabe*](./img/idxml-typesettings.png)

## XML zusammenführen

Wähle *XML Importieren* im Flyout-Menü der Struktur-Ansicht und selektiere die Datei `index.xml` aus unserem Projektverzeichnis. Kontrolliere, dass du dir die Importoptionen anzeigen lässt:

![ID XML, Import XML](./img/idxml-importxml.png)

Bei den Importoptionen aktivierst du:

* *Link erstellen*
* *Wiederholte Textelemente kopieren*
* *Inhalte von Elementen, die nur Leerräume enthalten, nicht importieren*
* *Elemente, Rahmen und Inhalte löschen, die mit dem importierten XML nicht übereinstimmen*

![ID XML, alle Elemente importieren](./img/idxml-xmlmassimport.png)

Danach kontrollierst du, dass bei *XSLT Anwenden* die Transformation `transformation.xslt` aus dem Projektverzeichnis korrekt angewählt ist und klickst auf *OK*.

Adobe InDesign importiert nun den Inhalt aus `index.xml`, führt ihn über die Transformation von `transformation.xslt` und platziert ihn in deinem Dokument. Für jede Bild-Text-Kombination erstellt dir Adobe InDesign dabei eine neue Seite. Das Endergebnis sollte so aussehen:

![ID XML, fertiges Dokument](./img/idxml-finished.png)

Fertig – das hat Spass gemacht!
