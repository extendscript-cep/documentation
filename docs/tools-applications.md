# Programme

In diesem Abschnitt fassen wir einige Programme zusammen, die du bei der Entwicklung von Adobe ExtendScript, CEP und XML nutzen kannst. Es ist eine unvollständige Liste und wir sind dir dankbar, wenn du uns weitere Orte mitteilen kannst.

## Eclipse XML Editors and Tools

> Open Source Software (kostenlos/freiwillige Spende)

| ExtendScript | CEP | XML | Bemerkung                    |
| ------------ | --- | --- | ---------------------------- |
|              |     | x   | XML: Solide Grundausstattung |

Download & Installation:

* Eclipse Java: <https://www.eclipse.org/>
* Eclipse XML Editors and Tools: Via installiertes Eclipse über das Menü *Help* > *Eclipse Marketplace* oder den Direktlink <https://marketplace.eclipse.org/content/eclipse-xml-editors-and-tools-0>

*Hinweis: Wenn du dir bei der Installation unsicher bist, findest du eine Anleitung im Abschnitt [XPath](./xml-xpath.md).*

## VSCode/VSCodium

> Open Source Software (kostenlos/freiwillige Spende)

| ExtendScript | CEP | XML | Bemerkung                             |
| ------------ | --- | --- | ------------------------------------- |
| x            | x   | (x) | XML: Nicht für alle Aufgaben geeignet |

Download & Installation:

* VSCode: <https://code.visualstudio.com/>
* VSCodium: <https://vscodium.com/>
* Extensions: siehe [*Einführung*](./einfuehrung.md)

*Hinweis: VSCode und VSCodium sind weitgehend deckungsgleich. VSCode wird von Microsoft entwickelt und mit Microsoft Tracking ausgelierfert. VSCodium wird von einer Gruppe von freien Entwicklern betreut. Sie stellen das Programm VSCode ohne Microsoft Tracking zur Verfügung. Wir arbeiten in dieser Dokumentation mit VSCodium ohne Microsoft Tracking.*
