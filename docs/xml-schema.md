# Schema

Mit einem XML-Schema kannst du – wie mit einer DTD – Regeln für dein XML-Dokument festlegen. Sie lassen sich in einem Schema aber genauer und flexibler definierten als mit einer DTD. Zum Beispiel kennt die DTD nur den Datentypen `#PCDATA`. Dagegen kann ein XML-Schema über 40 einfache Datentypen unterscheiden. Dazu gehören unter anderem `string`, `int`, `date` und `float`. Du kannst in XML-Schemas aber auch eigene Datentypen bestimmen, was in einer DTD nicht möglich ist. Ein XML-Schema erkennst du in der Regel an der Dateiendung `.xsd`. Die Abkürzung steht für *XML Schema Definition*.

Wir wechseln zu VSCodium und legen ein neues XML-Schema mit dem Namen `data.xsd` an. Validieren wollen wir unser `data.xml` aus dem letzten Abschnitt. Falls du direkt in diesem Abschnitt einsteigst, kannst du dir die Arbeitsdaten aus dem letzten Abschnitt <a href="/examples/xml-dtd.zip" target="_blank" download>hier</a> herunterladen.

Dein Arbeitsordner sollte nun drei Dokumente enthalten:

```text
xml
├── data.dtd
├── data.xml
└── data.xsd
```

## Schema anlegen

Ein XML-Schema beginnt, wie ein XML-Dokument beginnen muss: mit einer Deklaration. Danach wird das Schema eingeführt. Auch hier: *Ein XML-Schema ist ein XML-Dokument*. Es muss den Regeln von wohlgeformten XML folgen und darf zum Beispiel nur ein Wurzelelement haben. Für unser `data.xsd` sieht das so aus:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
</xs:schema>
```

In der Deklaration legen wir fest, welchem XML-Standard wir folgen und mit welcher Zeichenencodierung wir arbeiten. Das *Pseudo-Attribut* `standalone` fällt weg. Der Zweck eines Schemas ist, ein anderes Dokument zu validieren. Es kann also nie alleine stehen.

Das XML-Tag `xs:schema` sieht hier etwas anders aus, als die Tags in unserem XML-Dokument. Es hat vor dem Doppelpunkt einen sogenannten *Namespace*. Ein *Namespace* definiert einen Namensraum oder «Zuständigkeitsbereich». Ein solcher bestimmt die Gültigkeit eines Tags. Ein Namensraum ermöglicht, dass zum Beispiel Elemente mit dem gleichen Namen aber anderem (semantischem) Inhalt, mehrfach vorkommen können. Es könnte zum Beispiel in einem deiner Dokumente ein Element `<name>` geben, das ein Mal den Namen einer Person enthält und ein anderes Mal den Namen eines Produkts. Damit du die beiden Elemente semantisch und technisch auseinander halten kannst, ordnest du sie unterschiedlichen Namensräumen zu:

```xml
<person:name>Meier</person:name>
<product:name>HP LaserJet 1234</product:name>
```

`person:name` ist nun der Name der Person im Namensraum `person`.  Dagegen ist `product:name` der Name des Produkts im Namensraum `product`.

In unserem Schema definieren wir den Namensraum `xs` und nennen das Wurzelelement `schema`. Daraus ergibt sich die Kombination `xs:Schema`. Mit `xmlns:xs="http://www.w3.org/2001/XMLSchema"` teilen wir dem Parser mit, dass der Namensraum (`xmlns`) mit dem Namen `xs` ein XML-Schema ist und dessen Standard des W3C folgt.

```text
Tag (Bezeichner)           Definition
        │                       │
        ▼   ┌───────────────────┴─────────────────────┐
 <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" …
  ▲                │
   ─ ─ ─ ─ ─ ─ ─ ─ ┘
     Namensraum
```

`elementFormDefault="qualified"` führen wir sicherheitshalber auf. Es besagt, dass Namensräume, wenn es in unserem `data.xml` welche gäbe, explizit angeführt würden. Im Augenblick haben wir in `data.xml` aber keine Namespaces eingesetzt. Ein gutes, detailliertes Beispiel für `elementFormDefault` findest bei Bedarf unter anderem bei <a href="https://www.wilfried-grupe.de/XSD3_8.html" target="_blank">wilfried-grupe.de</a>.

## Schema aufbauen

Wie in der DTD müssen wir auch im Schema jetzt festlegen, welche Objekte in welcher Reihenfolge auftreten können. Wir beginnen mit dem Wurzelelement `data` aus `data.xml`:

```diff
 <?xml version="1.0" encoding="UTF-8"?>
 <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
+  <xs:element name="data">
+    <xs:complexType>
+      <xs:sequence maxOccurs="unbounded">
+        <xs:element ref="items"/>
+      </xs:sequence>
+    </xs:complexType>
+  </xs:element>
 </xs:schema>
```

`xs:element` leitet das Element ein. Das Attribut `name` mit dem Wert `data` bestimmt, dass das eingeleitete Element `data` heissen muss.

`xs:complexType` definiert, dass unser Element ein Element ist, das weitere Elemente und Attribute enthalten darf. Danach leiten wir über `sequence` die erlaubten Kindelemente ein. `sequence` selbst legt aber «nur» die Regeln für deren Verhalten fest. Wenn du `sequence` irgendwo ohne Attribute siehst, also so: `<xs:sequence>`, bedeutet das, dass die nachfolgenden Kindelemente mindestens ein Mal und maximal ein Mal – also genau ein Mal – vorkommen dürfen. Dieses Minimal und Maximal kannst du über die Attribute `minOccurs` und `maxOccurs` steuern. Wir nutzen `maxOccurs` mit `unbounded` um dem Parser mitzuteilen, dass `items` mindestens ein Mal vorkommen müssen und mehrfach vorkommen können. `<xs:element ref="items"/>` legt anschliessend fest, dass die Kindelemente `items` heissen müssen. ***Achtung, hier heisst es `ref=Items`!*** Wir referenzieren damit auf die Definition von `items`, welche wir im nächsten Schritt anlegen werden.

Dein ganzes Schema sollte aktuell so aussehen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
  <xs:element name="data">
    <xs:complexType>
      <xs:sequence maxOccurs="unbounded">
        <xs:element ref="items"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>
```

Nachdem wir nun `data` definiert haben, fügen wir die (referenzierte) Definition für `items` unter `<xs:element name="data">...</xs:element>` hinzu:

```diff
 <?xml version="1.0" encoding="UTF-8"?>
 <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
   <xs:element name="data">
     <xs:complexType>
       <xs:sequence maxOccurs="unbounded">
         <xs:element ref="items"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
+  <xs:element name="items">
+    <xs:complexType>
+      <xs:sequence maxOccurs="unbounded">
+        <xs:element ref="item"/>
+      </xs:sequence>
+    </xs:complexType>
+  </xs:element>
 <xs:schema>
```

`data` und `items` sind in der unmittelbaren Struktur der direkten Kindelemente fast gleich. Nur die Namen der Beizeichner ändern sich – `data` wird zu `items` und `items` zu `item`.

Mit `<xs:element ref="item"/>` referenzieren wir auf das Element `item`, welches wir als nächstes unterhalb von `<xs:element name="items">...</xs:element>` definieren wollen:

```diff
 <?xml version="1.0" encoding="UTF-8"?>
 <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
   <xs:element name="data">
     <xs:complexType>
       <xs:sequence maxOccurs="unbounded">
         <xs:element ref="items"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
   <xs:element name="items">
     <xs:complexType>
       <xs:sequence maxOccurs="unbounded">
         <xs:element ref="item"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
+  <xs:element name="item">
+    <xs:complexType>
+      <xs:sequence>
+        <xs:element ref="name"/>
+        <xs:element ref="author"/>
+        <xs:element ref="photo"/>
+      </xs:sequence>
+    </xs:complexType>
+  </xs:element>
 </xs:schema>
```

Von `item` erwarten wir, dass es eine exakte Anzahl Kindelemente in einer exakten Reihenfolge enthalten muss. Deshalb geben wir weder `minOccurs` noch `maxOccurs` an. So muss jedes der Kindelemente exakt ein Mal vorkommen. Innnerhalb von `<xs:element name="item">...</xs:element>` referenzieren wir auf die drei Kindelemente `name`, `author` und `photo`, welche wir jetzt zum Abschluss anlegen:

```diff
 <?xml version="1.0" encoding="UTF-8"?>
 <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
   <xs:element name="data">
     <xs:complexType>
       <xs:sequence maxOccurs="unbounded">
         <xs:element ref="items"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
   <xs:element name="items">
     <xs:complexType>
       <xs:sequence maxOccurs="unbounded">
         <xs:element ref="item"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
   <xs:element name="item">
     <xs:complexType>
       <xs:sequence>
         <xs:element ref="name"/>
         <xs:element ref="author"/>
         <xs:element ref="photo"/>
       </xs:sequence>
     </xs:complexType>
   </xs:element>
+  <xs:element name="name" type="xs:string"/>
+  <xs:element name="author" type="xs:anyURI"/>
+  <xs:element name="photo" type="xs:anyURI"/>
 </xs:schema>
```

`<xs:element name="name" type="xs:string"/>` definiert, dass unser Element mit dem Bezeichner `name` vom Typ `string` (Zeichenkette) sein muss. Dagegen haben wir bei den anderen beiden Elementen `author` und `photo` den Typ `xs:anyURI`. Mit diesem kann der Inhalt der Tags auf eine valide URL-Formatierung geprüft werden. Das ist zum Beispiel mit einer DTD nicht möglich, da die innerhalb einer DTD nur ein Datentyp für alle Textdaten zur Verfügung steht: `#PCDATA`.

## Schema in `data.xml` verlinken

Du kannst in deinem XML-Dokument `data.xml` kannst du die DTD durch das neue Schema ersetzen, indem du die Referenzzeile der DTD löscht und dafür deinem Wurzelelement `data` eine Referenz auf sein Schema mitgibst:

```diff
  <?xml version="1.0" encoding="UTF-8" standalone="no"?>
- <!DOCTYPE data SYSTEM "data.dtd">

- <data>
+ <data
+   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
+   xsi:noNamespaceSchemaLocation="data.xsd">
    <items>
      <item>
        <name>Peter Muster</name>
        <author>https://example.com/petermuster</author>
        <photo>https://example.com/photos/pm1234</photo>
      </item>
    </items>
  </data>
```

Mit `xmlns:xsi` teilen wir dem Parser mit, dass `data` und alle seine Elemente von einem Schema abgeleitet sind. `xsi:noNamespaceSchemaLocation="data.xsd"` spezifizieren wir danach unser Schema in `data.xsd`, das für alle Element in `<data>...</data>` gilt, die in keinem Namespace sind.

## Fertige Dokumente

Dein `data.xml` sollte nun so aussehen …

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<data
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="data.xsd">
  <items>
    <item>
      <name>Peter Muster</name>
      <author>https://example.com/petermuster</author>
      <photo>https://example.com/photos/pm1234</photo>
    </item>
  </items>
</data>
```

… und dein fertiges Schema so:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
  <xs:element name="data">
    <xs:complexType>
      <xs:sequence maxOccurs="unbounded">
        <xs:element ref="items"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="items">
    <xs:complexType>
      <xs:sequence maxOccurs="unbounded">
        <xs:element ref="item"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="item">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="name"/>
        <xs:element ref="author"/>
        <xs:element ref="photo"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="name" type="xs:string"/>
  <xs:element name="author" type="xs:anyURI"/>
  <xs:element name="photo" type="xs:anyURI"/>
</xs:schema>
```

Den aktuellen Stand unserer Dokumente kannst du auch <a href="/examples/xml-schema.zip" target="_blank" download>hier</a> nochmals herunterladen.
