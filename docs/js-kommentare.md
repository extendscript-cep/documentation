# Kommentare

Kommentare sind Notizen im Quelltext. Sie können zum Beispiel helfen, den Ablauf eines Skripts zu erklären. JavaScript unterscheidet einzeilige und mehrzeilige Kommentare. Einzeilige Kommentare beginnen mit zwei Schrägstrichen `//`; mehrzeilige Kommentare beginnen mit Schrägstrich und Stern `/*` und enden mit Stern und Schrägstrich `*/`.

```javascript
// Dies ist ein einzeiliger Kommentar

/* Dies ist
   ein Kommentar
   über mehrere
   Zeilen hinweg. */
```
