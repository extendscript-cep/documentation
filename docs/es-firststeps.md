# Erste Schritte mit Adobe ExtendScript

Nachdem wir die Grundlagen von JavaScript angeschaut und dich mit VSCodium etwas vertraut gemacht haben, wollen wir uns nun auf ExtendScript stürzen. Im nächsten Beispiel erstellen wir ein Skript, das in allen Adobe Programmen funktioniert. Wir benutzen dafür vor allem Funktionen aus dem JavaScript-Kern und dem Adobe ScriptUI. Das ScriptUI ist eine Sammlung von Funktionen und Klassen, mit denen Dialoge erstellt werden können.

***Erwarte nicht zu viel von dir!*** Wenn du zum ersten Mal JavaScript oder ExtendScript schreibst, kann es gut sein, dass du nicht alles verstehst. Darum geht es in dieser Übung aber nicht! Stattdessen wollen wir mit dir ein Skript von A-Z durchgehen, damit du eine Idee davon bekommst, wie Skripte zusammenhängen. In den Abschnitten der einzelnen Adobe Programme gibt es anschliessend viele übersichtliche Beispiele.

## Ergebnis

Das Ergebnis unseres Skripts ist ein Dialogfenster, über welches direkt auf die Hilfeseiten von Adobe gesprungen werden kann.

![ExtendScript Dialoge](./img/es-dialog.gif "ExtendScript Dialoge")

Du kannst das fertige Skript <a href="/examples/es-dialog.jsx" target="_blank" download>hier</a> herunterladen. Wir empfehlen dir aber, mit uns Schritt für Schritt vorzugehen.

## Das Skript

Als Erstes erstellst du in VSCodium ein neues Skript und speicherst es auf dem Desktop oder an einem Ort deiner Wahl auf deinem Computer. Wir nennen es `es-Dialog.jsx` – du kannst es aber benennen wie du möchtest. Wichtig ist, dass du die Dateiendung `.jsx` anhängst.

Als erstes solltest du jetzt kontrollieren, ob unten rechts *JavaScript* steht. Wenn dort so etwas wie *JavaScript React* steht, musst du auf den Schriftzug klicken und *JavaScript* auswählen. Danach bestimmst du über *Select the Target application* in welchem Adobe Programm dein Skript ausgeführt werden soll. Wir wählen dafür in unserem Beispiel *Adobe InDesign*.

![VSCodium einrichten](./img/es-vscodium-start.gif "VSCodium einrichten")

Wenn dein Adobe Programm noch nicht ausgeführt wird, wird VSCodium es automatisch starten.

Nun definieren wir die zentrale Funktion mit dem Bezeichner `main`. Die Aufgabe von `main` ist es, alle anderen Funktionen des Skripts aufzurufen und auszuführen. Beachte, dass die Definition von `main` noch nicht bedeutet, dass die Funktion auch wirklich ausgeführt wird. Damit das passiert, rufen wir sie gleich nach dem Definitionsblock mit `main()` auf.

```javascript
// Part 1
function main() {
  var config = [
    {
      name: 'Adobe InDesign',
      url: 'https://helpx.adobe.com/indesign/user-guide.html',
    },
    {
      name: 'Adobe Photoshop',
      url: 'https://helpx.adobe.com/photoshop/user-guide.html',
    },
  ];

  return dialog(config, openURL);
}

main();
```

Die Funktion `main` enthält eine Variable mit dem Bezeichner `config`. `config` ist ein Array, das Konfigurationsobjekte enthält. Sie bestehen aus jeweils zwei Eigenschaften: `Name` und `url`.`Name` Brauchen wir später für die Anzeige im Dropdown des Dialogs. `url` enthält dagegen die URL wo hinnavigiert werden soll. Du kannst diese Liste beliebig erweitern, solange du dich an das Muster hältst und die einzelnen Konfigurationsobjekte durch ein Komma `,` trennst.

Als nächstes wollen wir prüfen, ob das Skript korrekt in Adobe InDesign ausgeführt werden kann. Dafür starten wir eine *Debugging Session*. Beim *Debugging* kannst du mit Hilfe eines *Debugger* prüfen, ob dein Skript korrekt ausgeführt wird. Ein *Debugger* ist ein Hilfsprogramm das Analysefunktionen für eine bestimmte Umgebung bietet. Wir verwenden dafür den *ExtendScript Debugger* von Adobe.

Den *ExtendScript Debugger* rufst du auf, indem du in der *Activity Bar* auf das Icon mit dem Pfeil und dem Käfer klickst. Danach wählst du den blauen Button *Run and debug* und anschliessend *ExtendScript Debug*.

![VSCodium debuggen](./img/es-vscodium-debug.gif "VSCodium debuggen")

Oh! Was ist das? Der *ExtendScript Debugger* zeigt dir ein *Runtime Error* an. Du erkennst ihn unter anderem am roten Schriftzug unten links in der *Status bar* sowie am Text im *Output-Panel*:

```text
Starting /Users/run/Desktop/es-dialog.jsx in target: indesign-15.064 and engine: main.
Runtime Error: Error Code# 24: dialog is not a function @ file '~/Desktop/es-dialog.jsx'
[line:13, col:1]
```

Ganz am Ende der Nachricht siehst du die Linie, wo der Fehler auftritt: `[Line:13, col:1]`. Auf Linie 13 liegt bei uns `return dialog(config, openURL);`. Der Fehler entsteht, weil wir hier versuchen die Funktion `dialog()` aufzurufen, ohne dass wir sie definiert haben. Deshalb legen wir sie nun an:

```javascript
// Part 1
function main() {
  var config = [
    {
      name: 'Adobe InDesign',
      url: 'https://helpx.adobe.com/indesign/user-guide.html',
    },
    {
      name: 'Adobe Photoshop',
      url: 'https://helpx.adobe.com/photoshop/user-guide.html',
    },
  ];

  return dialog(config, openURL);
}

main();

// Part 2
function dialog(config, cb) {
  var options = [];

  for (var i in config) {
    options.push(config[i].name);
  }

  var dialog = new Window('dialog', 'Open Adobe Help');

  var selectionGrp = dialog.add('group');
  selectionGrp.add('statictext', [0, 0, 140, 15], 'Please select: ');

  var dropdown = selectionGrp.add('dropdownlist', [0, 0, 150, 24], options);

  dropdown.selection = 0;

  var buttonsGrp = dialog.add('group');
  buttonsGrp.add('button', undefined, 'OK');
  buttonsGrp.add('button', undefined, 'Cancel');

  var action = dialog.show();

  if (action === 1) {
    return cb(File.fs, config, dropdown.selection.index);
  }
  return false;
}
```

Die Funktion `dialog(config, cb)` nimmt zwei Parameter entgegen: `config` und `cb`. `config` muss ein Array sein, das Konfigurationsobjekte enthält, so wie wir sie in `main` bei `var config` definiert haben. Bei `cb` muss eine Funktion übergeben werden, die wir später ausführen können.

Mit `var options = [];` erzeugen wir ein leeres Array. In dieses füllen wir mit einer `for`-Schleife die Optionen respektive Texte, die später im Dropdown angezeigt werden sollen. Danach erzeugen wir mit `new Window('dialog', 'Open Adobe Help')` ein neues Fenster vom Typ *dialog* mit dem Titel *Open Adobe Help*.

In den Variablen `selectionGrp` und `buttonsGrp` speichern wir jeweils eine Referenz auf eine Gruppe von Menü-Elementen. Diese Referenz benutzen wir, um über die `.add()` Methode Anzeigeelemente hinzuzufügen. Bei `selectionGrp` sind das ein beschreibender Text (Label) mit einer Anweisung für die Dropdown-Auswahl und das Dropdown selbst: `selectionGrp.add('dropdownlist', [0, 0, 150, 24], options)`. `options` ist eine Referenz auf die Liste der Optionen, die wir weiter oben mit der `for`-Schleife erstellt haben.

`var action = dialog.show()` zeigt den Dialog an, den wir gerade erzeugt haben. Das Skript wird nun so lange «angehalten», bis der Benutzer eine Auswahl trifft und `OK` oder `Cancel` klickt. Wenn er `OK` klickt, gibt `dialog.show()` den Wert `1` zurück. Da wir unser Skript nur dann weiter ausführen wollen, wenn er `OK` geklickt hat, prüfen wir den Rückgabewert in einem `if`-Statement. Sollte er `Cancel` klicken ist der Rückgabewert `2` und das Skript wird mit `return false` beendet.

Wenn wir nun davon ausgehen, dass der Benutzer `OK` geklickt hat, soll die Seite im Browser geöffnet werden. Dafür verantwortlich ist die Zeile `return cb(File.fs, config, dropdown.selection.index)`. Sie führt die *Callback-Function*, auch «Rückruffunktion» genannt, aus. Der Parameter `cb` referenziert die Funktion, die in `main` mit `return dialog(config, openURL)` als Argument übergeben wurde. Das heisst `openURL` wird innerhalb der Funktion `dialog` zu `cb`. Diese Funktion `openURL` haben wir jedoch noch nicht definiert. Deshalb wollen wir sie jetzt hinzufügen. Das ganze Skript sieht so aus:

```javascript
// Part 1
function main() {
  var config = [
    {
      name: 'Adobe InDesign',
      url: 'https://helpx.adobe.com/indesign/user-guide.html',
    },
    {
      name: 'Adobe Photoshop',
      url: 'https://helpx.adobe.com/photoshop/user-guide.html',
    },
  ];

  return dialog(config, openURL);
}

main();

// Part 2
function dialog(config, cb) {
  var options = [];

  for (var i in config) {
    options.push(config[i].name);
  }

  var dialog = new Window('dialog', 'Open Adobe Help');

  var selectionGrp = dialog.add('group');
  selectionGrp.add('statictext', [0, 0, 140, 15], 'Please select: ');

  var dropdown = selectionGrp.add('dropdownlist', [0, 0, 150, 24], options);

  dropdown.selection = 0;

  var buttonsGrp = dialog.add('group');
  buttonsGrp.add('button', undefined, 'OK');
  buttonsGrp.add('button', undefined, 'Cancel');

  var action = dialog.show();

  if (action === 1) {
    return cb(File.fs, config, dropdown.selection.index);
  }
  return false;
}

// Part 3
function openURL(os, config, index) {
  var url = config[index].url || false;

  switch (os) {
    case 'Macintosh':
      var scpt =
        'tell application "Finder"\ropen location "' + url + '"\rend tell';
      app.doScript(scpt, ScriptLanguage.APPLESCRIPT_LANGUAGE);
      break;
    case 'Windows':
      var winShell =
        'dim objShell\rset objShell = CreateObject("Shell.Application")\rstr = "' +
        url +
        '"\robjShell.ShellExecute str, "", "", "open", 1 ';
      app.doScript(winShell, ScriptLanguage.VISUAL_BASIC);
      break;
    default:
      if (!url) {
        return alert('No valid URL given! Received: ' + url);
      }

      var link = File(Folder.temp.absoluteURI + '/link.html');
      link.open('w');

      var body =
        '<html><head><meta http-equiv="refresh" content="0;URL=\'' +
        url +
        '\'" /></head><body><p></p></body></html>';
      link.write(body);
      link.close();
      link.execute();
      return link;
  }
}
```

Die Funktion `openURL(os, config, index)` nimmt drei Parameter entgegen:

* `os` soll den Namen des Betriebssystems (Operating System) enthalten;
* `config` soll unser `config` Element aus `main()` enthalten;
* `index` referenziert die Auswahlposition im Dropdown – wurde das erste (Adobe InDesign) oder das zweite (Adobe Photoshop) Element ausgewählt.

Da die Funktion selbst als Argument an `cb` an die Funktion `dialog()` übergeben wurde, trägt sie innerhalb von `dialog()` den Bezeichner `cb()`. Wenn wir also innerhalb von `dialog()` den Aufruf `return cb(File.fs, config, dropdown.selection.index)` machen, rufen wir die Funktion `openURL` auf und übergeben ihr drei Argumente:

* `File.fs` ist eine Funktion von ExtendScript, die das aktuelle Betriebssystem zurückgibt. Dieses wird in `openURL()` als `os` bezeichnet.
* `config` ist die von uns in `main()` definierte `config` und bleibt in `openURL` auch als `config` bezeichnet.
* `dropdown.selection.index` referenziert den gewählten Index aus dem Dropdown. Innerhalb von `openURL()` bezeichnen wir ihn simpel als `index`.

Mit `var url = config[index].url || false` prüfen wir, ob wir für den gewählten Index ein Konfigurationsobjekt im Array von `config` haben. Ist dies nicht der Fall, wird `url` auf `false` gesetzt. Mit der `Switch...Case`-Anweisung entscheiden wir, für welches Betriebssystem wir den Aufruf des Links aufbereiten sollen. Danach führen wir den Aufruf über die ExtendScript-Methode `app.doScript()` aus. Sollte das Betriebssystem nicht evaluiert werden können, brechen wir das Skript ab, wenn `url` den Wert `false` hat und geben eine Fehlermeldung aus. Sollte dagegen die Betriebssystem-Evaluation irgendwie versagt haben obwohl wir eine valide `url` haben, versuchen wir über einen Workaround, den Link aus `url` in eine temporäre Datei zu schreiben und diese anschliessend im Browser zu öffnen.

Im [nächsten Abschnitt](./es-debugging.md) schauen wir uns an, wie du ein solches Skript `debuggen` und damit auf Fehler untersuchen kannst. Wenn du weiterfährst, kannst du das Skript im Editor geöffnet lassen.

