# Adobe Script Runner

Mit dem Adobe Script Runner kannst du ein Skript direkt in einem Adobe Programm ausführen, ohne dass du es über den Debugger führst. Das kann sinnvoll sein, wenn du kurz prüfen willst, ob ein Skript das macht, was du erwartest. Allerdings wirst du, im Gegensatz zum Debugger, nicht oder nur unzureichend über Fehler informiert, wenn welche bestehen.

Wenn du den Adobe Script Runner noch nicht installiert hast, kannst du das [hier](./einführung.md) nachholen.

Für das nächste Beispiel verwenden wir nochmals das Skript aus den letzten Abschnitten. Falls du direkt in diesem Abschnitt einsteigst, kannst du es <a href="/examples/es-dialog.jsx" target="_blank" download>hier</a> herunterladen und anschliessend in VSCodium öffnen.

Den Adobe Script Runner rufst du auf, in dem du die ***Command Palette*** **⌘⇧P** (Windows **ctrl⇧P**) öffnest und dort direkt dein Adobe Programm eintippst und aufrufst:

![VSCodium, Adobe Script Runner](./img/es-script-runner.gif "VSCodium, Adobe Script Runner")
