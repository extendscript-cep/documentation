# Grundlagen zu Adobe ExtendScript

## Was ist Adobe ExtendScript

Zur Erinnerung (aus [Einführung](einfuehrung.md)):

> Adobe ExtendScript setzt auf JavaScript auf und besteht aus einer Reihe von Befehlen, die erlauben, Adobe Programme anzusprechen. Zum einen gibt es die *Core JavaScript Elemente*, welche dem Standard von JavaScript entsprechen. Zum anderen gibt es die *Adobe ExtendScript Elemente*, welche durch Adobe definiert werden. Wichtig ist hier zu wissen, dass sich Adobe mit ExtendScript auf einen 25 Jahre alten Standard von JavaScript stützt. Das heisst, neuere Sprachelemente von JavaScript, wie zum Beispiel `const`, können nicht direkt verwendet werden.

## Skripte schreiben mit VSCodium

Während der Einführung haben wir das Programm VSCodium sowie einige Extensions installiert. Für die Entwicklung von Skripten sind zwei dieser Extensions besonders wichtig: der *ExtendScript Debugger* und der *Adobe Script Runner*. Der *ExtendScript Debugger* kommt von Adobe selbst. Der *Adobe Script Runner* wird von einem freien Entwickler mit dem Namen *renderTom* entwickelt.

### ExtendScript Debugger

Mit dem ExtendScript Debugger können Skripte in Adobe Programmen ausgeführt und genau analysiert werden. Dabei ist es nicht nötig, diese Skripte in den Skripte-Ordner des betreffenden Adobe Programms zu kopieren. Stattdessen kann das Skript irgendwo auf den Computer gespeichert und in VSCodium geöffnet sein. Wir machen ein Beispiel mit Adobe InDesign. Die Funktionsweise lässt sich jedoch auf alle Adobe Programme applizieren.

Für die nächsten Erklärungen verwenden wir ein Skript, das ein neues Dokument erzeugt:

```javascript
function main() {
  var d = app.documents.add();
  return d;
}

main();
```

 Du kannst es entweder nach VSCodium kopieren und speichern oder <a href="/examples/es-new-document.jsx" target="_blank" download>hier</a> herunterladen. Wenn du es kopierst und speicherst: der Name spielt keine Rolle, die Dateiendung ***muss*** aber `.jsx` sein.

#### Skript im ***Editor*** öffnen

![Skript im Editor öffnen](./img/esdebugger-open.png "Skript herunterladen und im Editor öffnen")

#### Haltepunkt (Breakpoint)

![Haltepunkt setzen](./img/esdebugger-breakpoint.png "Haltepunkt durch klick setzen.")

1. Haltepunkt setzen (klick links neben der Zeilennummer)

Ein Haltepunkt (Breakpoint) hält das Skript an der betreffenden Stelle an, damit es von uns analysiert werden kann.

#### Zielapplikation auswählen

![Zielapplikation wählen](./img/esdebugger-app.png "Adobe InDesign als Zielapplikation wählen.")

1. Ist JavaScript als Sprache ausgewählt? Wenn nicht: Auf Text bei 1. klicken und *JavaScript* auswählen
2. Auswahl durch klicken öffnen
3. Applikation (Adobe InDesign) mit klicken wählen

Die Zielapplikation legt fest, in welchem Adobe Programm das Skript ausgeführt werden soll. Wir wählen ***Adobe InDesign***.

#### Run and Debug öffnen

![Run & Debug starten](./img/esdebugger-run-debug.png "Debugging starten.")

1. Run and Debug Ansicht öffnen
2. Starten
3. ExtendScript Debug wählen (klick, Tastatur)

In diesem Schritt baut VSCodium eine Verbindung zu Adobe InDesign auf und führt dort das Skript aus, das im ***Editor*** geöffnet ist. Nun kann das Skript im nächsten Schritt genau analysiert und diagnostiziert werden.

#### Debuggen

![Degugging des Skripts](./img/esdebugger-debugging.png "Debugging des Skripts")

1. Steuerelemente
2. Programmzustand auswerten

Das Skript wird nun dort angehalten, wo der Haltepunkt (Breakpoint) gesetzt wurde. In der ***Side Bar*** wird zeitgleich der genaue Zustand von Skript und Adobe InDesign angezeigt. Um das Skript weiterlaufen zulassen, muss auf das erste blaue Steuerelement geklickt werden **|▷**.

##### Die Steuerelemente im Detail

Von links nach rechts:

* Play/Pause: Skript bis zum nächsten Breakpoint oder bis zum Schluss ausführen
* Step over: Über Funktion/Abschnitt springen
* Step in/out: Tiefer in Funktion gehen
* Restart: Debugging neu starten
* Stop: Ausführung des Skripts stoppen/abbrechen

### Adobe Script Runner

Der *Adobe Script Runner* kann als eine Art Kurzform des *ExtendScript Debuggers* verstanden werden. Er führt, wie der *ExtendScript Debugger* ein Skript in einem Adobe Programm aus. Im Unterschied zum Debugger, sammelt der Skript Runner jedoch keine Informationen zum Zustand des Skripts, während es ausgeführt wird. Stattdessen führt er das Skript umgehend aus – egal ob es fehlerhaft oder fehlerfrei ist.

![Skripte direkt ausführen mit dem Script Runner](./img/adobe-script-runner.png "Der Script Runner wird über die Command Palette gestartet.")

Der *Adobe Script Runner* kann mit Hilfe der *Command Palette* aufgerufen werden. Dazu drücke die Tastenkombination **⌘⇧P** (Windows **ctrl⇧P**), suche nach `Adobe InDesign` und bestätigen deine Wahl mit `Enter` oder einem Klick.
