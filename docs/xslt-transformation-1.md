# Transformation Aufgabe 1: `<items>` in `Root` umbenennen

Als erstes erstellen wir eine neue XSLT-Datei. Wir nennen sie `transformation.xslt`. Du kannst sie aber benennen wie du willst. Wichtig ist einfach, dass die Dateiendung `.xslt` ist.

In Eclipse kannst du über einen Rechtsklick auf den Projektordner und dann *New* > *File* ein neues Dokument erstellen.

![XSLT, Eclipse new File](./img/xslt-eclipse-file-new.png)

Im nächsten Dialogfenster benennen wir unser Dokument:

![XSLT, Eclipse File name](./img/xslt-eclipse-file-name.png)

Nach dem Erstellen sollte dir Eclipse die neue Datei direkt anzeigen. Auf der linken Seite siehst du im *Package Explorer*, dass das Dokment auf der obersten Ebene neben `index.xml` angelegt wurde:

![XSLT, Eclipse File blank](./img/xslt-eclipse-file-blank.png)

Jetzt können wir mit unseren Transformationen beginnen. Du kannst die nachfolgenden Beispiele direkt in die XSLT-Datei übertragen. Zwischendurch zeigen wir dir auch mit Screenshots, wie deine XSLT-Datei aussehen sollte.

Das «Wurzelelement» einer XSL-Transformation ist das <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/stylesheet" target="_blank">*Stylesheet*</a>. In seinem Innern werden wir nachher unsere «Transformationsregeln» in Form von *Templates* anlegen.

```xml
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

</xsl:stylesheet>
```

Im Attribut `version="1.0"` teilen wir dem XML-Parser mit, in welcher Version von XSLT wir arbeiten. Mit `xmlns:xsl="http://www.w3.org/1999/XSL/Transform"` definieren wir den korrespondierenden Namensraum. Wenn du nicht mehr weisst, was ein Namensraum ist, kannst du das gerne [hier](./xml-schema.md#schema-anlegen) nochmals kurz nachlesen.

Als nächstes fügen wir einige globale «Einstellungen» hinzu:

```diff
  <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
+   <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
+   <xsl:strip-space elements="*"/>

  </xsl:stylesheet>
```

Mit <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/output" target="_blank">`xsl:output method="xml"`</a> definieren wir, dass unser Resultat auch eine XML-Datei ist. `encoding="UTF-8"` legt den Zeichenstandard fest, in dem wir uns bewegen und mit `indent="yes"` teilen wir dem Parser mit, dass wir gerne ein schön eingerücktes Dokument wollen. Danach legen wir mit <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/strip-space">`xsl:strip-space elements="*"`</a> noch fest, dass wir im Ergebnis ***keine*** leeren Zeilen zwischen den Elementen haben wollen. Deine Transformation sollte nun so aussehen:

![XSLT, Document header](./img/xslt-document-header.png)

Im nächsten Schritt wollen wir ein <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/template" target="_blank">*Template*</a> erstellen, das auf alle XML-Elemente angewendet wird:

```diff
  <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>
+
+     <xsl:template match="node()|@*">
+       <xsl:copy>
+         <xsl:apply-templates select="node()|@*"/>
+       </xsl:copy>
+     </xsl:template>

  </xsl:stylesheet>
```

`match="node()|@*"` definiert, dass alle Elemente mit diesem Template angesprochen werden. <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/copy" target="_blank">`xsl:copy`</a> macht nichts anderes, als den Inhalt der gesamten XML-Datei zu kopieren. Entscheidend ist aber das eingeschlossene `xsl:apply-templates select="node()|@*"`. Es definiert, dass der Parser, sollte er ein weiteres Template für ein betreffendes Element finden, nicht kopiert, sondern stattdessen dieses Template anwendet.

Mit diesem Konstrukt schaffen wir uns die Möglichkeit, nicht für jedes Element ein Template erstellen zu müssen. Das machen wir, weil wir ja nicht alle Elemente verändern wollen, sondern nur an zwei Punkten (Aufgabe 1 & Aufgabe 2) eingreifen wollen. Die Elemente `item`, `name`, `author` und `photo` sollen ja bleiben, wie sie sind.

Jetzt, wo wir unser initales Template für alle Elemente erstellt haben, wollen wir jenes einfügen, das unser Wurzelelement von `items` auf `Root` umbenennt:

```diff
  <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

      <xsl:template match="node()|@*">
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:template>

+     <xsl:template match="items">
+       <xsl:text>&#xa;</xsl:text>
+       <xsl:element name="Root">
+      <xsl:apply-templates/>
+       </xsl:element>
+     </xsl:template>
  </xsl:stylesheet>
```

Was passiert hier?

Wenn der XML-Parser ein Element mit dem Namen `items` sieht, sucht er in unserer Transformation nach einem Template, das auf `items` angewendet werden soll. Mit `xsl:template match="items""` teilen wir dem Parser mit, dass wir für dieses Element ein Template haben und etwas ändern wollen. Danach erstellen wir mit <a href="https://developer.mozilla.org/en-US/docs/Web/XSLT/Element/element" target="_blank">`xsl:element Name="Root"`</a> ein neues XML-Element das den Namen `Root` haben soll. Anschliessend weisen wir den Parser an, mit `xsl:apply-templates` nach weiteren Templates zu suchen. Sollte er weitere Templates für Elemente in unserer XML-Datei finden, wird er diese anwenden. Wenn er für ein Element kein Template findet, sorgt `xsl:copy` dafür, dass das Element samt Tag und allfälligen Attributen kopiert wird. Mit der Zeile `<xsl:text>&#xa;</xsl:text>` stellen wir sicher, dass der XML-Parser im transformierten Dokument nach der Deklaration (`<?xml version="1.0" encoding="UTF-8"?>`) und vor dem `Root`-Tag einen Zeilenumbruch einfügen wird.

Mit dem aktuellen Stand unseres XSL-Stylesheets sollten wir die erste Aufgabe der Transformation erledigen können. Sie sollte in Eclipse nun so aussehen:

![XSLT, erste Aufgabe](./img/xslt-exercise-01.png)

Falls dein Dokument anders aussieht, kannst du bei Bedarf  deinen Code mit diesem hier ersetzen:

```xml
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
	  <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
```

### Transformation konfigurieren

Damit wir die Transformation prüfen können, muss sie in Eclipse konfiguriert werden. Klicke dazu auf das kleine schwarze Dreieck neben dem grünen «Play»-Button und danach auf *Run Configurations…*:

![XSLT, Eclipse Run Configuration](./img/xslt-eclipse-run-conf.png)

Im nächsten Dialogfenster machst du einen Rechtsklick auf *XSLT* und wählst *New Configuration*:

![XSLT, Eclipse New Configuration](./img/xslt-eclipse-new-conf.png)

In der Konfiguration klickst du im Tab *Main* auf den Button *Workspace* und wählst dort die XML-Datei `index.xml` aus und bestätigst mit dem *Ok*-Button.

![XSLT, Eclipse New Configuration](./img/xslt-eclipse-src-select.png)

*`workspace_loc` meint an dieser Stelle den Pfad bis zu einem Projekt auf der Festplatte. Eclipse wird diesen selbständig auflösen, wenn wir die Transoformation laufen lassen.*

![XSLT, Eclipse New Configuration](./img/xslt-eclipse-src-path.png)

Gleich unterhalb im Abschnitt *Transformation Pipeline* müsste Eclipse unsere XSLT-Datei `transformation.xslt` bereits automatisch erkannt haben:

![XSLT, Eclipse CSLT conf](./img/xslt-eclipse-xslt-conf.png)

Sollte das nicht der Fall sein oder sollte dort eine falsche XSLT-Datei stehen, kannst du den Eintrag über die Buttons *Add Files* und *Remove* verändern und auf die XSLT-Datei `transformation.xslt` zeigen lassen.

Als nächstes wechseln wir in den Tab *Output*. Dort macht dir Eclipse den Vorschlag, das transformierte Dokument `index.out.xml` zu nennen. Wir übernehmen das so und lassen den Haken bei *Use default location* gesetzt. Zum Schluss geben wir unserer Konfiguration noch einen aussagekräftigen Namen. Wir benennen sie von `New_configuration` in `xslt-transformation` um und bestätigen anschliessend mit einem Klick auf den Button *Run* unten rechts im Dialogfenster:

![XSLT, Eclipse out](./img/xslt-eclipse-out.png)

Eclipse führt nun unsere Konfiguration sofort aus und zeigt dir das Resultat an:

![XSLT, Eclipse first run](./img/xslt-eclipse-firstrun.png)

Wir finden diese Ansicht nicht so angenehm und empfehlen dir, das Resultatfenster unterhalb der Transformation anzeigen zu lassen. Das machst du mit einem Klick auf die zwei kleinen übereinanderliegenden Fenster in der oberen rechten Ecke:

![XSLT, Eclipse Layout](./img/xslt-eclipse-layout.png)

Wenn du die Transformation erneut starten willst, machst du das, indem du neben dem grünen «Play»-Button auf den kleinen schwarzen Pfeil klickst und anschliessend auf den Namen unserer Transformation (*xslt-transformation*) klickst:

![XSLT, Eclipse next run](./img/xslt-eclipse-next-run.png)

Die finale XML-Datei aus dieser Aufgabe (`index.out.xml`) kannst du bei Bedarf <a href="/examples/index.out.xml" target="_blank" download>hier</a> zum Abgleich herunterladen.
