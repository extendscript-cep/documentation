function main() {
  var config = [
    {
      name: 'Adobe InDesign',
      url: 'https://helpx.adobe.com/indesign/user-guide.html',
    },
    {
      name: 'Adobe Photoshop',
      url: 'https://helpx.adobe.com/photoshop/user-guide.html',
    },
  ];

  return dialog(config, openURL);
}

main();

function dialog(config, cb) {
  var options = [];

  for (var i in config) {
    options.push(config[i].name);
  }

  var dialog = new Window('dialog', 'Open Adobe Help');

  var selectionGrp = dialog.add('group');
  selectionGrp.add('statictext', [0, 0, 140, 15], 'Please select: ');

  var dropdown = selectionGrp.add('dropdownlist', [0, 0, 150, 24], options);

  dropdown.selection = 0;

  var buttonsGrp = dialog.add('group');
  buttonsGrp.add('button', undefined, 'OK');
  buttonsGrp.add('button', undefined, 'Cancel');

  var action = dialog.show();

  if (action === 1) {
    return cb(File.fs, config, dropdown.selection.index);
  }
  return false;
}

function openURL(os, config, index) {
  var url = config[index].url || false;

  switch (os) {
    case 'Macintosh':
      var scpt =
        'tell application "Finder"\ropen location "' + url + '"\rend tell';
      app.doScript(scpt, ScriptLanguage.APPLESCRIPT_LANGUAGE);
      break;
    case 'Windows':
      var winShell =
        'dim objShell\rset objShell = CreateObject("Shell.Application")\rstr = "' +
        url +
        '"\robjShell.ShellExecute str, "", "", "open", 1 ';
      app.doScript(winShell, ScriptLanguage.VISUAL_BASIC);
      break;
    default:
      if (!url) {
        return alert('No valid URL given! Received: ' + url);
      }

      var link = File(Folder.temp.absoluteURI + '/link.html');
      link.open('w');

      var body =
        '<html><head><meta http-equiv="refresh" content="0;URL=\'' +
        url +
        '\'" /></head><body><p></p></body></html>';
      link.write(body);
      link.close();
      link.execute();
      return link;
  }
}
