<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="items">
    <xsl:text>&#xa;</xsl:text>
    <xsl:element name="Root">
	  <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>