# Grundlagen in JavaScript

## Übersicht

JavaScript ist eine objektbasierte Sprache. Für Programmieranfänger ist das oft eine sehr abstrakte Aussage. Wie viele andere versuchen auch wir uns mit einer möglichst griffigen Erklärung. Allerdings ist es nicht so wichtig, wenn du diese nicht auf Anhieb nachvollziehen kannst. Unserer Erfahrung nach kommt das «richtige» Verständnis erst mit der Praxis.

### Klassen, Instanzen, Objekte

Objekte sind zusammenhängende Elemente aus Eigenschaften und Methoden. Eigenschaften können zum Beispiel mit Zeichen, Zahlen oder einem Zustand wie `TRUE` oder `FALSE` belegt werden. Methoden oder Funktionen führen Operationen aus. Sie können zum Beispiel die Eigenschaften des eigenen Objekts oder solche aus anderen Objekten verändern. Jedes Objekt hat einen Bauplan. In JavaScript werden diese Baupläne in der Regel als Basis-Klassen und Klassen bezeichnet. Alle Objekte, die du in deinen Skripten erstellst, werden von diesen Basisklassen und Klassen abgeleitet. Deshalb hast du unter Umständen auf Funktionen respektive Methoden Zugriff, die du nie selbst erstellt hast. Sie werden dir quasi als «Starthilfe» mitgegeben.

Die von dir erstellten Objekte werden gerne auch als «Instanzen» bezeichnet. Klassen selbst können keinen Zustand haben. Sie definieren nur, wie das abgeleitete Objekt zusammengebaut werden soll. Dagegen haben Objekte einen konkreten Zustand. Wenn die Klassen zum Beispiel eine Eigenschaft «Farbe» definiert, wird diese erst in deinem abgeleiteten Objekt mit Blau, Grün oder Gelb belegt sein.

### Schlüsselwörter

In JavaScript gibt es eine Reihe von *Schlüsselwörtern*, die mit einer konkreten Eigenschaft oder Funktion belegt sind. Die nächste Abbildung zeigt dir ihre Bedeutung. Wichtig ist, dass du die Schlüsselwörter nur an den exakt von Javascript vorgesehenen Stellen einsetzen kannst. Du kannst zum Beispiel keine deiner Variablen den Namen `delete` geben, weil das Wort bereits besetzt ist.

| Schlüsselwort | Beschreibung
| ------------- | -------------
| `break`| Abbruch von Schleifen
| `case` | Einleitung eines Treffers (`switch-case`)
| `catch`| Beginn eines Blocks zum Auffangen von Ausnahmen
| `continue`| Fortsetzung von Schleifen
| `default`| Vorgabefall bei `switch-case` (wenn kein Ergebnis)
| `delete`| Löschen von Datenfeldelement oder selbst definierter Objekteigenschaft
| `do`| Beginn einer fussgesteuerten Schleife (Überprüfung ob «weiter» am Fuss der Schleife)
| `else`| Einleitung alternativer Block bei `if`-Entscheidungsstruktur
| `false`| Der Wert ist *falsch*
| `finally`| Einsatz im JS-Ausnahmebehandlungskonzept, leitet auszuführenden Block ein
| `for`| Beginn einer kopfgesteuerten Schleife
| `function`| Leitet Funktion ein
| `if`| Leitet `if`-Fallunterscheidung ein
| `in`| Teil von `for`... `in`, bedingte Anweisung in Fallunterscheidung
| `instanceof`| Test des Objekttyps
| `new`| Einleitung bei Verwendung von Konstruktor für Objektdefinition
| `return`| Sprachanweisung mit (optionaler) Übergabe eines Rückgabewerts von Funktionen
| `switch`| Einleitung `switch-case` Fallunterscheidung
| `this`| Stellvertreter für aktuelle Instanz des Objekts
| `throw`| JS-Ausnahmebehandlungskonzept: Auswerfen einer Ausnahme
| `true`| Der Wert ist *wahr*
| `try`| JS-Ausnahmebehandlungskonzept: Beginn des Umschliessens einer kritischen Anweisung
| `typeof`| Typ eines Elements
| `var`| Beginn einer Variablendefinition
| `void`| Leerer Funktionstyp
| `while`| Einleitung einer kopfgesteuerten Schleife
| `with`| Erlaubt mehrere Anweisungen mit Objekt durchzuführen
