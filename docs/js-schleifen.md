# Schleifen & Iteration

## Kontrollstrukturen in JavaScript (Schleifen)

Mit Kontrollstrukturen kannst du vergleichen und entscheiden, wie dein Programm weitergehen soll. Damit du Kontrollstrukturen umsetzen kannst, sind sogenannte Kontrollflussanweisungen nötig. Du kannst diese auf drei Arten einsetzen:

1. ***Entscheidungsanweisungen***: Wählen auf Grund einer Bedingung einen Programmfluss aus;
2. ***Schleifen*** bzw. Iterationsanweisungen: Wiederholen eine bestimmte Anzahl an Anweisungen;
3. ***Sprunganweisungen***: Verlassen eine umgebende Struktur.

## if-Bedingung

Mit der *if-Bedingung* kannst du eine einfache Entscheidung treffen.

```javascript
var test = 5;

if (test < 10) {
  console.log(test + ' ist kleiner als 10.')
}

else { // optional
  console.log('Der Wert von "test" ist: ' + test);
}
````

Wenn die Bedingung wahr ist, wird der *if-Block* ausgeführt, ansonsten der *else-Block*. In unserem Beispiel ist *if* wahr, weil die Ziffer 5 kleiner ist als 10.

Die Bedingung in den Klammern ist ein Vergleich, der sowohl true (richtig) als auch false (falsch) sein kann oder ein boolescher Wert selbst. Zur Definition des Vergleichs werden in der Regel Vergleichsoperatoren verwendet.

Der *else-Block* ist optional und muss nicht unbedingt notiert werden, wenn es keine Alternative gibt. Wenn er nicht notiert wird und die Prüfung im *if-Block* falsch ergibt, wird einfach nichts gemacht. Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/if...else) zu `if` und `else` findest du auch auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## switch-case

```javascript
var test = 'Hello';

switch(test){
  case 'World':
    console.log('"Case 1", "test" ist: ' + test);
    break;
  case 'Hello':
    console.log('"Case 2", "test" ist: ' + test);
    break;
  default:
    console.log('"default", "test" ist: ' + test);
} 
```

Mit *switch* und *case* kannst du auf mehrere Fälle prüfen. Du könntest das auch mit *if-else* machen, aber *switch* und *case* sind für solche Aufgaben eleganter. Das Schlüsselwort *switch* leitet die Unterscheidung ein. In die runden Klammern schreibst du den Wert, auf den du prüfen willst. Die einzelnen Fälle werden durch das Schlüsselwort *case* eingeleitet. Nach dem Schlüsselwort *case*  schreibst du deinen Testwert hinein.  Dieser Testwert kann ein beliebiger Datentyp sein. In unserem Beispiel testen wir auf einen `string`. Im zweiten `case` entspricht dieser genau dem Wert der Variable `test`. Damit wird *case* zwei positiv ausfallen (`true`) und ausgeführt.

In der Regel wirst du am Ende des Anweisungsblocks das Schlüsselwort `break` schreiben. `break verhindert`, dass die nachfolgenden Anweisungen in weiteren Blöcken ausgeführt werden. Stattdessen springt das Programm ans Ende von *switch*-*case*.

Der letzte Block ist meist der *default*-Zweig. Er ist eine Standardreaktion und wird ausgeführt, wenn kein *case*-Fall zutrifft. Als letzter Block kann auch ein *case*-Fall notiert werden, wenn keinen *default* willst. So oder so ist `break` im letzten Fall überflüsstig, da du nun am Ende deiner Entscheidungsstruktur bist. Neben `break` kannst du u.a. auch mit `return` arbeiten. 

Der grosse Nachteil der Programmflusssteuerung mit *switch*-*case* ist, dass du nur diskrete Fälle unterscheiden kannst. Es ist nur ein Vergleich auf exakte Gleichheit möglich. Vergleiche wie «grösser als» oder «kleiner als», wie bei *if*-*else*, gehen nicht. Zudem können nicht mehrere Werte gemeinsam getestet werden.

Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/switch) zu *switch*-*case* findest du auch auf den den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## Übung 1, `if` und `switch`-`case`

In dieser Übung wollen wir den Unterschied von `if` und `switch`-`case` etwas genauer anschauen.

### Aufgabe

Wir erstellen eine Variable `num` mit dem Wert `3` und lassen diese durch eine `if`- und eine `switch`-`case`-Schleife laufen.

```javascript
var num = 3;

if (num < 10) {
  console.log('Num ist kleiner 10: ' + num);
}

switch(num) {
  case 1:
    console.log('Num ist 1');
    break;
  case 2:
    console.log('Num ist 2');
    break;
  case 3:
    console.log('Num ist 3');
    break;
  default:
    console.log('Num ist: ' + num);
}    
```

### Lösung

![RunJS, Schleifen `if` und `switch`](./img/runjs-if-switch.png "Die Unterschiede von if und switch-case")

Die *if*-Bedingung trifft immer zu, bis `num` grösser oder `10` ist. Dagegen hat *switch*-*case* nur exakte Regeln für die Ziffern 1-3. Danach springt der `default`-Block an. Das heisst, wenn du die Variable `num` auf 10 setzt (`var num = 10;` statt `var num = 3;`), wird die *if*-Bedingung kein Ergebnis liefern. Dem gegenüber wirst du von *switch*-*case* auf jeden Fall eine Rückmeldung erhalten – nämlich jene des `default`-Blocks.

## while-Schleife

Die *while*-Schleife ist eine Iterationsanweisung. Sie führt am Anfang eines Blocks eine Prüfung einer Bedingung durch und wiederholt diese so lange, bis die Bedingung nicht mehr zutrifft. Damit ist sie kopfgesteuert (auch «abweisend» genannt). Jede *while*-Schleife braucht in ihrem Innern einen Mechanismus, mit dem die Schleife abgebrochen werden kann. Dadurch besteht die Gefahr, dass du aus Versehen eine Endlosschleife erzeugst. In den meisten Fällen ist diese nicht gewollt, denn sie verhindert, dass der gesamte Programmcode abgearbeitet werden kann. Der Interpreter bleibt dann in der Schleife «hängen».

```javascript
var stop = 5;
var times = 0;

while(times < stop) {
  times = times + 1;
  console.log('Times ist: ', times);
}
```

Es gibt zwei grundsätzliche Varianten, um aus einer while-Schleife auszusteigen:

1. In der Bedingung einer *while*-Schleife wird eine Variable geprüft, welche im Innern der Schleife mit jedem Durchlauf verändert wird – zum Beispiel numerisch erhöht oder verringert. Trifft die veränderte Variable in der Bedingung nach dem x-ten Durchlauf der Schleife nicht mehr zu, weil ihr Wert zu gross oder zu klein geworden ist, bricht die Schleife ab. Im obigen Beispiel gehen wir nach diesem Muster vor und steigen aus, wenn die Variable `Times` einen höheren Wert hat, als die Variable `stop`. Dazu erhöhen wir den Wert der Variable `Times` bei jedem Durchlauf `+1`.
2. Die Schleife wird mit einer Sprunganweisung wie `break`, `return` oder `throw` verlassen.

Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/schleifen_und_iterationen#while_Anweisung) findest du auch auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## do-while-Schleife

Die *do*-*while*-Schleife ist eine Variante der *while*-Schleife. Sie unterscheidet sich einzig darin, dass die in `do` notierten Anweisungen auf jeden Fall 1x ausgeführt werden, bevor die Bedingung erstmalig in `while` überprüft wird. Damit ist die *do*-*while*-Schleife fussgesteuert (auch annehmend genannt).

```javascript
var stop = 5;
var times = 5;

do {
  times = times + 1;
  console.log('Times ist: ', times);
}
while(times < stop);
```

Im Beispiel haben wir die Variable `Times` gleich zu Beginn mit dem Wert `5` belegt. Im Unterschied zum Beispiel mit der *while*-Schleife wird `times` danach auf `6` erhöht, bevor die Schleife abbricht, weil `6` grösser ist als `5`.

Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/schleifen_und_iterationen#do...while_Anweisung) für *do*-*while* findest du auch auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## Übung 2, `while` und `do`-`while`

In dieser Übung zeigen wir dir den Unterschied von `while` und `do`-`while`.

### Aufgabe

Wir erstellen eine Variable mit dem Bezeichner `times`. Dies zählen wir in einer *while*-Schleife nach unten bis zum Wert `0`. Danach schicken wir sie durch eine *do*-*while*-Schleife.

```javascript
var stop = 0;
var times = 5;

while(times > stop) {
  times = times - 1;
  console.log('times in "while": ', times);
}

console.log('Zwischenstand: ', times);

do {
  times = times + 1;
  console.log('times in "do-while": ', times);
}

while(times == stop);
```

### Lösung

![RunJS, while und do-while](./img/runjs-while-do.png "Die Unterschiede von while und do-while.")

Vor der *do*-*while*-Schleife hat `times` den Wert `0`. Obwohl die *while*-Anweisung unter `do` bereits jetzt zutrifft – `stop` ist `0` und `times` ist `0` wird die *do*-Anweisung noch einmal ausgeführt.

## for-Schleife

Die *for*-Schleife wirst du vor allem verwenden, um etwas hoch- oder runterzuzählen. Die *for*-Schleife hat den Vorteil, dass an zentraler Stelle alle Werte definiert werden können. Der vorzeitige Abbruch einer *for*-Schleife aus dem Innern, zum Beispiel mit einer Sprunganweisung wie break, ist allerdings möglich.

```javascript
var data = ['Apple', 'Kiwi', 3];

for(var i = 0; i < data.length; i++) {
  console.log('Array. Index ' + i + ': ' + data[i]);
}
```

Nach dem Schlüsselwort `for` definieren wir als erstes eine Zählvariable mit dem Bezeichner `i`. Danach folgt die Abbruchbedingung. Im Beispiel vergleichen wir die «länge» des Arrays `data` mit dem Wert von `i`. Hierzu musst du wissen, dass `data.length` nicht die Länge des Array-Index ausgibt, sondern die Anzahl Elemente im Array. Dadurch wird ein Vergleich besonders einfach. Der Index endet bei `2`, weil die erste Position im Array-Index `0` ist. Das heisst, der letzte Index `2` wird immer kleiner sein als die Anzahl Elemente, welche von `data.length` mit `3` ausgewiesen wird.

An der letzten Position erhöhen wir mit `i++` den Wert von `i` bei jedem Durchlauf mit `+1`. `i++` ist dabei die Kurzform von `i = i + 1`. Sobald `i` nicht mehr kleiner ist als `data.length`, wird die Schleife abgebrochen.

Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/schleifen_und_iterationen#for_Anweisung) zur *for*-Schleife findest du auch auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## for-in Schleife

Die *for*-*in* Schleife ist eine Variante der *for*-Schleife. Sie wird vor allem im Zusammenhang mit Objekten und Arrays verwendet. Im Unterschied zur *for*-Schleife führt sie eine automatische Iteration auf alle Elemente durch. Das heisst, du brauchst keine Kontrollvariable. Zusätzlich erkennt sie die wahrscheinlich schnellste Möglichkeit, um schnell über alle Eigenschaften und Methoden eines Objekts zu iterieren.

```javascript
var person = {
  prename: 'Jon',
  lastname: 'Skinner',
  description: 'Sublime Text developer'
}

for(prop in person) {
  console.log(prop + ': ' + person[prop])
}
```

Im Beispiel erstellen wir ein Objekt mit drei Eigenschaften. In der *for*-*in*-Schleife wird `prop` zum Stellvertreter des Bezeichners (Schlüssel) jeder Eigenschaft. Dadurch kann mit `person[prop]` effizient der Wert der aktuellen Eigenschaft im Durchlauf aufgerufen werden.

Ein detailliertes [Beispiel](https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/schleifen_und_iterationen#for...in_Anweisung) findest du auch auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).

## Übung 3, `for` und `for`-`in`

Für diese Übung verarbeiten wir ein Array mit drei Datensätze mit der *for*- und der *for*-*in*-Schleife.

### Aufgabe

Wir nehmen das Array aus dem Beispiel mit der *for*-Schleife und führen es nacheinander durch die beiden Schleifen.

### Lösung

![RunJS, `for` und `for`-`in`](./img/runjs-for-for-in.png "Die Unterschiede von for und for-in")

In der *for*-Schleife wird das Array mit Hilfe des Index abgearbeitet. Sobald der Index gross oder gleich gross ist wie die Anzahl Elemente im Array, verlässt der Interpreter die Schleife, weil die Bedingung nicht mehr Eintritt. Dagegen arbeitet die *for*-*in*-Schleife einfach alle Elemente des Arrays ab.

## `break` und `continue`

Mit `break` kannst du eine Schleife sofort abbrechen. Dagegen bricht `continue` nur den gegenwärtigen Durchlauf ab und springt zum unmittelbar nächsten Schleifendurchlauf. Das heisst, mit `continue` lassen sich bestimmte Teile einer verschachtelten Schleife ignorieren, ohne dass diese komplett abgebrochen wird.

Wird nach `break` ein Label angegeben (`break: label`), springt das Script nach dem Abbruch der Schleife direkt zur Position des `label` im Script. Der Name des `label` ist dabei frei wählbar. Die Funktion wird allerdings nicht von allen Interpretern richtig unterstützt und birgt zudem die Gefahr von «Spaghetti-Code».
