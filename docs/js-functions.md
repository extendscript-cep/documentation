# Funktionen

Funktionen werden bei JavaScript über das Schlüsselwort `function` eingeleitet. Im Unterschied zu Methoden, sind «reine» Funktionen eine Art «freistehend». Das heisst, sie bekommen einen eigenen Bezeichner, unter dem sie aufgerufen werden können. Vom Prinzip her machen sie dasselbe wie Methoden: sie führen etwas aus. Methoden sind aber an ein Objekt geknüpft.

Funktionen werden im Deutschen auch gerne als Anweisungen bezeichnet. Bevor du eine Funktion ausführen kannst, musst du sie definieren. Das wollen wir jetzt mit einem ersten Beispiel machen:

```text
          Bezeichner
              │
Schlüsselwort │
      │       │
      │       │
      ▼       ▼
   function doIt() {
       return 4 + 4;
   }     ▲
         │
         │
    Rückgabewert
```

Die Funktion `doIt` ist nun definiert und steht bereit für die Verwendung in deinem Skript. Kannst du sie aufrufen mit:

```text
doIt();
```

Das Schlüsselwort `return` weist die Funktion an, einen Wert zurückzugeben. Nach `return` verlässt der Interpreter die Funktion und schliesst sie ab. Es ist deshalb wichtig, dass du nach einem `return` keine weiteren Zeilen mehr mit Programmcode stehen hast. Dieser Code ist für den Interpreter unerreichbar, weil er die Funktion bei `return` ja bereits verlassen hat.

## Übung 1, Rückgabewert verstehen

### Aufgabe

Wir erstellen eine Funktion mit dem Namen `doIt()`. Sie kalkuliert die Addition 4 + 4 und gibt das Ergebnis via `return` an die Variable `result` zurück.

```javascript
function doIt() {
  return 4 + 4;
}

var result = doIt();

console.log(result);
```

### Lösung

![RunJS Funktion mit `return`](./img/runjs-fn-return.png "Funktion mit return ausführen.")

### Aufgabe

Nun löschen wir das `return` Statement aus der Funktion `doIt` und untersuchen, was mit dem Wert der Variable `result` passiert.

```javascript
function doIt() {
  4 + 4;
}

var result = doIt();

console.log(result);
```

### Lösung

![RunJS Funktion ohne `return`](./img/runjs-fn-noreturn.png "Funktion ohne return ausführen.")

Die Variable `result` ist nun `undefined`. JavaScript hat wohl 4 + 4 zusammengezählt, weil aber die `return` Anweisung fehlt, hat JavaScript das Ergebnis nicht zurückgegeben. Es konnte dadurch auch nicht in der Variable `result` gespeichert werden.

## Parameter und Argumente

Funktionen und Methoden können Argumente als Parameter entgegennehmen. Parameter sind stellvertretende Bezeichner innerhalb einer Funktion. Sie werden mit den Werten belegt, die beim Aufrufen der Funktion mitgegeben werden. Diese Werte werden auch Argumente genannt. Parameter- und Argumentlisten werden durch Kommas getrennt.

```text
         Bezeichner
              │
Schlüsselwort │ Parameter 1
      │       │    │   Parameter 2
      │       │    │      │
      ▼       ▼    ▼      ▼
   function doIt(first, second) {
       var result = first + second;
       return result;
   }     ▲
         │
         │
    Rückgabewert
```

Die Funktion `doIt(first, second)` kann nun zwei benannte Argumente als Parameter entgegennehmen und diese addieren. Der Aufruf mit Argumenten sieht so aus:

```text
doIt(2,3);
      ▲
      │
      │
 Argumente 2, 3
 In der Funktion werden sie mit
 'first' und 'second' bezeichnet
```

## Übung 2, Parameter und Argumente anwenden

In dieser Übung wollen wir den Aufruf mit Argumenten etwas genauer anschauen. Dafür erstellen wir die oben gezeigte Funktion `doIt` und rufen sie mit verschiedenen Argumenten auf.

### Aufgabe

```javascript
function doIt(first, second) {
  return first + second;
}

doIt(2, 3);
doIt(4, 5);
````

### Lösung

![RunJS, mit Argumenten und Parameter arbeiten](./img/runjs-fn-arg-param.png "Argumente und Parameter mit Funktionen nutzen.")

Im gezeigten Beispiel rufen wir die Funktion `doIt` mehrfach auf und ändern die Argumente. Wenn die Funktion anschliessend ausgeführt wird, referenzieren die Parameter `first` und `second` die Werte der übergebenen Argumente. *RunJS* zeigt uns die Ergebnisse jedes Aufrufs an. Weil die Funktion eine `return`-Anweisung hat, können wir auf die Hilfe von `console.log()` verzichten. Das Ergebnis wird in den globalen Kontext zurückgegeben und von der JavaScript-Umgebung aufgefangen und angezeigt.

## Benannte und anonyme Funktionen

In den nächsten Abschnitten schauen wir uns die erweiterten Techniken von Funktionen an. Sie sind für uns im weiteren Verlauf nicht so wichtig. Trotzdem wollen wir sie euch nicht vorenthalten. Vielleicht triffst du sie in einem Skript an, das du abändern willst.

Bis jetzt haben wir vor allem benannte Funktionen definiert. Das heisst, jede Funktion hatte einen Bezeichner – zum Beispiel `doIt`. Anonyme Funktionen haben keinen Bezeichner. Wir können sie zum Beispiel einer Variable zuweisen:

```javascript
var test = function() {
            return 'Hello World';
          }
```

Jetzt kann die anonyme Funktion über den Bezeichner der Variable ausgeführt werden. Der Aufruf unterscheidet sich dabei nicht von den benannten Funktionen:

```javascript
test();
```

Oft siehst du anonyme Funktionen auch als Methoden in Objekten – so wie in unseren Beispielen:

```javascript
var data = {
  month: 'July',
  day: 15,
  calc: function(param) {
    return param * param;
  }
};

data.calc(5);
```

Auch hier unterscheidet sich der Aufruf nicht wesentlich von den benannten Funkionen – ausser dass die Methode nur über die Punktnotation des Objekts erreichbar ist.

Neben den syntaktischen Feinheiten haben anonyme Funktionen aber auch gewichtige, technische Unterschiede. Zum Beispiel kann eine anonyme Funktion sofort ausgeführt werden. Das heisst, Definition und Aufruf gehen Hand in Hand:

```javascript
(function(){
  return 'Hello World';
})();
```

Ebenfalls ist es möglich, anonyme Funktionen als Callback mitzugeben. Was Callbacks sind, zeigen wir dir gleich im übernächsten Abschnitt.

```javascript
function main(value, callback) {
  var result = value * value;
  return callback(result);
}

var final = main(12345, function(item) {
  return 'Das Ergebnis ist: ' + item;
});

console.log(final);
```

### Innere Funktionen

JavaScript erlaubt das Verschachteln von Funktionen. Dabei wird eine Funktion direkt in eine andere Funktion hinein notiert (ähnlich wie innere Klassen bei Java). Wird so programmiert, gelten folgende Regeln:

* Die innere, verschachtelte Funktion kann nur über die äussere aufgerufen werden und ist für alle anderen Zugriffe unzugänglich.
* Die innere Funktion kann die Variablen der äusseren Funktion verwenden.
* Die äussere Funktion hat keinen Zugriff auf die Variablen der inneren Funktion.
* ***Die innere Funktion wird als lokale Variable der äusseren Funktion angesehen.***

```javascript
function outer(){
  var a = 1;
  function inner(){
    document.write(a);
  }
  inner();
}

outer();
```

In diesem Beispiel wird die Funktion `inner` automatisch ausgeführt, wenn die Funktion `outer` aufgerufen wird. Das ist so, weil die Funktion `inner` Teil der Funktion `outer` ist, da sie in ihrem Rumpf oder Block definiert wird. Von aussen ist die Funktion `inner` nicht erreichbar. Das heisst, wenn du in der Zeile nach `outer()` versuchst, die Funktion `inner()` aufzurufen, wird dir die JavaScript-Umgebung sagen, dass die Funktion `inner` nicht existiert. Ebenso ist es mit der Variablen `a`. Sie existiert nur innerhalb der Funktion `outer` und kann von aussen nicht erreicht werden.

### Callback-Funktion

Eine Callback-Funktion, kurz Callback, ist eine JavaScript-Funktion, die einer Methode oder Funktion als Argument oder Option übergeben wird, so wie oben gezeigt:

```javascript
function main(value, callback) {
  var result = value * value;
  return callback(result);
}

var final = main(12345, function(item) {
  return 'Das Ergebnis ist: ' + item;
});

console.log(final);
```

Callback-Funktionen können benannte und anonyme Funktionen sein. Im Unterschied zu den anonymen Funktionen, müssen die benannten Funktionen jedoch vorgängig definiert werden:

```javascript
function main(value, callback) {
  var result = value * value;
  return callback(result);
}

function test(item) {
  return 'Das Ergebnis ist: ' + item;
};

var final = main(12345, test);

console.log(final);
```

Eine Callback-Funktion muss eine bestimmte Struktur haben, damit sie funktioniert. Einfach formuliert haben sie drei Aufgaben:

1. Übergebene Argumente an die Funktionen in ihrem Innern weiterleiten.
2. Auf deren Rückmeldung (return) zu horchen.
3. Die Rückmeldung (return) an den Aufrufer zurückzuleiten.

 Ein [detailliertes Beispiel](https://developer.mozilla.org/de/docs/Mozilla/js-ctypes/Using_js-ctypes/Declaring_and_Using_Callbacks) findest du u. a. auf den Hilfeseiten des [Mozilla Developer Networks](https://developer.mozilla.org/de/).
