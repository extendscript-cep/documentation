# Adobe ExtendScript and CEP

![Build Status](https://gitlab.com/extendscript-cep/documentation/badges/master/pipeline.svg)

(Unofficial!) Documentation and resources about Adobe ExtendScript, CEP and XML. The generated documentation you will finde [here](https://extendscript-cep.gitlab.io/documentation).

## Installation

```bash
cd <project_root>
nvm use
npm ci
```

## Run for development

```bash
npm start
```

## Build

```bash
npm run build
```

## Tools

* [Eclipse IDE](https://www.eclipse.org/)
  * [Eclipse XML Editors and Tools](https://marketplace.eclipse.org/content/eclipse-xml-editors-and-tools-0)
* [honkit](https://github.com/honkit/honkit) (Static–Site-Genearator)
* [markdownlint](https://github.com/DavidAnson/markdownlint) (Linting in Markdown Files)
* [RunJS](https://runjs.dev/) (JavaScript Playground)
* [VSCodium](https://vscodium.com/) (Community-driven, freely-licensed binary distribution of Microsoft’s editor VSCode)
  * Extension [Adobe Script Runner](https://marketplace.visualstudio.com/items?itemName=renderTom.adobe-script-runner)
  * Extension [ExtendScript Debugger](https://marketplace.visualstudio.com/items?itemName=Adobe.extendscript-debug)
  * Extension [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
  * Extension [XML (Red Hat)](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)
