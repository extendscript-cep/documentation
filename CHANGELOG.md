# Changelog

## 2.1.0-beta (2020-12-13)

### New

* Introduction in XML
* Introduction in Adobe InDesign and its XML workflow(s)
* Overview of Tools

## 2.0.0-beta (2020-09-05)

### Upated

* Revised and improved JavaScript chapters

